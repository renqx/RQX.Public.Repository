﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Hardware.Net;
using RQX.Common.Core.Logger;

namespace Mindray_EX
{
    public class EX55 : AbstractComCollection
    {
        #region 默认配置
        public override DeviceType DeviceType => DeviceType.麻醉机;

        public override int BaudRate => 57600;

        public override int DataBits => 8;

        public override StopBits StopBits => StopBits.One;

        public override Parity Parity => Parity.None;

        public override bool DTR => false;

        public override bool RTS => false;
        #endregion
        #region 常量
        private const byte ESC = 0x1B;
        private const byte SOH = 0x01;
        private const byte CR = 0x0D;
        #endregion
        private bool IsFirst = true;
        public EX55() { }

        public EX55(StructureModel<MyCom> structureModel) : base(structureModel)
        {
        }

        protected override void Connect(StepType stepNum, string msg)
        {
            throw new NotImplementedException();
        }

        protected override void CircleSend()
        {
            throw new NotImplementedException();
        }

        protected override string CutToOneMsg(StringBuilder sb)
        {
            throw new NotImplementedException();
        }

        protected override StepType CheckRecvMsgType(string lineStr)
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<string, string> AnalysisData(string msg)
        {
            throw new NotImplementedException();
        }

        #region 内部函数
        private string GetCheckNum(byte[] buffer)
        {
            byte chk = 0;
            buffer.ForEach(b => chk += b);
            var str = chk.ToString("x2");
            var first = str.Substring(0, 1).ToByte()[0].ToString("x2");
            var second = str.Substring(1, 1).ToByte()[0].ToString("x2");
            return $"{first}{second}";
        }

        #endregion
    }
}
