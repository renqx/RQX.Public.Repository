﻿namespace RQX.DataCollection.Test.Net
{
    partial class TcpClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_send = new System.Windows.Forms.Button();
            this.txt_send = new System.Windows.Forms.RichTextBox();
            this.txt_recv = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_send_circle_time = new System.Windows.Forms.TextBox();
            this.ckb_send_circle = new System.Windows.Forms.CheckBox();
            this.ckb_send_hex = new System.Windows.Forms.CheckBox();
            this.ckb_send_autoclean = new System.Windows.Forms.CheckBox();
            this.ckb_send_file = new System.Windows.Forms.CheckBox();
            this.btn_clean = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.ckb_recv_stop = new System.Windows.Forms.CheckBox();
            this.ckb_recv_hex = new System.Windows.Forms.CheckBox();
            this.ckb_recv_newline = new System.Windows.Forms.CheckBox();
            this.ckb_recv_autosave = new System.Windows.Forms.CheckBox();
            this.btn_connect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_set_port = new System.Windows.Forms.TextBox();
            this.txt_set_ip = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_send
            // 
            this.btn_send.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_send.Location = new System.Drawing.Point(596, 428);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(83, 86);
            this.btn_send.TabIndex = 5;
            this.btn_send.Text = "发 送";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // txt_send
            // 
            this.txt_send.Location = new System.Drawing.Point(168, 428);
            this.txt_send.Name = "txt_send";
            this.txt_send.Size = new System.Drawing.Size(422, 86);
            this.txt_send.TabIndex = 4;
            this.txt_send.Text = "";
            // 
            // txt_recv
            // 
            this.txt_recv.Location = new System.Drawing.Point(168, 12);
            this.txt_recv.Name = "txt_recv";
            this.txt_recv.Size = new System.Drawing.Size(511, 410);
            this.txt_recv.TabIndex = 3;
            this.txt_recv.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "毫秒";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "循环间隔";
            // 
            // txt_send_circle_time
            // 
            this.txt_send_circle_time.Location = new System.Drawing.Point(62, 130);
            this.txt_send_circle_time.Name = "txt_send_circle_time";
            this.txt_send_circle_time.Size = new System.Drawing.Size(47, 23);
            this.txt_send_circle_time.TabIndex = 1;
            // 
            // ckb_send_circle
            // 
            this.ckb_send_circle.AutoSize = true;
            this.ckb_send_circle.Location = new System.Drawing.Point(17, 103);
            this.ckb_send_circle.Name = "ckb_send_circle";
            this.ckb_send_circle.Size = new System.Drawing.Size(75, 21);
            this.ckb_send_circle.TabIndex = 0;
            this.ckb_send_circle.Text = "循环发送";
            this.ckb_send_circle.UseVisualStyleBackColor = true;
            // 
            // ckb_send_hex
            // 
            this.ckb_send_hex.AutoSize = true;
            this.ckb_send_hex.Location = new System.Drawing.Point(17, 76);
            this.ckb_send_hex.Name = "ckb_send_hex";
            this.ckb_send_hex.Size = new System.Drawing.Size(99, 21);
            this.ckb_send_hex.TabIndex = 0;
            this.ckb_send_hex.Text = "十六进制发送";
            this.ckb_send_hex.UseVisualStyleBackColor = true;
            // 
            // ckb_send_autoclean
            // 
            this.ckb_send_autoclean.AutoSize = true;
            this.ckb_send_autoclean.Location = new System.Drawing.Point(17, 49);
            this.ckb_send_autoclean.Name = "ckb_send_autoclean";
            this.ckb_send_autoclean.Size = new System.Drawing.Size(111, 21);
            this.ckb_send_autoclean.TabIndex = 0;
            this.ckb_send_autoclean.Text = "发送完自动清空";
            this.ckb_send_autoclean.UseVisualStyleBackColor = true;
            // 
            // ckb_send_file
            // 
            this.ckb_send_file.AutoSize = true;
            this.ckb_send_file.Location = new System.Drawing.Point(17, 22);
            this.ckb_send_file.Name = "ckb_send_file";
            this.ckb_send_file.Size = new System.Drawing.Size(111, 21);
            this.ckb_send_file.TabIndex = 0;
            this.ckb_send_file.Text = "从文件读取发送";
            this.ckb_send_file.UseVisualStyleBackColor = true;
            // 
            // btn_clean
            // 
            this.btn_clean.Location = new System.Drawing.Point(12, 160);
            this.btn_clean.Name = "btn_clean";
            this.btn_clean.Size = new System.Drawing.Size(128, 23);
            this.btn_clean.TabIndex = 1;
            this.btn_clean.Text = "清除接收区显示";
            this.btn_clean.UseVisualStyleBackColor = true;
            this.btn_clean.Click += new System.EventHandler(this.btn_clean_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(12, 131);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(128, 23);
            this.btn_save.TabIndex = 1;
            this.btn_save.Text = "保存接收区数据";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_stop
            // 
            this.ckb_recv_stop.AutoSize = true;
            this.ckb_recv_stop.Location = new System.Drawing.Point(17, 103);
            this.ckb_recv_stop.Name = "ckb_recv_stop";
            this.ckb_recv_stop.Size = new System.Drawing.Size(99, 21);
            this.ckb_recv_stop.TabIndex = 0;
            this.ckb_recv_stop.Text = "暂停接收显示";
            this.ckb_recv_stop.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_hex
            // 
            this.ckb_recv_hex.AutoSize = true;
            this.ckb_recv_hex.Location = new System.Drawing.Point(17, 76);
            this.ckb_recv_hex.Name = "ckb_recv_hex";
            this.ckb_recv_hex.Size = new System.Drawing.Size(99, 21);
            this.ckb_recv_hex.TabIndex = 0;
            this.ckb_recv_hex.Text = "十六进制显示";
            this.ckb_recv_hex.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_newline
            // 
            this.ckb_recv_newline.AutoSize = true;
            this.ckb_recv_newline.Location = new System.Drawing.Point(17, 49);
            this.ckb_recv_newline.Name = "ckb_recv_newline";
            this.ckb_recv_newline.Size = new System.Drawing.Size(75, 21);
            this.ckb_recv_newline.TabIndex = 0;
            this.ckb_recv_newline.Text = "自动换行";
            this.ckb_recv_newline.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_autosave
            // 
            this.ckb_recv_autosave.AutoSize = true;
            this.ckb_recv_autosave.Location = new System.Drawing.Point(17, 22);
            this.ckb_recv_autosave.Name = "ckb_recv_autosave";
            this.ckb_recv_autosave.Size = new System.Drawing.Size(111, 21);
            this.ckb_recv_autosave.TabIndex = 0;
            this.ckb_recv_autosave.Text = "自动保存到文件";
            this.ckb_recv_autosave.UseVisualStyleBackColor = true;
            // 
            // btn_connect
            // 
            this.btn_connect.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_connect.ForeColor = System.Drawing.Color.Red;
            this.btn_connect.Location = new System.Drawing.Point(12, 78);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(128, 48);
            this.btn_connect.TabIndex = 2;
            this.btn_connect.Text = "连 接";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "停止位";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "端口号";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "IP地址";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_send_circle_time);
            this.groupBox1.Controls.Add(this.ckb_send_circle);
            this.groupBox1.Controls.Add(this.ckb_send_hex);
            this.groupBox1.Controls.Add(this.ckb_send_autoclean);
            this.groupBox1.Controls.Add(this.ckb_send_file);
            this.groupBox1.Location = new System.Drawing.Point(12, 352);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 162);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "发送区设置";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_clean);
            this.groupBox2.Controls.Add(this.btn_save);
            this.groupBox2.Controls.Add(this.ckb_recv_stop);
            this.groupBox2.Controls.Add(this.ckb_recv_hex);
            this.groupBox2.Controls.Add(this.ckb_recv_newline);
            this.groupBox2.Controls.Add(this.ckb_recv_autosave);
            this.groupBox2.Location = new System.Drawing.Point(12, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 191);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "接收区设置";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_set_port);
            this.groupBox3.Controls.Add(this.txt_set_ip);
            this.groupBox3.Controls.Add(this.btn_connect);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 137);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "设置";
            // 
            // txt_set_port
            // 
            this.txt_set_port.Location = new System.Drawing.Point(62, 49);
            this.txt_set_port.Name = "txt_set_port";
            this.txt_set_port.Size = new System.Drawing.Size(78, 23);
            this.txt_set_port.TabIndex = 3;
            // 
            // txt_set_ip
            // 
            this.txt_set_ip.Location = new System.Drawing.Point(62, 19);
            this.txt_set_ip.Name = "txt_set_ip";
            this.txt_set_ip.Size = new System.Drawing.Size(78, 23);
            this.txt_set_ip.TabIndex = 3;
            // 
            // TcpClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 523);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_recv);
            this.Controls.Add(this.txt_send);
            this.Controls.Add(this.btn_send);
            this.Name = "TcpClientForm";
            this.Text = "TCP客户端采集测试工具";
            this.Load += new System.EventHandler(this.TcpClientForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.RichTextBox txt_send;
        private System.Windows.Forms.RichTextBox txt_recv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_send_circle_time;
        private System.Windows.Forms.CheckBox ckb_send_circle;
        private System.Windows.Forms.CheckBox ckb_send_hex;
        private System.Windows.Forms.CheckBox ckb_send_autoclean;
        private System.Windows.Forms.CheckBox ckb_send_file;
        private System.Windows.Forms.Button btn_clean;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.CheckBox ckb_recv_stop;
        private System.Windows.Forms.CheckBox ckb_recv_hex;
        private System.Windows.Forms.CheckBox ckb_recv_newline;
        private System.Windows.Forms.CheckBox ckb_recv_autosave;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt_set_port;
        private System.Windows.Forms.TextBox txt_set_ip;
    }
}