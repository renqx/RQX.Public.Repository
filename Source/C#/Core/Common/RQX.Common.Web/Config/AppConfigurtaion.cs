﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using RQX.Common.Web.Tools;
using System;
using System.Collections.Generic;

namespace RQX.Common.Web.Config
{
    public class AppConfigurtaion
    {
        private static IConfiguration Configuration { get; set; }
        static AppConfigurtaion()
        {
            //ReloadOnChange = true 当appsettings.json被修改时重新加载            
            Configuration = new ConfigurationBuilder()
            .Add(new JsonConfigurationSource { Path = "appsettings.json", ReloadOnChange = true })
            .Build();
        }

        public static string GetConfigValue(string key)
        {
            return Configuration[key];
        }

        public static int? GetConfigValueInt(string key)
        {
            var value = GetConfigValue(key);
            if (value.IsNotNullOrEmpty())
            {
                return Convert.ToInt32(value);
            }
            else
            {
                return null;
            }
        }

        public static IEnumerable<KeyValuePair<string, string>> GetAllConfigValue()
        {
            return Configuration.AsEnumerable();
        }
    }
}
