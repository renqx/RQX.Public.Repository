﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Logger;

namespace Probe_Diy
{
    public class Probe : AbstractComCollection
    {
        #region 初始属性
        public override DeviceType DeviceType => DeviceType.麻醉机;
        public override int BaudRate => 115200;

        public override int DataBits => 8;

        public override StopBits StopBits => StopBits.One;

        public override Parity Parity => Parity.Even;

        public override bool DTR => false;

        public override bool RTS => false;
        #endregion
        public Probe() { }
        public Probe(StructureModel<MyCom> structureModel) : base(structureModel)
        {
        }
        protected override Dictionary<string, string> AnalysisData(string msg)
        {
            var dic = new Dictionary<string, string>();
            var buffer = msg.HexStringToByte();
            var str = buffer.ToUTF8();
            try
            {
                //pplat=0,freq=12,etco2=269,vte=422,ppeak=22,pmean=7,peep=0,mv=512,ie=11,freqset=12,o2flow=12,fio2=89
                str = str.Substring(4, str.Length - 8);
                var list = str.Split(',');
                list.ForEach(item =>
                {
                    var itemList = item.Split('=');
                    if (itemList.Length == 2)
                    {
                        var key = itemList[0];
                        var value = itemList[1];
                        if (key.Equals("etco2") || key.Equals("o2flow")) value = (Convert.ToDouble(value) / 10.0).ToString();
                        if (key.Equals("ie"))
                        {
                            var ie = Convert.ToInt32(value);
                            var v = Math.Abs(ie) / 10.0 + 1;
                            if (ie > 0)
                            {
                                value = $"1:{v}";
                            }
                            else
                            {
                                value = $"{v}:1";
                            }
                        }
                        dic.Add(key, value);
                    }
                });
            }
            catch (Exception ex)
            {
                LogUtils.WriteErrorLog($"{str}\n{ex}", _localIP);
            }
            return dic;
        }

        protected override StepType CheckRecvMsgType(string lineStr)
        {
            lineStr = lineStr.HexStringToByte().ToUTF8();
            if (lineStr.StartsWith("EFEF") && lineStr.EndsWith("FEFE") && lineStr.LastIndexOf("EFEF") == 0)
            {
                return StepType.DataSource;
            }
            else
            {
                return StepType.None;
            }

        }

        protected override void CircleSend() { }

        protected override void Connect(StepType stepNum, string msg) { }

        protected override string CutToOneMsg(StringBuilder sb)
        {//EFEFpplat=0,freq=14,etco2=23,vte=1C1,ppeak=11,pmean=2,peep=0,mv=386,ie=A,freqset=1E,o2flow=8,fio2=28FEFE
            var str = "";
            try
            {
                str = sb.ToString().HexStringToByte().ToUTF8();
                //Console.WriteLine($"recv:{str}");
                var index = str.LastIndexOf("FEFE");
                if (index == -1)
                {
                    str = "";
                }
                else
                {
                    str = str.Substring(0, index + 4).UTF8ToByte().ToHex();
                }
            }
            catch (Exception ex)
            {
                LogUtils.WriteErrorLog($"{sb.ToString().HexStringToByte().ToUTF8()}\n{ex}", "127.0.0.1");
            }

            return str;
        }
    }
}
