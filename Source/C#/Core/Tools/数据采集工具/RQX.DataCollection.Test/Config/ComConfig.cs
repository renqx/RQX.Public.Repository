﻿using RQX.Common.Core.Config;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Test.Config
{
    /// <summary>
    /// Com串口配置
    /// </summary>
    public class ComConfig : CommonConfig
    {
        public const string _ConfigPath = "COM";
        /// <summary>
        /// 端口号
        /// </summary>
        public string PortName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRate { get; set; }
        /// <summary>
        /// 校验位
        /// </summary>
        public Parity Parity { get; set; }
        /// <summary>
        /// 数据位
        /// </summary>
        public int DataBits { get; set; }
        /// <summary>
        /// 停止位
        /// </summary>
        public StopBits StopBits { get; set; }
        /// <summary>
        /// 握手协议DTR
        /// </summary>
        public bool DTR { get; set; }
        /// <summary> 
        /// 握手协议RTS
        /// </summary>
        public bool RTS { get; set; }

        public override void SaveConfig(string path = _ConfigPath)
        {
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:PortName", PortName);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:BaudRate", BaudRate);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Parity", Parity);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:DataBits", DataBits);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:StopBits", StopBits);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:DTR", DTR);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:RTS", RTS);
            base.SaveConfig(path);
            ConfigUtils.SaveToFile();
        }
    }
}
