﻿using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Net;
using RQX.Common.Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLT_HL7SDK
{
    class Program
    {
        static void Main(string[] args)
        {
            //var sdk = new SDK();
            //sdk.Init();
            LogUtils.WriteDebugLog("test"); 

            var hl7sdk = new HL7SDK(new StructureModel<TcpSocketClient>()
            {
                Conn = null,
                LocalIP = "192.168.203.11",
                MaxRecvTime = 30 * 1000,
                Ext1 = "192.168.203.11",
            });
            hl7sdk.Start();

            Console.ReadKey();
        }
    }
}
