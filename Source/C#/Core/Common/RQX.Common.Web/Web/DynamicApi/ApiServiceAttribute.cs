﻿using System;

namespace RQX.Common.Web.Web.DynamicApi
{
    public class ApiServiceAttribute : Attribute
    {
        public string AreaName;
        public ApiServiceAttribute() : this("") { }
        public ApiServiceAttribute(string areaName)
        {
            AreaName = areaName;
        }
    }
}
