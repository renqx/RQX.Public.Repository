﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity.Interface
{
    /// <summary>
    /// 包含软删除信息
    /// </summary>
    public interface IDelete
    {
        /// <summary>
        /// 能否被删除
        /// </summary>
        public bool CanDele { get; set; }
        /// <summary>
        /// 是否已删除
        /// </summary>
        public bool IsRemove { get; set; }
    }
}
