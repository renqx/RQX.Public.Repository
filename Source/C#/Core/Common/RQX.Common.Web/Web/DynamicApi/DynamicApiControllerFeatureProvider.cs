﻿using Microsoft.AspNetCore.Mvc.Controllers;
using RQX.Common.Web.Tools;
using System.Reflection;

namespace RQX.Common.Web.Web.DynamicApi
{
    public class DynamicApiControllerFeatureProvider : ControllerFeatureProvider
    {
        /// <summary>
        /// 判断是否是controller
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <returns></returns>
        protected override bool IsController(TypeInfo typeInfo)
        {
            var type = typeInfo.AsType();
            if (!typeof(IApiService).IsAssignableFrom(type) ||
                !typeInfo.IsPublic || typeInfo.IsAbstract || typeInfo.IsGenericType)
            {
                return false;
            }

            if (null == ReflectionHelper.GetSingleAttributeOrDefaultByFullSearch<ApiServiceAttribute>(typeInfo))
            {
                return false;
            }

            if (null != ReflectionHelper.GetSingleAttributeOrDefault<IgnoreApiServiceAttribute>(typeInfo, false))
            {
                return false;
            }

            return true;
        }
    }
}
