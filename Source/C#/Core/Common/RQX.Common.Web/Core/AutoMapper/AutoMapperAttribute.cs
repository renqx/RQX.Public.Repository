﻿using System;

namespace RQX.Common.Web.Core.AutoMapper
{
    /// <summary>
    /// 自定义类型转换
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoMapperAttribute : Attribute
    {
        public Type BaseType;
        public AutoMapperAttribute(Type type)
        {
            BaseType = type;
        }
    }
}
