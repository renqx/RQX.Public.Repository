﻿using RQX.Common.Web.Core.Interface;
using RQX.Common.Web.Core.Valid;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RQX.Common.Web.Core.Entity
{
    /// <summary>
    /// 带创建者的实体
    /// </summary>
    public class CreateUserEntity : BaseEntity, ICreateUser
    {
        /// <summary>
        /// 创建人ID
        /// </summary>
        public int create_user_id { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        [ApolloMaxLength(50)]
        public string create_user_name { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime create_time { get; set; } = DateTime.Now;
    }
}
