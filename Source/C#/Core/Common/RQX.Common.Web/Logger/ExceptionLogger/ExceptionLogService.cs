﻿using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.Container;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Service;
using System;

namespace RQX.Common.Web.Logger.ExceptionLogger
{
    /// <summary>
    /// 异常日志
    /// </summary>
    [IocInjection]
    [IgnoreApiService]
    public class ExceptionLogService : BaseLoggerService<ExceptionLog, ExceptionLogDto, BasePagger>
    {
        public ExceptionLogService(CommonDbContext dbContext) : base(dbContext)
        {
        }
        /// <summary>
        /// 新增异常
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override ExceptionLog Add(ExceptionLog entity)
        {
            entity.code = Guid.NewGuid().ToString().Replace("-", "");
            return base.Add(entity);
        }
    }
}
