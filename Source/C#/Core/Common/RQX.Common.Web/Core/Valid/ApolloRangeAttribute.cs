﻿using RQX.Common.Web.Tools;
using System;
using System.ComponentModel.DataAnnotations;

namespace RQX.Common.Web.Core.Valid
{
    public class ApolloRangeAttribute : RangeAttribute
    {
        private const string DefaultErrorMessage = "参数{0}的值需要介于{1}和{2}之间";

        public ApolloRangeAttribute(double minimum, double maximum) : base(minimum, maximum) { }
        public ApolloRangeAttribute(int minimum, int maximum) : base(minimum, maximum) { }
        public ApolloRangeAttribute(Type type, string minimum, string maximum) : base(type, minimum, maximum) { }
        public ApolloRangeAttribute(double minimum, double maximum, string errorMessage) : this(minimum, maximum)
        {
            ErrorMessage = errorMessage;
        }
        public ApolloRangeAttribute(int minimum, int maximum, string errorMessage) : this(minimum, maximum)
        {
            ErrorMessage = errorMessage;
        }
        public ApolloRangeAttribute(Type type, string minimum, string maximum, string errorMessage) : this(type, minimum, maximum)
        {
            ErrorMessage = errorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            if (ErrorMessage.IsNullOrEmpty())
            {
                return string.Format(DefaultErrorMessage, name, Minimum, Maximum);
            }
            else
            {
                return ErrorMessage;
            }
        }
    }
}
