﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    /// <summary>
    /// TCP服务端采集抽象类
    /// </summary>
    public abstract class AbstractTcpServerCollection : AbstractBaseCollection<TcpSocketServer>
    {
        protected AbstractTcpServerCollection(StructureModel<TcpSocketServer> structureModel) : base(structureModel)
        {
            _conn.HandleServerAccepted = (socket) => Connect(StepType.Init);
            _conn.HandleMsgReceived = (c, buffer) => RecvData(buffer);
        }

        public override void Start(bool isUseReStart = true)
        {
            _conn.Start();
            Thread.Sleep(1000);
            base.Start(isUseReStart);
        }

        public override void Dispose()
        {
            _conn.Close();
            base.Dispose();
        }

        protected override void Send(byte[] buffer)
        {
            var socket = _conn._socketList.FirstOrDefault().Value;
            WriteSendLogMethod(BufferTransferToString(buffer));
            socket.Send(buffer);
        }

    }
}
