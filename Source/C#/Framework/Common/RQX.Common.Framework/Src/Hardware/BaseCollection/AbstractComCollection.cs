﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.Com;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    /// <summary>
    /// COM采集抽象类
    /// </summary>
    public abstract class AbstractComCollection : AbstractBaseCollection<MyCom>
    {
        protected AbstractComCollection(StructureModel<MyCom> structureModel) : base(structureModel)
        {
            _conn.BindRecvedHandler(RecvData);
        }

        public override void Start(bool isUseReStart = true)
        {
            _conn.Open();
            Thread.Sleep(1000);
            base.Start(isUseReStart);
        }

        public override void Dispose()
        {
            _conn.Close();
            base.Dispose();
        }

        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn.Send(buffer);
        }

    }
}
