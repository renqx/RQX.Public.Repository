﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace BLT_HL7SDK
{
    public struct GLOBALDATA
    {
        public static readonly int MODULE_NAME_LEN = 8;        // length of module name
        public static readonly int PARAM_NAME_LEN = 32;        // length of param name 
        public static readonly int PATIENT_ID_LEN = 20;        // length of patient ID
        public static readonly int MAX_BEDS_NUM = 67;          // max the number of bed
        public static readonly int MAX_PARAM_VALUE_ID = 17;    // max index of physiological param
        public static readonly int WAVE_HITS_MAX = (250 + 300); //波心最大采样点数
        public static readonly int MAX_SPO2_BUFLEN = 3;
    }

    // the definition of Message type about HL7
    public enum HL7_MSG_TYPE
    {
        HL7_MSG_TYPE_INVALID = 0,

        HL7_MSG_HEARTBEAT = 1,              // Heart beat(keep status of TCP connection)
        HL7_MSG_QUERY = 2,                  // query message(from client to server)
        HL7_MSG_ASM_BROADCAST = 3,          // broadcast of A monitor
        HL7_MSG_CMS_BROADCAST = 4,          // broadcast of CMS or Gateway
        HL7_MSG_BED_LIST = 5,               // current list on-line beds
        HL7_MSG_PATIENT_INFO = 6,           // patient information
        HL7_MSG_QRY_SUCCESS = 7,            // response message from server when client's query is successful
        HL7_MSG_QRY_FAIL = 8,               // response message from server when client's query is unsuccessful
        HL7_MSG_PARAM_DATA = 9,             // param data
        HL7_MSG_ALARM_LIMIT = 10,            // alarm limit
        HL7_MSG_ALARM_LEVEL,            // alarm level
        HL7_MSG_PARAM_ALARM,            // physiological alarm information
        HL7_MSG_TECH_ALARM,             // technical alarm information
        HL7_MSG_MODULE_LOAD,            // module plugs
        HL7_MSG_MODULE_UNLOAD,          // module pulls out
        HL7_MSG_PARAM_MODIFY,           // param config modified
        HL7_MSG_WAVE_DATA,              // wave data
    };


   

    //当前监护仪支持的波形通道定义
    public enum tagWV_CHN
    {
	    WV_CHN_INVALID, //无效通道号
	    WV_CHN_I = 1, //I
	    WV_CHN_II,    //II
	    WV_CHN_III,   //III
	    WV_CHN_AVR,   //AVR
	    WV_CHN_AVL,   //AVL
	    WV_CHN_AVF,   //AVF
	    WV_CHN_C1,   //C1
	    WV_CHN_C2,   //C2
	    WV_CHN_C3,   //C3
	    WV_CHN_C4,   //C4
	    WV_CHN_C5,   //C5
	    WV_CHN_C6,   //C6
	    WV_CHN_SPO2, //SpO2
	    WV_CHN_RESP, //resp
	    WV_CHN_IBP1, //IBP1
	    WV_CHN_IBP2, //IBP1
	    WV_CHN_CO2,  //CO2
	    WV_CHN_O2,   //O2
	    WV_CHN_N2O,  //N2O
	    WV_CHN_AA,   //AA
	    WV_CHN_ICG,  //ICG
	    WV_CHN_FHR1, //FHR1
	    WV_CHN_FHR2, //FHR2
	    WV_CHN_TOCO, //TOCO
	    WV_CHN_FM,   //FM
	    WV_CHN_NUM = WV_CHN_FM,  //通道总数
    };



    public enum CIS_DATA_TYPE
    {
        CIS_DATA_INVALID = 0,
        CIS_DATA_PATIENT,
    };

    //work mode of HL7SDK: difference of mode brings difference in calling net port
    public enum HL7_WORK_MODE
    {
        HL7_MODE_NONE = 0,  // no work mode
        HL7_MODE_SERVER_CMS = 1,  // CMS mode(server is CMS)
        HL7_MODE_SERVER_GATEWAY = 2,  // Gateway mode(server is Gateway)
        HL7_MODE_SERVER_MONITOR = 3,  // MONITOR mode(server is A series monitor)
        HL7_MODE_SERVER_CLIENT = 4,  // server & client(only for test)	
        HL7_MODE_CLIENT = 5,  // client mode
    };

    //type of query monitor data
    public enum QUERY_TYPE
    {
        QUERY_PARAM_DATA = 1,
        QUERY_PARAM_WAVE = 2,
        QUERY_ALARM_PARAM = 3,
        QUERY_ALARM_TECH = 4,
        QUERY_PARAM_CONF = 5,
        QUERY_PATIENT_INFO = 6,
        QUERY_BED_LIST = 7,
        QUERY_ALL_DATA = 8
    };

    //index of physiological param
    public enum PARAM_INDEX
    {
        PARAM0 = 0,
        PARAM1 = 1,
        PARAM2 = 2,
        PARAM3 = 3,
        PARAM4 = 4,
        PARAM5 = 5,
        PARAM6 = 6,
        PARAM7 = 7,
        PARAM8 = 8,
        PARAM9 = 9,
        PARAM10 = 10,
        PARAM11 = 11,
        PARAM12 = 12,
        PARAM13 = 13,
        PARAM14 = 14,
        PARAM15 = 15,
        PARAM16 = 16,
        PARAM_MAX,

        MAX_PARAM_ID = 255  //range of all params of a module is 0-254
    };
    [StructLayout(LayoutKind.Sequential)]
    public struct HL7_PATIENT_INFO1
    {
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 40, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] wSurname;            // family name
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 40, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] wName;               // first name
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] byReserve;// = new byte[3];             // reserved
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 16, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] strOfficeName;                 // name of office
        
    };

    //patient information:
    [StructLayout(LayoutKind.Sequential)]
    public struct HL7_PATIENT_INFO
    {
        public bool bNew;                              // flags of new information: client should update patient info when bNew is true
        public UInt32 dwValidFlag;              // valid flag : dwValidFlag from low bit(0) to hight bit(31) 

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strID;                         // patient ID
        /*
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string wSurname;            // family name
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string wName;               // first name
          */

        //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40, ArraySubType = UnmanagedType.I1)]
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 40, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] wSurname;            // family name
        //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40, ArraySubType = UnmanagedType.I1)]
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 40, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] wName;               // first name

        public byte bySex;                    // sex 0-M(male), 1-F(female)
        public byte byType;                   // type:0-N(neonate),1-A(adult),2-P(child),3-UN(unknown)
        public ushort wHeight;                 // height
        public ushort wWeight;                 // weight
        public byte byBloodType;              // blood tyep:0-A，1-B，2-AB，3-O,4-NA
        public byte byPaceSwitch;             // switch of pacer: 0-OFF，1-ON，2-UN(unknown)
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string strOfficeName;                 // name of office
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strRoomNO;                     // room number
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strBedNO;                      // bed number
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string strMonitorName;                // name of monitor
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strBornDate;                   // birthday
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strInHospitalDate;             // in date
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string strDoctorInCharge;             // doctor
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 160)]
        public string strDiagnosis;                 // diagnose
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string strCharacterSet;               // character set
        //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3,ArraySubType = UnmanagedType.I1)]
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.AnsiBStr)]
        public byte[] byReserve;// = new byte[3];             // reserved
        public void copy(HL7_PATIENT_INFO patInf)
        {
            //bNew = patInf.bNew;                        // flags of new information: client should update patient info when bNew is true
            //dwValidFlag = patInf.dwValidFlag;              // valid flag : dwValidFlag from low bit(0) to hight bit(31) 
            strID = patInf.strID;                         // patient ID
            wSurname = new byte[40];
            wName = new byte[40];
            for (int i = 0; i < patInf.wSurname.Length; i++)
            {
                wSurname[i] = patInf.wSurname[i];            // family name
            }
            for (int i = 0; i < patInf.wName.Length; i++)
            {
                wName[i] = patInf.wName[i];               // first name
            }

            //wName = patInf.wName;               // first name
            //bySex = patInf.bySex;                    // sex 0-M(male), 1-F(female)
            //byType = patInf.byType;                   // type:0-N(neonate),1-A(adult),2-P(child),3-UN(unknown)
            //wHeight = patInf.wHeight;                 // height
            //wWeight = patInf.wWeight;                 // weight
            //byBloodType = patInf.byBloodType;              // blood tyep:0-A，1-B，2-AB，3-O,4-NA
            //byPaceSwitch = patInf.byPaceSwitch;             // switch of pacer: 0-OFF，1-ON，2-UN(unknown)
            //strOfficeName = patInf.strOfficeName;                 // name of office
            //strRoomNO = patInf.strRoomNO;                     // room number
            //strBedNO = patInf.strBedNO;                      // bed number
            //strMonitorName = patInf.strMonitorName;                // name of monitor
            //strBornDate = patInf.strBornDate;                   // birthday
            //strInHospitalDate = patInf.strInHospitalDate;             // in date
            //strDoctorInCharge = patInf.strDoctorInCharge;             // doctor
            //strDiagnosis = patInf.strDiagnosis;                 // diagnose
            //strCharacterSet = patInf.strCharacterSet;               // character set
            //byReserve = patInf.byReserve;             // reserved
        }
    };

    //Alarm information(include physiological and technical):
    //1 alarm code please refer <Some ID Definitions of Monitor>
    //2 differentiate the type of alarm by wParamID: Physical-0~70，Technical-71~180，Tips-181~254                                         
    public struct ALARM_INFO
    {
        public ushort wModuleID;        // module ID of param
        public ushort wParamID;         // param ID
        public ushort wAlarmCode;       // alarm code
        public ushort wLevel;	         // level(optional)
    };

    //physiological param data:
    //1 send param value by fixed unit, please refer <Some ID Definitions of Monitor>
    //2 wParamID only for physiological param, refer <Some ID Definitions of Monitor>
    public struct PARAM_DATA
    {
        public UInt16 wParamID;            // param ID
        public UInt16 wModuleID;           // module ID
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string strName;       // param name
        public UInt16 bValid;              // flag of param value: 1-valid, 0-invalid
        public Int16 dwValue;                      // param value
    };

    //bed information of a monitor
    [StructLayout(LayoutKind.Sequential)]
    public struct BED_INFO
    {
        public HL7_PATIENT_INFO PatientInfo; // patient information
        public UInt32 ulIP;        // monitor IP
        public int iBedNO;      // bed number(machine number of monitor)
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string strMonitorName;        // name of monitor
    };

    //server information
    public struct HL7SERVER_INFO
    {
        public UInt32 ulServerIP;          // server IP
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string strName;         // server name

        public BED_INFO pBedList;            // current bed list of on-line monitor
        public int iBedNUm;             // the total number of on-line monitor
    };

    //query message from client(HL7_MSG_QUERY)
    public class HL7QRY_DATA
    {
        public string QueryID;       // Query ID: 
        public QUERY_TYPE QueryType;   // always 1 query all monitor data, else error
        public UInt32 dwDestIP; // destination IP, dwDestIP should be zero when Server is A series monitor
        public string[] cPatID = new string[GLOBALDATA.MAX_BEDS_NUM];
        public int nCount;
    };

    //Heart beat for keeping satus of TCP connection(HL7_MSG_HEARTBEAT)
    public struct HEART_BEAT_DATA
    {
        public UInt32 dwPartIP;     //IP address of the other side of TCP connection
        public ushort wBeatError;  //0-normal, 1-error
        public ushort wTimeOut;    //0-normal, 1-time out
    };

    //current list on-line beds(HL7_MSG_BED_LIST)
    [StructLayout(LayoutKind.Sequential)]
    public struct HL7BED_LIST
    {
        public UInt32 dwOnLineNum;       // the number of on-line beds
        //[MarshalAs(UnmanagedType.SysInt, SizeConst = 67)]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 67)]
        public BED_INFO[] BedList;// = new BED_INFO[GLOBALDATA.MAX_BEDS_NUM];  // bed information: bed is on-line when ulIP of BED_INFO is nonzero
    };

    //information of one module plug(HL7_MSG_MODULE_LOAD)
    public class HL7MODULE_LOAD
    {
        public ushort wID;  // module ID
        public ushort[] wParamID = new ushort[(int)PARAM_INDEX.PARAM_MAX];  //loading param: the index of array is param ID, the value of array:true-this param is exist, false-param is not exist
    };

    //information when module pulls out(HL7_MSG_MODULE_UNLOAD) 
    public struct HL7MODULE_UNLOAD
    {
        public ushort wID;  // the ID of module pulled out
    };

    //information when param config modified(HL7_MSG_PARAM_MODIFY)
    public class HL7PARAM_MODIFY
    {
        public ushort wID;  // module ID
        public ushort[] wLoadParam = new ushort[(int)PARAM_INDEX.PARAM_MAX];    //param of loading: the index of array is param ID; the value of array: true-param load, false-ignore
        public ushort[] wUnloadParam = new ushort[(int)PARAM_INDEX.PARAM_MAX];  //param of pulling out: the index of array is param ID; the value of array:TRUE-param pull out，FALSE-ignore
    };

    //response when query failed
    public struct HL7QRY_ACK
    {
        public string strAckCode;
        public string strCtrlID;
        public string strErrorTip;  //error information
    };

    // alarm limit information
    public struct HL7PARAM_LIMIT
    {
        public ushort wParamID;           // param ID
        public ushort wModuleID;          // module ID of param
        public ushort wLimitHi;           // up limit
        public ushort wLimitLo;           // low limit
    };

    //alarm level information
    public struct HL7PARAM_LEVEL
    {
        public ushort wParamID;           // param ID
        public ushort wModuleID;          // module ID of param
        public ushort wLevelHi;           // level of up limit
        public ushort wLevelLo;           // level of low limit
    };

    //波形数据更新结构---静态内存,客户端可以直接使用
    
    //[StructLayout(LayoutKind.Sequential)]
    //public class WAVE_DATA_S
    //{
    //    public ushort wWaveChanel;          //波形通道号(使用WV_CHN赋值)
    //    public ushort wModuleID;            //参数所属模块ID
    //    public string strTime;                    //波形时间
    //    public string strLabel;       //波形标签
    //    public ushort wHits;                //采样频率
    //    public ushort wMaxWaveValue;        //波形最大值
    //    public ushort wMinWaveValue;        //波形最小值
    //    public short[] WaveData = new short[GLOBALDATA.WAVE_HITS_MAX];            //波形采样值
    //    public ushort wValidLen;            //有效         
    //};
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVE_DATA_S
    {
        public ushort wWaveChanel;          //波形通道号(使用WV_CHN赋值)
        public ushort wModuleID;            //参数所属模块ID

        //char strTime[20];                    //波形时间
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] strTime;                         // 波形时间

        //char strLabel[PARAM_NAME_LEN];       //波形标签
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] strLabel;

        public ushort wHits;                //采样频率
        public ushort wMaxWaveValue;        //波形最大值
        public ushort wMinWaveValue;        //波形最小值

        //short WaveData[WAVE_HITS_MAX];            //波形采样值
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 550)]
        public short[] WaveData;

        public ushort wValidLen;            //有效         
    }
    //当前监护仪支持的波形通道定义
    public enum WV_CHN
    {
        WV_CHN_INVALID, //无效通道号
        WV_CHN_I = 1, //I
        WV_CHN_II,    //II
        WV_CHN_III,   //III
        WV_CHN_AVR,   //AVR
        WV_CHN_AVL,   //AVL
        WV_CHN_AVF,   //AVF
        WV_CHN_C1,   //C1
        WV_CHN_C2,   //C2
        WV_CHN_C3,   //C3
        WV_CHN_C4,   //C4
        WV_CHN_C5,   //C5
        WV_CHN_C6,   //C6
        WV_CHN_SPO2, //SpO2
        WV_CHN_RESP, //resp
        WV_CHN_IBP1, //IBP1
        WV_CHN_IBP2, //IBP1
        WV_CHN_CO2,  //CO2
        WV_CHN_O2,   //O2
        WV_CHN_N2O,  //N2O
        WV_CHN_AA,   //AA
        WV_CHN_ICG,  //ICG
        WV_CHN_FHR1, //FHR1
        WV_CHN_FHR2, //FHR2
        WV_CHN_TOCO, //TOCO
        WV_CHN_FM,   //FM
        WV_CHN_NUM = WV_CHN_FM,  //通道总数
    };
    //XS 2017-04-27
    enum MONITOR_MODULE_ID
    {
        MODULE_BEGIN,

        MODULE_ECG = 1,
        MODULE_SPO2,
        MODULE_NIBP,
        MODULE_RESP,
        MODULE_CO2,
        MODULE_ICG,
        MODULE_BIS,
        MODULE_AA,
        MODULE_O2,
        MODULE_N2O,
        MODULE_PR,
        MODULE_TD,
        MODULE_CO,
        MODULE_CPP,
        MODULE_SPO2L,
        MODULE_TSPO2,

        MODULE_P1 = 20,
        MODULE_P2,
        MODULE_ART,
        MODULE_CVP,
        MODULE_PA,
        MODULE_RAP,
        MODULE_LAP,
        MODULE_ICP,

        MODULE_T1 = 50,
        MODULE_T2,
        MODULE_ECO,
        MODULE_NASO,
        MODULE_TYMP,
        MODULE_RECT,
        MODULE_BLAD,
        MODULE_SKIN,

        MODULE_FHR = 60,
        MODULE_CTG,

        MODULE_SYSTEM = 124,
        MODULE_RECORDER,
        MODULE_PRINTER,

        MODULE_END
    };

    //定义函数委托（函数指针）
    //the TCP connection from one bed has built, data can be received
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnConnectDelgate(int iBedNO);
    //the TCP connection from one bed has disconnected, abnormal condition happened
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnDisconnectDelgate(int iBedNO);
    //received original HL7 message of one bed(iBedNO), such as 0x0B......0x1C 0x0D
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnRecvHL7MsgDelgate(int iBedNO, IntPtr pMsg, int iLen, int iMsgType);
    //sent original HL7 message of one bed(iBedNO), such as 0x0B......0x1C 0x0D
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnSendHL7MsgDelgate(int iBedNO, IntPtr pMsg, int iLen, int iMsgType);
    // receive and process of monitor data: for client when HL7SDK received HL7 message, it will call this function and update the data of bed(iBedNO) to client
    // iBedNO-bed number,function will update monitor data of this bed
    // pData-begin address of monitor data which is formated as above class
    // iLen-date length
    // iDataType-data type:homologous with above enum HL7_MSG_TYPE
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnRecvMonitorDataDelgate(int iBedNO, IntPtr pData, int iLen, int iDataType);
    //get Patinfo
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetPatientInfoDelgate(HL7_PATIENT_INFO[] stPatInfo, bool bUpdate, int nMacNO = 0);
    //recv patID
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnRecvPatIDDelgate(string strID, int nCount);

    public struct HL7SDK_INTERFACE
    {
        //定义委托的函数指针
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnConnectDelgate OnConnect;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnDisconnectDelgate OnDisconnect;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnRecvHL7MsgDelgate OnRecvHL7Msg;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnSendHL7MsgDelgate OnSendHL7Msg;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnRecvMonitorDataDelgate OnRecvMonitorData;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public GetPatientInfoDelgate GetPatientInfo;
        [MarshalAs(UnmanagedType.FunctionPtr)]
        public OnRecvPatIDDelgate OnRecvPatID;
        public void Init()
        {
            //OnConnect = new OnConnectDelgate(SDK.OnConnectFunc);
            //OnDisconnect = new OnDisconnectDelgate(SDK.OnDisconnectFunc);
            //OnRecvHL7Msg = new OnRecvHL7MsgDelgate(SDK.OnRecvHL7MsgFunc);
            //OnSendHL7Msg = new OnSendHL7MsgDelgate(SDK.OnSendHL7MsgFunc);
            //OnRecvMonitorData = new OnRecvMonitorDataDelgate(SDK.OnRecvMonitorDataFunc);
        }
    };

    public class HL7SDK_Model
    {
        [DllImport("HL7SDK.dll", EntryPoint = "InitHL7SDK", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
        public extern static int InitHL7SDK(IntPtr pHL7SDKInterace, HL7_WORK_MODE enHL7ServerMode, UInt32 ulServerIP, UInt32 ulLocalIP);
        [DllImport("HL7SDK.dll", EntryPoint = "ExitHL7SDK", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
        public extern static int ExitHL7SDK();
        [DllImport("HL7SDK.dll", EntryPoint = "GetHL7ServerInfo", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
        public extern static int GetHL7ServerInfo(HL7SERVER_INFO pServerInfo);
        [DllImport("HL7SDK.dll", EntryPoint = "UpdateCISData", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
        public extern static void UpdateCISData(int iBedNO, IntPtr pData, int iLen, int iDataType);
        [DllImport("HL7SDK.dll", EntryPoint = "SendPatToBedSDK", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
        public extern static int SendPatToBedSDK(int iBedNO, IntPtr pData);
    }
    public class HL7DataValType
    {
        public byte[] RecvBuf = new Byte[10240];
        public int nDataType = 0;
        public int nDataLen = 0;
        public int nBedNo = 0;
    };
}