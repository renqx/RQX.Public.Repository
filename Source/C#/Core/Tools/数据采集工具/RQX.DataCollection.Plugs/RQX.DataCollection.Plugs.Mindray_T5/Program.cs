﻿using RQX.Common.Core.Db.SqlServer;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Net;
using RQX.Common.Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.DataCollection.Plugs.Mindray_T5
{
    class Program
    {
        //public static BaseSqlHelper sqlHelper = new BaseSqlHelper("Data Source=172.16.1.89;Initial Catalog=ACIS;User Id=sa;Password=123.com");

        static void Main(string[] args)
        {
            //var sql = "SELECT count(1) FROM [dbo].[TH_BUSI_OBS_DATA] ";
            //while (true)
            //{
            //    try
            //    {
            //        var value = sqlHelper.ExcuteSingleResult(sql);
            //        LogUtils.WriteLogToConsole(value);
            //    }
            //    catch (Exception ex)
            //    {
            //        LogUtils.WriteLogToConsole(ex.Message);
            //    }
            //    Thread.Sleep(1000);
            //}
            //return;

            string localIP = "192.168.1.1";
            string ip = "192.168.0.11";
            if (args.Length > 0)
            {
                ip = args[0];
            }
            int port = 4601;

            T5 t5 = new T5(new StructureModel<TcpSocketClient>()
            {
                Conn = new TcpSocketClient(ip, port),
                LocalIP = localIP,
                MaxRecvTime = 1 * 30 * 1000
            });
            //t5.ShowMsgHandler = (msg) => Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss}\t{msg}");
            t5.DealDataHandler = (dic) => dic.ForEach(item => Console.WriteLine($"{item.Key}\t{item.Value}"));
            t5.Start();
            Console.ReadKey();
        }
    }
}
