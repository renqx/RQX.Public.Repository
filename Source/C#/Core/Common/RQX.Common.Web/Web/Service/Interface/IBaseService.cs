﻿using Microsoft.EntityFrameworkCore.Storage;
using RQX.Common.Web.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace RQX.Common.Web.Web.Service.Interface
{
    public interface IBaseService<TEntity>
        where TEntity : BaseEntity
    {
        void Include(Expression<Func<TEntity, object>> expression, params Expression<Func<object, object>>[] thenInclude);
        void Commit();
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> WhereIf(bool isRun, Expression<Func<TEntity, bool>> expression);
        IEnumerable<TEntity> DoFilter(IEnumerable<TEntity> entities, Func<IQueryable<TEntity>, IQueryable<TEntity>> func);
        IDbContextTransaction GetBeginTransaction();
        IEnumerable<TEntity> Find(params int[] primarykeyList);
    }
}
