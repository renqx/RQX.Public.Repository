﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity.Interface
{
    /// <summary>
    /// 包含更新的信息
    /// </summary>
    public interface IUpdateUser : ICreateUser
    {
        /// <summary>
        /// 更新者ID
        /// </summary>
        int? UpdateUserId { get; set; }
        /// <summary>
        /// 更新者名字
        /// </summary>
        string UpdateUserName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        DateTime? UpdateTime { get; set; }
    }
}
