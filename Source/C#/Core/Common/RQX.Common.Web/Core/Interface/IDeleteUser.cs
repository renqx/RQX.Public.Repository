﻿using System;

namespace RQX.Common.Web.Core.Interface
{
    /// <summary>
    /// 包含删除的信息
    /// </summary>
    public interface IDeleteUser : IUpdateUser, IDelete
    {
        /// <summary>
        /// 删除者ID
        /// </summary>
        int? delete_user_id { get; set; }
        /// <summary>
        /// 删除者名字
        /// </summary>
        string? delete_user_name { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        DateTime? delete_time { get; set; }
    }
}
