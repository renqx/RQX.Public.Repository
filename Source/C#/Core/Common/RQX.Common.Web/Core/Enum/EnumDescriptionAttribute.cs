﻿using System;

namespace RQX.Common.Web.Core.Enum
{
    /// <summary>
    /// 枚举名称
    /// </summary>
    public class EnumDescriptionAttribute : Attribute
    {
        public string Name { get; set; }
        public EnumDescriptionAttribute(string Name)
        {
            this.Name = Name;
        }
    }
}
