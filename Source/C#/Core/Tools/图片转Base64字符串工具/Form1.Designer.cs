﻿namespace 图片转Base64字符串工具
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_file = new System.Windows.Forms.TextBox();
            this.rtxt_content = new System.Windows.Forms.RichTextBox();
            this.btn_change = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_file
            // 
            this.txt_file.Location = new System.Drawing.Point(13, 13);
            this.txt_file.Name = "txt_file";
            this.txt_file.Size = new System.Drawing.Size(385, 23);
            this.txt_file.TabIndex = 0;
            this.txt_file.TextChanged += new System.EventHandler(this.txt_file_TextChanged);
            // 
            // rtxt_content
            // 
            this.rtxt_content.Location = new System.Drawing.Point(13, 42);
            this.rtxt_content.Name = "rtxt_content";
            this.rtxt_content.Size = new System.Drawing.Size(466, 396);
            this.rtxt_content.TabIndex = 1;
            this.rtxt_content.Text = "";
            // 
            // btn_change
            // 
            this.btn_change.Location = new System.Drawing.Point(404, 12);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(75, 23);
            this.btn_change.TabIndex = 2;
            this.btn_change.Text = "转换";
            this.btn_change.UseVisualStyleBackColor = true;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 450);
            this.Controls.Add(this.btn_change);
            this.Controls.Add(this.rtxt_content);
            this.Controls.Add(this.txt_file);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_file;
        private System.Windows.Forms.RichTextBox rtxt_content;
        private System.Windows.Forms.Button btn_change;
    }
}

