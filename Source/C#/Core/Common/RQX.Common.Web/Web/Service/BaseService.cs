﻿using RQX.Common.Web.Core.Dto;
using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;

namespace RQX.Common.Web.Web.Service
{
    public class BaseService<TEntity, TDto, TPagger> : BaseDtoService<TEntity, TDto, TPagger>
        where TEntity : BaseEntity
        where TDto : BaseDto
        where TPagger : IPagger
    {
        public BaseService(BaseDbContext context) : base(context) { }
    }
}
