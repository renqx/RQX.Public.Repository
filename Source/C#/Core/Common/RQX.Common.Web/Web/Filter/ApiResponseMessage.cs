﻿namespace RQX.Common.Web.Web.Filter
{
    public class ApiResponseMessage
    {
        public ApiResponseMessage() : this("") { }
        public ApiResponseMessage(string message) : this(message, null) { }
        public ApiResponseMessage(string message, object data)
        {
            this.Message = message;
            this.Data = data;
        }
        public string Message { get; set; }
        public object Data { get; set; }

        public ApiResponse ToApiResponse()
        {
            return new ApiResponse(200, Message, Data);
        }
    }
}
