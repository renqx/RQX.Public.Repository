﻿using Microsoft.AspNetCore.Mvc;
using RQX.Common.Web.Web.Container;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Filter;

namespace RQX.Common.Web.Web.Service
{
    /// <summary>
    /// 基础service，不包装任何entity
    /// </summary>
    [ServiceFilter(typeof(ApiResponseFilterAttribute))]
    [ServiceFilter(typeof(ApiExceptionFilterAttribute))]
    [IgnoreApiService]
    [ApiService]
    public class ApiService : IApiService, IIoCManager
    {
        public ApiService() { }
        public ApiService(AutoInjectionService autoInjectionService)
        {
            autoInjectionService?.AutoInjection(this);
        }
    }
}
