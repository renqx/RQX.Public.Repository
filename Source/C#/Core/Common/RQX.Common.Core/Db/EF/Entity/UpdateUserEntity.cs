﻿using RQX.Common.Core.Db.EF.Entity.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity
{
    /// <summary>
    /// 带修改者的实体
    /// </summary>
    public class UpdateUserEntity : CreateUserEntity, IUpdateUser
    {
        /// <summary>
        /// 更新者ID
        /// </summary>
        [Column("update_user_id")]
        public int? UpdateUserId { get; set; }
        /// <summary>
        /// 更新者名字
        /// </summary>
        [Column("update_user_name")]
        [MaxLength(50)]
        public string UpdateUserName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("update_time", TypeName = "datetime")]
        public DateTime? UpdateTime { get; set; }
    }
}
