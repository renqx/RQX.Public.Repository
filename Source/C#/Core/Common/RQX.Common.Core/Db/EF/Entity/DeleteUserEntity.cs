﻿using RQX.Common.Core.Db.EF.Entity.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity
{
    /// <summary>
    /// 带删除者的实体
    /// </summary>
    public class DeleteUserEntity : UpdateUserEntity, IDeleteUser, IDelete
    {
        /// <summary>
        /// 删除者ID
        /// </summary>
        [Column("delete_user_id")]
        public int? DeleteUserId { get; set; }
        /// <summary>
        /// 删除者名字
        /// </summary>
        [Column("delete_user_name")]
        [MaxLength(50)]
        public string DeleteUserName { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        [Column("delete_time", TypeName = "datetime")]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 能否被删除
        /// </summary>
        [Column("can_dele")]
        public bool CanDele { get; set; } = true;
        /// <summary>
        /// 是否已删除
        /// </summary>
        [Column("is_remove")]
        public bool IsRemove { get; set; } = false;
    }
}
