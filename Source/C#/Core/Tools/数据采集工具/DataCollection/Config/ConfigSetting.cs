﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace DataCollection.Config
{
    public partial class ConfigSetting : Form
    {
        private bool IsInit = true;
        public ConfigSetting()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigSetting_Load(object sender, EventArgs e)
        {
            //绑定IP
            List<string> IpList = new List<string>();
            string HostName = Dns.GetHostName(); //得到主机名
            IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
            for (int i = 0; i < IpEntry.AddressList.Length; i++)
            {
                if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                {
                    IpList.Add(IpEntry.AddressList[i].ToString());
                }
            }
            ddl_localIP.DataSource = IpList;
            var index = ddl_localIP.Items.IndexOf(LocalConfig.info.Name);
            if (index > 0)
            {
                ddl_localIP.SelectedIndex = index;
            }
            if (!ddl_localIP.Text.Equals(LocalConfig.info.Name)) ddl_localIP_SelectedIndexChanged(null, null);
            //绑定开机自启动
            ckb_run_poweron.Checked = PowerOnRunConfig.CheckIsSet();
            //绑定托盘启动
            ckb_notifyicon_run.Checked = LocalConfig.info.NotifyIconRun;
            //绑定插件采集自启动
            ckb_autorun_plug.Checked = LocalConfig.info.AutoRunPlug;
        }
        /// <summary>
        /// 自启动设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_run_poweron_CheckedChanged(object sender, EventArgs e)
        {
            PowerOnRunConfig.Set(ckb_run_poweron.Checked, false);
        }
        /// <summary>
        /// 托盘启动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_notifyicon_run_CheckedChanged(object sender, EventArgs e)
        {
            LocalConfig.info.NotifyIconRun = ckb_notifyicon_run.Checked;
            LocalConfig.Save();
        }
        /// <summary>
        /// 自动运行插件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_autorun_plug_CheckedChanged(object sender, EventArgs e)
        {
            LocalConfig.info.AutoRunPlug = ckb_autorun_plug.Checked;
            LocalConfig.Save();
        }
        /// <summary>
        /// IP改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddl_localIP_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsInit)
            {
                IsInit = false;
                return;
            }
            LocalConfig.info.Name = ddl_localIP.Text;
            LocalConfig.Save();
        }
    }
}
