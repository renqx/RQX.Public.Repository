﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RQX.Common.Framework.File;
using RQX.Common.Framework.Logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Test.Logger
{
    [TestClass]
    public class LogUtilsTest
    {
        [TestMethod]
        public void 测试日志目录路径构建与基础功能()
        {
            Thread.Sleep(1000);
            var baseDir = "DebugLog";
            var ipName = "127.0.0.1";
            var msg = "WriteDebugLog";
            FileUtils.DeletePath(baseDir, true);
            var path = LogUtils.BuildPath(baseDir, ipName);
            LogUtils.WriteDebugLog(msg, ipName, baseDir);
            Assert.IsTrue(System.IO.File.Exists(path));
        }
        /// <summary>
        /// 调试日志记录，并发性测试
        /// </summary>
        [TestMethod]
        public void 测试日志记录并发记录是否完整()
        {
            Thread.Sleep(1000);
            var baseDir = "DebugLog";
            var ipName = "127.0.0.1";
            var totalCount = 1000;

            FileUtils.DeletePath(baseDir, true);
            var path = LogUtils.BuildPath(baseDir, ipName);
            for(var i = 0; i < totalCount; i++)
            {
                LogUtils.WriteDebugLog(i.ToString(), ipName, baseDir);
            }
            Assert.IsTrue(System.IO.File.Exists(path));
            int count = 0;
            Thread.Sleep(1000);
            using(FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    var str = sr.ReadLine();
                    while (str != null)
                    {
                        count++;
                        str = sr.ReadLine();
                    }
                }
            }
            Assert.IsTrue(totalCount == count,$"文件中的记录有：{count}条");
        }


    }
}
