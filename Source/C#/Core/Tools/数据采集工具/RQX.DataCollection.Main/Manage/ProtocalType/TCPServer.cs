﻿using Microsoft.Extensions.Configuration;
using RQX.Common.Core.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Manage.ProtocalType
{
    /// <summary>
    /// TCP服务端
    /// </summary>
    public class TCPServer
    {
        /// <summary>
        /// 本机IP
        /// </summary>
        public string LocalIP { get; set; }
        /// <summary>
        /// 监听端口
        /// </summary>
        public int ListenPort { get; set; }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="path"></param>
        public void Save(IConfigurationSection section)
        {
            section.SetValue($"LocalIP", LocalIP);
            section.SetValue($"ListenPort", ListenPort);
        }
        /// <summary>
        /// 获取参数字符串
        /// </summary>
        /// <returns></returns>
        public string GetParamStr() => $"{LocalIP}:{ListenPort}";
    }
}
