﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    /// <summary>
    /// 协议类型
    /// </summary>
    public enum ProType
    {
        UDP,
        TCPClient,
        TCPServer,
        COM
    }
}
