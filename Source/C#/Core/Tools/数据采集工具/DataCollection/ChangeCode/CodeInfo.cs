﻿using RQX.Common.Core.Config;
using RQX.Common.Core.Extension;
using System.Collections.Generic;

namespace DataCollection.ChangeCode
{
    /// <summary>
    /// 对码配置类
    /// </summary>
    public class CodeInfo
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        public string DeviceType { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }
        /// <summary>
        /// 上限
        /// </summary>
        public string High { get; set; }
        /// <summary>
        /// 下限
        /// </summary>
        public string Low { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// 是否自动生成
        /// </summary>
        public bool IsRandom { get; set; }
        /// <summary>
        /// 是否生成
        /// </summary>
        public bool IsUse { get; set; }

        public List<SourceCode> SourceCode;
        public static List<CodeInfo> list;
        static CodeInfo()
        {
            var root = ConfigUtils.GetConfigurationRoot().GetSection("ChangeCode");
            list = ConfigUtils.BuildClass<List<CodeInfo>>(root);
            for (var i = 0; i < list.Count; i++)
            {
                var sourceRoot = root.GetSection(i.ToString()).GetSection("SourceCode");
                list[i].SourceCode = ConfigUtils.BuildClass<List<SourceCode>>(sourceRoot);
            }
        }

        public static void Save()
        {
            var root = ConfigUtils.GetConfigurationRoot();
            var codeRoot = root.GetSection("ChangeCode");
            var allList = codeRoot.GetAllConfigDic();
            //移除原有数据
            allList.ForEach(item =>
            {
                root.SetValue(item.Key, null);
            });
            //添加当前数据
            for (var i = 0; i < list.Count; i++)
            {
                var code = codeRoot.GetSection(i.ToString());

                code.SetValue($"DeviceType", list[i].DeviceType);
                code.SetValue($"Code", list[i].Code);
                code.SetValue($"Name", list[i].Name);
                code.SetValue($"Desc", list[i].Desc);
                code.SetValue($"High", list[i].High);
                code.SetValue($"Low", list[i].Low);
                code.SetValue($"IsUse", list[i].IsUse);
                code.SetValue($"IsRandom", list[i].IsRandom);
                code.SetValue($"DefaultValue", list[i].DefaultValue);
                for (var j = 0; j < list[i].SourceCode.Count; j++)
                {
                    if (list[i].SourceCode[j].CollectionName.IsNullOrEmpty()) continue;
                    code.SetValue($"SourceCode:{j}:PlugName", list[i].SourceCode[j].PlugName);
                    code.SetValue($"SourceCode:{j}:CollectionName", list[i].SourceCode[j].CollectionName);
                }
            }
            ConfigUtils.SaveToFile();
        }
    }
}
