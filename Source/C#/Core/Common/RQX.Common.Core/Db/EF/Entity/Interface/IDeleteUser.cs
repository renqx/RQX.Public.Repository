﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity.Interface
{
    /// <summary>
    /// 包含删除的信息
    /// </summary>
    public interface IDeleteUser : IUpdateUser, IDelete
    {
        /// <summary>
        /// 删除者ID
        /// </summary>
        int? DeleteUserId { get; set; }
        /// <summary>
        /// 删除者名字
        /// </summary>
        string DeleteUserName { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        DateTime? DeleteTime { get; set; }
    }
}
