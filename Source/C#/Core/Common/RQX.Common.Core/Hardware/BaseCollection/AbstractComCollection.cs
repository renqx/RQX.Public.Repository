﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    /// <summary>
    /// COM采集抽象类
    /// </summary>
    public abstract class AbstractComCollection : AbstractBaseCollection<MyCom>
    {
        #region 协议默认参数
        public override ProType ProType => ProType.COM;
        public abstract int BaudRate { get; }
        public abstract int DataBits { get; }
        public abstract StopBits StopBits { get; }
        public abstract Parity Parity { get; }
        public abstract bool DTR { get; }
        public abstract bool RTS { get; }
        #endregion
        #region Init
        protected AbstractComCollection() { }
        protected AbstractComCollection(StructureModel<MyCom> structureModel) : base(structureModel)
        {
            _conn.BindRecvedHandler(RecvData);
        }
        #endregion
        #region 接口
        public override void Start(bool isUseReStart = true)
        {
            _conn.Open();
            Thread.Sleep(1000);
            base.Start(isUseReStart);
        }

        public override void Dispose()
        {
            _conn.Close();
            base.Dispose();
        }
        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn.Send(buffer);
        }
        #endregion
    }
}
