﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQX.DataCollection.Main.Simulate
{
    public partial class SimulateMain : Form
    {
        #region Init
        public SimulateMain()
        {
            InitializeComponent();
        }
        private void SimulateMain_Load(object sender, EventArgs e)
        {
            rbtn_ginsert_single.Checked = true;
            rbtn_gtime_current.Checked = true;
            rbtn_gvalue_random.Checked = true;
        }
        #endregion

        #region MyRegion

        #endregion
        /// <summary>
        /// 打开数据库配置界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_dbconfig_Click(object sender, EventArgs e)
        {
            DbConfig form = new DbConfig();
            form.ShowDialog();
        }


        #region radioButton change
        /// <summary>
        /// 起始时间设置的联动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbtn_gtime_point_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_gtime_point.Checked)
            {
                dtp_point_time.Enabled = true;
            }
            else
            {
                dtp_point_time.Enabled = false;
            }
        }
        /// <summary>
        /// 插入方式的联动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbtn_ginsert_circle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_ginsert_circle.Checked)
            {
                txt_circle_time.Enabled = true;
            }
            else
            {
                txt_circle_time.Enabled = false;
            }
        }
        #endregion

       
    }
}
