﻿using System.Windows.Forms;

namespace DataCollection
{
    public static class Version
    {
        public static string CurrentVersion = "V3.0";
        public static void Show()
        {
            string msg =
@"V3.0  2020-07-16   组件版本：2.3.2
    增加模拟采集工具
V2.3  2020-07-08   组件版本：2.3.0
    增加数据库配置界面
V2.2.1  2020-07-02   组件版本：2.3.0
    优化界面
V2.2  2020-07-01   组件版本：2.3.0
    增加托盘启动、启动自动运行采集插件功能
V2.1  2020-06-30   组件版本：2.2.0
    稳定框架，增加对码配置功能，废弃原有对码等xml文件
V2.0  2020-06-29   组件版本：2.2.0
    框架从Framework4.6迁移到Core5.0，对应插件也需要更新
V1.1  2020-06-28   组件版本：2.4.5
    程序需要管理员权限运行，设置自启动时去掉UAC提示
    去掉MetroFramework组件，为转Core5.0做准备
V1.0  2020-06-24   组件版本：2.4.5
    原有版本下，增加配置开机自启动功能，在托盘图标右键设置中";
            MessageBox.Show(msg);
        }
    }
}
