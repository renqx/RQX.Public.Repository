﻿using RQX.Common.Core.Hardware.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace RQX.DataCollection.Test.Net
{
    public partial class UdpForm : Form
    {
        public UdpForm()
        {
            InitializeComponent();
        }

        private void UdpForm_Load(object sender, EventArgs e)
        {
            txt_set_ip.Text = "127.0.0.1";
            ckb_recv_autosave.Enabled = false;
            ckb_send_file.Enabled = false;
            ckb_send_circle.Enabled = false;
            txt_send_circle_time.Enabled = false;
            btn_save.Enabled = false;

            txt_send_circle_time.Text = "1000";
            ckb_recv_hex.Checked = true;
            ckb_recv_newline.Checked = true;
            ckb_send_hex.Checked = true;
        }
    }
}
