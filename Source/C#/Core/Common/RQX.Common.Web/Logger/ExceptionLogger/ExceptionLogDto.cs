﻿using RQX.Common.Web.Core.AutoMapper;
using RQX.Common.Web.Core.Dto;
using System;

namespace RQX.Common.Web.Logger.ExceptionLogger
{
    /// <summary>
    /// 异常日志DTO
    /// </summary>
    [AutoMapper(typeof(ExceptionLog))]
    public class ExceptionLogDto : BaseDto
    {
        /// <summary>
        /// 异常编号GUID
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string type_name { get; set; }
        /// <summary>
        /// 异常发生时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 异常简要信息
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// 异常堆栈信息
        /// </summary>
        public string exception { get; set; }





    }
}
