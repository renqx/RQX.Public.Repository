﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RQX.Common.Web.Authority;
using System;
using System.Collections.Generic;
using System.Data;

namespace RQX.Common.Web.Tools
{
    public class Utils
    {
        /// <summary>
        /// 获取当前时间
        /// </summary>
        /// <returns></returns>
        public static DateTime GetCurrentDateTime()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// 获取当前登录人
        /// </summary>
        /// <returns></returns>
        public static LoginUser GetCurrentUser()
        {
            return LoginUser.GetCurrentUser();
        }

        /// <summary>
        /// JSON数据转DataTable
        /// </summary>
        /// <param name="json"></param>
        /// <param name="names">字典类型，字段名-显示名</param>
        /// <returns></returns>
        public static DataTable JsonToTable(string json, Dictionary<string, string> names)
        {
            DataTable table = new DataTable();
            foreach (var name in names)
            {
                table.Columns.Add(name.Value);
            }
            var obj = JsonConvert.DeserializeObject<JArray>(json);
            foreach (var item in obj)
            {
                var row = table.NewRow();
                foreach (var name in names)
                {
                    row[name.Value] = item[name.Key];
                }
                table.Rows.Add(row);
            }
            return table;
        }

    }
}
