﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Logger;

namespace GE_B40
{
    public class B40 : AbstractComCollection
    {
        private const string connStr = "7e 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 0a 00 0e 00 00 00 00 00 49 7e";
        #region 内部属性

        //private byte[] data;
        //private int dataBeginIndex = 0;
        //private ushort[] beginList = new ushort[8];
        //private int beginListLength = 0;
        //private const int recordLength = 278;//生理数据记录长度
        //private Dictionary<string, short> dic = new Dictionary<string, short>();

        //private short _hr;//心率
        //private short _nibp_sys;//无创收缩
        //private short _nibp_dia;//无创舒张
        //private short _Spo2;//血氧饱和度
        //private short _pr;//脉搏
        //private short _rr;//呼吸率
        //private short _temp;//体温
        //private short _ibp1_sys;//有创收缩
        //private short _ibp1_dia;//有创舒张
        //private short _bis;//脑电双频指数
        //private short _art_sys;//动脉收缩
        //private short _art_dia;//动脉舒张
        //private short _cvp_sys;//中心静脉收缩
        //private short _cvp_dia;//中心静脉舒张
        //private short _etco2;//呼末co2
        //private short _fio2;//吸入氧气
        #endregion
        #region 默认参数
        public override int BaudRate => 19200;
        public override int DataBits => 8;
        public override StopBits StopBits => StopBits.One;
        public override Parity Parity => Parity.Even;
        public override bool DTR => true;
        public override bool RTS => true;
        public override DeviceType DeviceType => DeviceType.监护仪;
        #endregion
        #region 初始化
        //public B40(string data)
        //{
        //    //data = data.Replace(" ", "");
        //    //WriteLog($"数据长度：{data.Length},{data.Length / 2},{data.Length / 2 - 2}");
        //    //var list = StringToByte(data);
        //    //this.data = new byte[list.Length - 2];
        //    //for (var i = 1; i < list.Length - 1; i++)
        //    //{
        //    //    this.data[i - 1] = list[i];
        //    //}
        //}

        public B40()
        {
        }

        public B40(StructureModel<MyCom> structureModel) : base(structureModel)
        {
        }
        #endregion
        //public Dictionary<string, short> Start()
        //{
        //    //ShowHeader();
        //    //ShowPhysiologicalData();
        //    TryStruct();
        //    //Console.WriteLine($"心率：{_hr}\t脉搏：{_pr}\t呼吸率:{_rr}");
        //    //Console.WriteLine($"无创收缩：{_nibp_sys}\t无创舒张：{_nibp_dia}\t血氧饱和:{_Spo2}");
        //    //Console.WriteLine($"有创收缩：{_ibp1_sys}\t有创舒张：{_ibp1_dia}\t体温:{_temp}");
        //    //Console.WriteLine($"动脉收缩：{_art_sys}\t动脉舒张：{_art_dia}\t脑电双频指数:{_bis}");
        //    //Console.WriteLine($"中心静脉收缩：{_cvp_sys}\t中心静脉舒张：{_cvp_dia}");
        //    //Console.WriteLine($"呼末co2：{_etco2}\t吸入氧气：{_fio2}");
        //    //Console.WriteLine("===============================================================");
        //    return dic;
        //}
        #region 实现
        protected override void Connect(StepType stepNum, string msg)
        {
            var buffer = connStr.HexStringToByte();
            Send(buffer);
        }

        protected override void CircleSend()
        {
           
        }

        protected override string CutToOneMsg(StringBuilder sb)
        {
            var msg = sb.ToString();
            var index = msg.IndexOf("7e", 2);
            if (index > -1)
            {
                try
                {
                    return msg.Substring(0, index + 2);
                }catch(Exception ex)
                {
                    LogUtils.WriteErrorLog($"CutToOneMsg异常:{sb}\n{ex}");
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        protected override StepType CheckRecvMsgType(string lineStr)
        {
            if (lineStr.IsNotNullOrEmpty() && lineStr.StartsWith("7e") && lineStr.EndsWith("7e"))
            {
                return StepType.DataSource;
            }
            else
            {
                return StepType.None;
            }
        }

        protected override Dictionary<string, string> AnalysisData(string msg)
        {
            var buffer = msg[2..^2].HexStringToByte();
            var record = buffer.ToStruct<datex_record>();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (var item in record.ph_rcrd.ph_subrec)
            {
                if (item.cl_drilvl_subt != 0 && (item.cl_drilvl_subt & 0x0F00) >> 8 == 0)
                {
                    var basic = item.basic;
                    dic.Add("hr", basic.ecg.hr.ToString());
                    dic.Add("nibp_sys", basic.nibp.sys.ToString());
                    dic.Add("nibp_dia", basic.nibp.dia.ToString());
                    //dic.Add("ibp_sys", basic.p1234?[0].sys.ToString());
                    //dic.Add("ibp_dia", basic.p1234?[0].dia.ToString());
                    //dic.Add("art_sys", basic.p1234?[1].sys.ToString());
                    //dic.Add("art_dia", basic.p1234?[1].dia.ToString());
                    //dic.Add("cvp_sys", basic.p1234?[2].sys.ToString());
                    //dic.Add("cvp_dia", basic.p1234?[2].dia.ToString());
                    var spo2 = basic.SpO2.SpO2;
                    dic.Add("SpO2", spo2.ToString().Equals("-32767") ? "-32767" : (spo2 / 100.0).ToString());
                    dic.Add("pr", basic.SpO2.pr.ToString());
                    dic.Add("rr", basic.co2.rr.ToString());
                    dic.Add("etco2", basic.co2.et.ToString());
                    dic.Add("fio2", basic.o2.fi.ToString());
                    dic.Add("t1", basic.t[0].temp.ToString());
                    break;
                }
            }
            var items = dic.Where(k => k.Value.Equals("-32767"));
            if (items.Count() > 0)
            {
                var deleteList = items.Select(k => new KeyValuePair<string, string>(k.Key, k.Value));
                WriteAnalysisLogMethod(items);
                deleteList.ToList().ForEach(item => dic.Remove(item.Key));
            }
            return dic;
        }
        #endregion
        #region 头部信息  不再使用

        //public void ShowHeader()
        //{
        //    WriteLog("=========================开始解析头部信息==================================");
        //    var len = GetShortOrWord(0);
        //    WriteLog($"r_len,数据长度：{len}");
        //    var nbr = GetByte(2);
        //    WriteLog($"r_nbr,记录的编号：{nbr}");
        //    var level = GetByte(3);
        //    WriteLog($"dri_level,支持的级别:{level}");
        //    var plugid = GetShortOrWord(4);
        //    WriteLog($"plug_id,监视器插件编号:{plugid}");
        //    var time = GetLongOrDword(6);
        //    WriteLog($"r_time,传输时间：{time},格式化：{ ShowTimeStr(time)}");
        //    var r1 = GetByte(10);
        //    WriteLog($"reserved1:保留字段1,{r1}");
        //    var r2 = GetByte(11);
        //    WriteLog($"reserved2:保留字段2,{r2}");
        //    var r3 = GetShortOrWord(12);
        //    WriteLog($"reserved3:保留字段3,{r3}");
        //    var maintype = GetShortOrWord(14);
        //    WriteLog($"r_maintype:记录的类型,{maintype},{ShowMainType(maintype)}");
        //    for (var i = 0; i < 8; i++)
        //    {
        //        int beginNum = 16 + i * 3;
        //        var type = Show_sr_desc(beginNum, i);
        //        if (type == 0xFF) break;
        //        beginListLength++;
        //    }
        //    dataBeginIndex = 40;//全头部长度
        //    WriteLog($"数据区域起始位置：{dataBeginIndex}");
        //    WriteLog("=========================结束解析头部信息==================================");
        //}

        //private string ShowMainType(int maintype)
        //{
        //    switch (maintype)
        //    {
        //        case 0: return "生理数据";
        //        case 1: return "波形数据";
        //        case 4: return "报警数据,网口有效";
        //        case 5: return "患者信息,网口有效";
        //        case 8: return "麻醉事件,网口有效";
        //        default:
        //            return "数据解析异常";
        //    }
        //}

        //private byte Show_sr_desc(int beginNum, int index)
        //{
        //    var offset = GetShortOrWord(beginNum);
        //    beginList[index] = offset;
        //    WriteLog($"sr_desc[{index}].sr_offset,后续记录偏移量：{offset}");
        //    var type = GetByte(beginNum + 2);
        //    WriteLog($"sr_desc[{index}].sr_type,子记录的类型，FF标识没有更多记录：{type}");
        //    return type;
        //}
        #endregion
        #region 生理数据解析 不再使用

        //public void ShowPhysiologicalData()
        //{
        //    WriteLog("=========================开始解析生理数据信息================================");
        //    for (var i = 0; i < beginListLength; i++)
        //    {
        //        WriteLog("***************单次生理数据开始*******************");
        //        int beginNum = dataBeginIndex + beginList[i];
        //        var time = GetLongOrDword(beginNum);
        //        WriteLog($"数据时间：{ShowTimeStr(time)}");
        //        var marker = GetByte(beginNum + recordLength - 4);
        //        WriteLog($"最近输入的数据标记：{marker}");
        //        var type = GetShortOrWord(beginNum + recordLength - 2);
        //        int phdb_class = (type & 0x0F00) >> 8;
        //        var className = ShowPHDB_Class(phdb_class);
        //        WriteLog($"该数据类型为：{className}");
        //        if (phdb_class != 0)
        //        {
        //            WriteLog("非Base数据,略过");
        //            WriteLog("***************单次生理数据结束*******************");
        //            continue;
        //        }
        //        //ecg
        //        int begin_ecg = beginNum + 4;
        //        ShowGroup_hdr("ecg", begin_ecg);
        //        _hr = (short)GetShortOrWord(begin_ecg + 6);

        //        //private short _art_sys;//动脉收缩
        //        //private short _art_dia;//动脉舒张
        //        //private short _cvp_sys;//中心静脉收缩
        //        //private short _cvp_dia;//中心静脉舒张
        //        //private short _nibp_sys;//无创收缩
        //        //private short _nibp_dia;//无创舒张
        //        //private short _ibp1_sys;//有创收缩
        //        //private short _ibp1_dia;//有创舒张

        //        //private short _bis;//脑电双频指数
        //        WriteLog($"ecg-hr", (short)GetShortOrWord(begin_ecg + 6));
        //        WriteLog($"ecg-st1", (short)GetShortOrWord(begin_ecg + 8));
        //        WriteLog($"ecg-st2", (short)GetShortOrWord(begin_ecg + 10));
        //        WriteLog($"ecg-st3", (short)GetShortOrWord(begin_ecg + 12));
        //        WriteLog($"ecg-imp_rr", (short)GetShortOrWord(begin_ecg + 14));
        //        //p_group
        //        int begin_p1 = begin_ecg + 16;
        //        ShowGroup_hdr("p_1", begin_p1);
        //        WriteLog($"p_1-sys", (short)GetShortOrWord(begin_p1 + 6));
        //        WriteLog($"p_1-dia", (short)GetShortOrWord(begin_p1 + 8));
        //        WriteLog($"p_1-mean", (short)GetShortOrWord(begin_p1 + 10));
        //        WriteLog($"p_1-hr", (short)GetShortOrWord(begin_p1 + 12));
        //        int begin_p2 = begin_p1 + 14;
        //        ShowGroup_hdr("p_2", begin_p2);
        //        WriteLog($"p_2-sys", (short)GetShortOrWord(begin_p2 + 6));
        //        WriteLog($"p_2-dia", (short)GetShortOrWord(begin_p2 + 8));
        //        WriteLog($"p_2-mean", (short)GetShortOrWord(begin_p2 + 10));
        //        WriteLog($"p_2-hr", (short)GetShortOrWord(begin_p2 + 12));
        //        int begin_p3 = begin_p2 + 14;
        //        ShowGroup_hdr("p_3", begin_p3);
        //        WriteLog($"p_3-sys", (short)GetShortOrWord(begin_p3 + 6));
        //        WriteLog($"p_3-dia", (short)GetShortOrWord(begin_p3 + 8));
        //        WriteLog($"p_3-mean", (short)GetShortOrWord(begin_p3 + 10));
        //        WriteLog($"p_3-hr", (short)GetShortOrWord(begin_p3 + 12));
        //        int begin_p4 = begin_p3 + 14;
        //        ShowGroup_hdr("p_4", begin_p4);
        //        WriteLog($"p_4-sys", (short)GetShortOrWord(begin_p4 + 6));
        //        WriteLog($"p_4-dia", (short)GetShortOrWord(begin_p4 + 8));
        //        WriteLog($"p_4-mean", (short)GetShortOrWord(begin_p4 + 10));
        //        WriteLog($"p_4-hr", (short)GetShortOrWord(begin_p4 + 12));
        //        //nibp_group
        //        int begin_nibp = begin_p4 + 14;
        //        ShowGroup_hdr("nibp", begin_nibp);
        //        WriteLog($"nibp-sys", (short)GetShortOrWord(beginNum + 6));
        //        WriteLog($"nibp-dia", (short)GetShortOrWord(beginNum + 8));
        //        WriteLog($"nibp-mean", (short)GetShortOrWord(beginNum + 10));
        //        WriteLog($"nibp-hr", (short)GetShortOrWord(beginNum + 12));
        //        //t_group
        //        int begin_t1 = begin_nibp + 14;
        //        ShowGroup_hdr("t_1", begin_t1);
        //        _temp = (short)GetShortOrWord(begin_t1 + 6);
        //        WriteLog($"t_1-temp", _temp);
        //        int begin_t2 = begin_t1 + 8;
        //        ShowGroup_hdr("t_2", begin_t2);
        //        WriteLog($"t_2-temp", (short)GetShortOrWord(begin_t2 + 6));
        //        int begin_t3 = begin_t2 + 8;
        //        ShowGroup_hdr("t_3", begin_t3);
        //        WriteLog($"t_3-temp", (short)GetShortOrWord(begin_t3 + 6));
        //        int begin_t4 = begin_t3 + 8;
        //        ShowGroup_hdr("t_4", begin_t4);
        //        WriteLog($"t_4-temp", (short)GetShortOrWord(begin_t4 + 6));
        //        //SpO2
        //        int begin_SpO2 = begin_t4 + 8;
        //        ShowGroup_hdr("SpO2", begin_SpO2);
        //        _Spo2 = (short)GetShortOrWord(begin_SpO2 + 6);
        //        WriteLog($"SpO2-SpO2", _Spo2);
        //        _pr = (short)GetShortOrWord(begin_SpO2 + 8);
        //        WriteLog($"SpO2-pr", _pr);
        //        WriteLog($"SpO2-ir_amp", (short)GetShortOrWord(begin_SpO2 + 10));
        //        WriteLog($"SpO2-SvO2", (short)GetShortOrWord(begin_SpO2 + 12));
        //        //CO2
        //        int begin_CO2 = begin_SpO2 + 14;
        //        ShowGroup_hdr("CO2", begin_CO2);
        //        _etco2 = (short)GetShortOrWord(begin_CO2 + 6);
        //        WriteLog($"CO2-et", _etco2);
        //        WriteLog($"CO2-fi", (short)GetShortOrWord(begin_CO2 + 8));
        //        _rr = (short)GetShortOrWord(begin_CO2 + 10);
        //        WriteLog($"CO2-rr", _rr);
        //        WriteLog($"CO2-amb_press", (short)GetShortOrWord(begin_CO2 + 12));
        //        //O2
        //        int begin_O2 = begin_CO2 + 14;
        //        ShowGroup_hdr("O2", begin_O2);
        //        WriteLog($"O2-et", (short)GetShortOrWord(begin_O2 + 6));
        //        _fio2 = (short)GetShortOrWord(begin_O2 + 8);
        //        WriteLog($"O2-fi", _fio2);
        //        //n2o
        //        int begin_n2o = begin_O2 + 10;
        //        ShowGroup_hdr("n2o", begin_n2o);
        //        WriteLog($"n2o-et", (short)GetShortOrWord(begin_n2o + 6));
        //        WriteLog($"n2o-fi", (short)GetShortOrWord(begin_n2o + 8));
        //        //aa
        //        int begin_aa = begin_O2 + 10;
        //        ShowGroup_hdr("aa", begin_aa);
        //        WriteLog($"aa-et", (short)GetShortOrWord(begin_aa + 6));
        //        WriteLog($"aa-fi", (short)GetShortOrWord(begin_aa + 8));
        //        WriteLog($"aa-mac_sum", (short)GetShortOrWord(begin_aa + 10));
        //        //flow_vol
        //        int begin_flow_vol = begin_aa + 12;
        //        ShowGroup_hdr("flow_vol", begin_flow_vol);
        //        WriteLog($"flow_vol-rr", (short)GetShortOrWord(begin_flow_vol + 6));
        //        WriteLog($"flow_vol-ppeak", (short)GetShortOrWord(begin_flow_vol + 8));
        //        WriteLog($"flow_vol-peep", (short)GetShortOrWord(begin_flow_vol + 10));
        //        WriteLog($"flow_vol-pplat", (short)GetShortOrWord(begin_flow_vol + 12));
        //        WriteLog($"flow_vol-tv_insp", (short)GetShortOrWord(begin_flow_vol + 14));
        //        WriteLog($"flow_vol-tv_exp", (short)GetShortOrWord(begin_flow_vol + 16));
        //        WriteLog($"flow_vol-compliance", (short)GetShortOrWord(begin_flow_vol + 18));
        //        WriteLog($"flow_vol-mv_exp", (short)GetShortOrWord(begin_flow_vol + 20));
        //        //co_wedge
        //        int begin_co_wedge = begin_flow_vol + 22;
        //        ShowGroup_hdr("co_wedge", begin_co_wedge);
        //        WriteLog($"co_wedge-co", (short)GetShortOrWord(begin_co_wedge + 6));
        //        WriteLog($"co_wedge-blood_temp", (short)GetShortOrWord(begin_co_wedge + 8));
        //        WriteLog($"co_wedge-ref", (short)GetShortOrWord(begin_co_wedge + 10));
        //        WriteLog($"co_wedge-pcwp", (short)GetShortOrWord(begin_co_wedge + 12));
        //        //nmt
        //        int begin_nmt = begin_co_wedge + 14;
        //        ShowGroup_hdr("co_wedge", begin_nmt);
        //        WriteLog($"co_wedge-t1", (short)GetShortOrWord(begin_nmt + 6));
        //        WriteLog($"co_wedge-tratio", (short)GetShortOrWord(begin_nmt + 8));
        //        WriteLog($"co_wedge-ptc", (short)GetShortOrWord(begin_nmt + 10));
        //        //ecg_extra
        //        int begin_ecg_extra = begin_nmt + 12;
        //        WriteLog($"ecg_extra-hr_ecg", (short)GetShortOrWord(begin_ecg_extra));
        //        WriteLog($"ecg_extra-hr_max", (short)GetShortOrWord(begin_ecg_extra + 2));
        //        WriteLog($"ecg_extra-hr_min", (short)GetShortOrWord(begin_ecg_extra + 4));
        //        //svo2
        //        int begin_svo2 = begin_ecg_extra + 6;
        //        ShowGroup_hdr("svo2", begin_svo2);
        //        WriteLog($"svo2-vo2", (short)GetShortOrWord(begin_svo2 + 6));



        //        WriteLog("***************单次生理数据结束*******************");
        //        //break;
        //    }
        //    WriteLog("=========================结束解析生理数据信息================================");
        //}

        //private ushort ShowGroup_hdr(string hdrName, int beginNum)
        //{
        //    var value = GetInt(beginNum);
        //    WriteLog($"\t\t{hdrName}头部,exists:{value & 0x1}");
        //    WriteLog($"\t\t{hdrName}头部,active:{(value & 0x2) >> 1}");
        //    var lable = GetShortOrWord(beginNum + 8);
        //    WriteLog($"\t\t{hdrName}头部,lable:{lable}");
        //    return lable;
        //}

        //private string ShowPHDB_Class(int num)
        //{
        //    switch (num)
        //    {
        //        case 0: return "DRI_PHDBCL_BASIC";
        //        case 1: return "DRI_PHDBCL_EXT1";
        //        case 2: return "DRI_PHDBCL_EXT2";
        //        case 3: return "DRI_PHDBCL_EXT3";
        //        default:
        //            return "解析异常";
        //    }
        //}

        #endregion

        #region 结构体解析
        //private void TryStruct()
        //{
        //    var record = (datex_record)ByteToStruct(data, typeof(datex_record));
        //    foreach (var item in record.ph_rcrd.ph_subrec)
        //    {
        //        if ((item.cl_drilvl_subt & 0x0F00) >> 8 == 0)
        //        {
        //            var basic = item.basic;
        //            WriteLog("*********************************");
        //            //WriteLog(ShowTimeStr(item.time));
        //            //WriteLog("ecg-hr", basic.ecg.hr);
        //            //WriteLog("SpO2-SpO2", basic.SpO2.SpO2);
        //            //WriteLog("SpO2-pr", basic.SpO2.pr);
        //            //WriteLog("co2-rr", basic.co2.rr);
        //            //WriteLog("co2-et", basic.co2.et);
        //            //WriteLog("o2-fi", basic.o2.fi);
        //            //WriteLog("t-1", basic.t[0].temp);

        //            WriteExtraDic(basic);
        //            WriteLog("*********************************");
        //        }
        //    }
        //}

        //private void WriteExtraDic(basic_phdb basic)
        //{
        //    //ECG
        //    //if (basic.ecg.hr >= 50 && basic.ecg.hr <= 120)
        //    WriteLog("ecg-hr", basic.ecg.hr);
        //    WriteLog("ecg-st1", basic.ecg.st1);
        //    WriteLog("ecg-st2", basic.ecg.st1);
        //    WriteLog("ecg-st3", basic.ecg.st3);
        //    WriteLog("ecg-tmp_rr", basic.ecg.tmp_rr);
        //    //p1~4
        //    for (var i = 0; i < 4; i++)
        //    {
        //        WriteLog($"p1234_{i + 1}-hr", basic.p1234[i].hr);
        //        WriteLog($"p1234_{i + 1}-sys", basic.p1234[i].sys);
        //        WriteLog($"p1234_{i + 1}-dia", basic.p1234[i].dia);
        //        WriteLog($"p1234_{i + 1}-mean", basic.p1234[i].mean);
        //    }
        //    //nibp
        //    WriteLog("nibp-hr", basic.nibp.hr);
        //    // if (basic.nibp.sys >= 80 && basic.nibp.sys <= 160) 
        //    WriteLog("nibp-sys", basic.nibp.sys);
        //    // if (basic.nibp.dia >= 50 && basic.nibp.dia <= 100) 
        //    WriteLog("nibp-dia", basic.nibp.dia);
        //    WriteLog("nibp-mean", basic.nibp.mean);
        //    //t1~4
        //    for (var i = 0; i < 4; i++)
        //    {
        //        WriteLog($"t{i + 1}-temp", basic.t[i].temp);
        //    }
        //    //SpO2
        //    //if (basic.SpO2.SpO2 >= 50 && basic.SpO2.SpO2 <= 100) 
        //    WriteLog("SpO2-SpO2", basic.SpO2.SpO2);
        //    // if (basic.SpO2.pr >= 50 && basic.SpO2.pr <= 120) 
        //    WriteLog("SpO2-pr", basic.SpO2.pr);
        //    WriteLog("SpO2-ir_amp", basic.SpO2.ir_amp);
        //    WriteLog("SpO2-SvO2", basic.SpO2.SvO2);
        //    //co2
        //    WriteLog("co2-et", basic.co2.et);
        //    WriteLog("co2-fi", basic.co2.fi);
        //    WriteLog("co2-rr", basic.co2.rr);
        //    WriteLog("co2-amb_press", basic.co2.amb_press);
        //    //o2
        //    WriteLog("o2-et", basic.o2.et);
        //    WriteLog("o2-fi", basic.o2.fi);
        //    //n2o
        //    WriteLog("n2o-et", basic.n2o.et);
        //    WriteLog("n2o-fi", basic.n2o.fi);
        //    //aa
        //    WriteLog("aa-et", basic.aa.et);
        //    WriteLog("aa-fi", basic.aa.fi);
        //    WriteLog("aa-mac_sum", basic.aa.mac_sum);
        //    //flow_vol
        //    //  if (basic.flow_vol.rr >= 8 && basic.flow_vol.rr <= 30) 
        //    WriteLog("flow_vol-rr", basic.flow_vol.rr);
        //    WriteLog("flow_vol-ppeak", basic.flow_vol.ppeak);
        //    WriteLog("flow_vol-peep", basic.flow_vol.peep);
        //    WriteLog("flow_vol-pplat", basic.flow_vol.pplat);
        //    WriteLog("flow_vol-tv_insp", basic.flow_vol.tv_insp);
        //    WriteLog("flow_vol-tv_exp", basic.flow_vol.tv_exp);
        //    WriteLog("flow_vol-compliance", basic.flow_vol.compliance);
        //    WriteLog("flow_vol-mv_exp", basic.flow_vol.mv_exp);
        //    //co_wedge
        //    WriteLog("co_wedge-co", basic.co_wedge.co);
        //    WriteLog("co_wedge-blood_temp", basic.co_wedge.blood_temp);
        //    WriteLog("co_wedge-ref", basic.co_wedge._ref);
        //    WriteLog("co_wedge-pcwp", basic.co_wedge.pcwp);
        //    //nmt
        //    WriteLog("nmt-t1", basic.nmt.t1);
        //    WriteLog("nmt-tratio", basic.nmt.tratio);
        //    WriteLog("nmt-ptc", basic.nmt.ptc);
        //    //ecg_extra
        //    WriteLog("ecg_extra-hr_ecg", basic.ecg_extra.hr_ecg);
        //    WriteLog("ecg_extra-hr_max", basic.ecg_extra.hr_max);
        //    WriteLog("ecg_extra-hr_min", basic.ecg_extra.hr_min);
        //    //svo2
        //    WriteLog("svo2-svo2", basic.svo2.svo2);
        //    for (var i = 0; i < 2; i++)
        //    {
        //        WriteLog($"p56_{i + 1}-sys", basic.p56[i].sys);
        //        WriteLog($"p56_{i + 1}-dia", basic.p56[i].dia);
        //        WriteLog($"p56_{i + 1}-hr", basic.p56[i].hr);
        //        WriteLog($"p56_{i + 1}-mean", basic.p56[i].mean);
        //    }
        //}



        //将Byte转换为结构体类型
        //public static object ByteToStruct(byte[] bytes, Type type)
        //{
        //    int size = Marshal.SizeOf(type);
        //    //if (size > bytes.Length)
        //    //{
        //    //    return null;
        //    //}
        //    //分配结构体内存空间
        //    IntPtr structPtr = Marshal.AllocHGlobal(size);
        //    object obj;
        //    try
        //    {
        //        //将byte数组拷贝到分配好的内存空间
        //        Marshal.Copy(bytes, 0, structPtr, Math.Min(size, bytes.Length));
        //        //将内存空间转换为目标结构体
        //        obj = Marshal.PtrToStructure(structPtr, type);
        //    }
        //    finally
        //    {
        //        Marshal.FreeHGlobal(structPtr);
        //    }
        //    return obj;
        //}

        #endregion


        #region 公共方法
        //public static uint ChangeBigSmall(uint value) => (uint)ChangeBigSmall(value, 4);
        //public static ushort ChangeBigSmall(ushort value) => (ushort)ChangeBigSmall(value, 2);
        //public static short ChangeBigSmall(short value) => (short)ChangeBigSmall((ulong)value, 2);
        //public static byte ChangeBigSmall(byte value) => (byte)ChangeBigSmall(value, 1);

        //public static ulong ChangeBigSmall(ulong value, int bytes)
        //{
        //    ulong result = 0;
        //    switch (bytes)
        //    {
        //        case 4: result = (value & 0xFF) << 24 | (value & 0xFF00) << 8 | (value & 0xFF0000) >> 8 | (value & 0xFF000000) >> 24; break;
        //        case 3: result = (value & 0xFF) << 16 | (value & 0xFF00) | (value & 0xFF0000) >> 16; break;
        //        case 2: result = (value & 0xFF) << 8 | (value & 0xFF00) >> 8; break;
        //        default:
        //            result = value;
        //            break;
        //    }
        //    return result;
        //}

        //private void WriteLog(string key, short? value = null)
        //{
        //    if (value.HasValue)
        //    {
        //        if (dic.ContainsKey(key))
        //        {
        //            if (dic[key] == -32767)
        //            {
        //                dic[key] = value.Value;
        //            }
        //        }
        //        else
        //        {
        //            dic.Add(key, value.Value);
        //        }
        //    }
        //    //Console.WriteLine(key);
        //}

        //private string ShowTimeStr(ulong ticks)
        //{
        //    return new DateTime(1970, 1, 1).AddSeconds(ticks).ToString("yyyy-MM-dd HH:mm:ss");
        //}

        //private byte[] StringToByte(string str)
        //{
        //    byte[] buffer = new byte[str.Length / 2];
        //    for (var i = 0; i < buffer.Length; i++)
        //    {
        //        var tmpStr = str.Substring(i * 2, 2);
        //        var num = Convert.ToInt32(tmpStr, 16);
        //        buffer[i] = (byte)num;
        //    }
        //    return buffer;
        //}


        //private byte GetByte(int beginNum)
        //{
        //    try
        //    {
        //        return data[beginNum];
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        return 0;
        //    }
        //}
        //private ushort GetShortOrWord(int beginNum)
        //{
        //    return (ushort)GetValue(2, beginNum);
        //}
        //private uint GetInt(int beginNum)
        //{
        //    return (uint)GetValue(4, beginNum);
        //}

        //private ulong GetLongOrDword(int beginNum)
        //{
        //    return GetValue(4, beginNum);
        //}

        //private ulong GetValue(int byteLength, int beginNum)
        //{
        //    ulong value = 0;
        //    try
        //    {
        //        for (var i = 0; i < byteLength; i++)
        //        {
        //            value |= (ulong)data[beginNum + i] << (i * 8);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }

        //    return value;
        //}

        

        #endregion

        #region 结构体


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct datex_record
        {
            /// <summary>
            /// 头部
            /// </summary>
            public datex_hdr hdr;
            /// <summary>
            /// 数据区
            /// </summary>
            public ph_srcrds ph_rcrd;
        };
        /// <summary>
        /// 数据区
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ph_srcrds
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5, ArraySubType = UnmanagedType.Struct)]
            public dri_phdb[] ph_subrec;
        }
        /// <summary>
        /// 具体数据db
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct dri_phdb
        {
            /// <summary>
            /// 数据的时间
            /// </summary>
            public uint time;// { get { return ChangeBigSmall(time); } set { time = value; } }
            /// <summary>
            /// basic 的数据集
            /// </summary>
            public basic_phdb basic;
            /// <summary>
            /// 最新的输入标记
            /// </summary>
            public byte marker;// { get { return ChangeBigSmall(marker); } set { marker = value; } }
            /// <summary>
            /// 保留字段
            /// </summary>
            public byte reserved;// { get { return ChangeBigSmall(reserved); } set { reserved = value; } }
            /// <summary>
            /// 8~11位，为0则是basic
            /// </summary>
            public ushort cl_drilvl_subt;// { get { return ChangeBigSmall(cl_drilvl_subt); } set { cl_drilvl_subt = value; } }
        }
        /// <summary>
        /// basic数据集
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct basic_phdb
        {
            public ecg_group ecg;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.Struct)]
            public p_group[] p1234;
            public nibp_group nibp;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.Struct)]
            public t_group[] t;
            public SpO2_pl_group SpO2;
            public co2_group co2;
            public o2_group o2;
            public n2o_group n2o;
            public aa_group aa;
            public flow_vol_group flow_vol;
            public co_wedge_group co_wedge;
            public nmt_group nmt;
            public ecg_extra_group ecg_extra;
            public svo2_group svo2;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2, ArraySubType = UnmanagedType.Struct)]
            public p_group[] p56;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public byte[] reserved;
        }
        /// <summary>
        /// ECG
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ecg_group
        {
            public group_hdr hdr;
            public short hr;// { get { return ChangeBigSmall(hr); } set { hr = value; } }
            public short st1;//{ get { return ChangeBigSmall(st1); } set { st1 = value; } }
            public short st2;//{ get { return ChangeBigSmall(st2); } set { st2 = value; } }
            public short st3;//{ get { return ChangeBigSmall(st3); } set { st3 = value; } }
            public short tmp_rr;// { get { return ChangeBigSmall(tmp_rr); } set { tmp_rr = value; } }
        }
        /// <summary>
        /// P
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct p_group
        {
            public group_hdr hdr;
            public short sys;// { get { return ChangeBigSmall(sys ); } set { sys  = value; } }
            public short dia;// { get { return ChangeBigSmall(dia ); } set { dia  = value; } }
            public short mean;// { get { return ChangeBigSmall(mean); } set { mean = value; } }
            public short hr;//{ get { return ChangeBigSmall(hr); } set { hr = value; } }
        }
        /// <summary>
        /// nibp
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct nibp_group
        {
            public group_hdr hdr;
            public short sys;//{ get { return ChangeBigSmall(sys); } set { sys = value; } }
            public short dia;//{ get { return ChangeBigSmall(dia); } set { dia = value; } }
            public short mean;// { get { return ChangeBigSmall(mean); } set { mean = value; } }
            public short hr;// { get { return ChangeBigSmall(hr); } set { hr = value; } }
        }
        /// <summary>
        /// t
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct t_group
        {
            public group_hdr hdr;
            public short temp;// { get { return ChangeBigSmall(temp); } set { temp = value; } }
        }
        /// <summary>
        /// SpO2
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SpO2_pl_group
        {
            public group_hdr hdr;
            public short SpO2;// { get { return ChangeBigSmall(SpO2); } set { SpO2 = value; } }
            public short pr;// { get { return ChangeBigSmall(pr); } set { pr = value; } }
            public short ir_amp;// { get { return ChangeBigSmall(ir_amp); } set { ir_amp = value; } }
            public short SvO2;// { get { return ChangeBigSmall(SvO2); } set { SvO2 = value; } }
        }
        /// <summary>
        /// co2
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct co2_group
        {
            public group_hdr hdr;
            public short et;// { get { return ChangeBigSmall(et); } set { et = value; } }
            public short fi;// { get { return ChangeBigSmall(fi); } set { fi = value; } }
            public short rr;// { get { return ChangeBigSmall(rr); } set { rr = value; } }
            public short amb_press;// { get { return ChangeBigSmall(amb_press); } set { amb_press = value; } }
        }
        /// <summary>
        /// o2
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct o2_group
        {
            public group_hdr hdr;
            public short et;// { get { return ChangeBigSmall(et); } set { et = value; } }
            public short fi;// { get { return ChangeBigSmall(fi); } set { fi = value; } }
        }
        /// <summary>
        /// n2o
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct n2o_group
        {
            public group_hdr hdr;
            public short et;// { get { return ChangeBigSmall(et); } set { et = value; } }
            public short fi;// { get { return ChangeBigSmall(fi); } set { fi = value; } }
        }
        /// <summary>
        /// aa
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct aa_group
        {
            public group_hdr hdr;
            public short et;//{ get { return ChangeBigSmall(et); } set { et = value; } }
            public short fi;//{ get { return ChangeBigSmall(fi); } set { fi = value; } }
            public short mac_sum;// { get { return ChangeBigSmall(mac_sum); } set { mac_sum = value; } }
        }
        /// <summary>
        /// flow_vol
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct flow_vol_group
        {
            public group_hdr hdr;
            public short rr;// { get { return ChangeBigSmall(rr); } set { rr = value; } }
            public short ppeak;// { get { return ChangeBigSmall(ppeak); } set { ppeak = value; } }
            public short peep;//{ get { return ChangeBigSmall(peep); } set { peep = value; } }
            public short pplat;// { get { return ChangeBigSmall(pplat); } set { pplat = value; } }
            public short tv_insp;// { get { return ChangeBigSmall(tv_insp); } set { tv_insp = value; } }
            public short tv_exp;//{ get { return ChangeBigSmall(tv_exp); } set { tv_exp = value; } }
            public short compliance;// { get { return ChangeBigSmall(compliance); } set { compliance = value; } }
            public short mv_exp;//{ get { return ChangeBigSmall(mv_exp); } set { mv_exp = value; } }
        }
        /// <summary>
        /// co_wedge
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct co_wedge_group
        {
            public group_hdr hdr;
            public short co;// { get { return ChangeBigSmall(co); } set { co = value; } }
            public short blood_temp;// { get { return ChangeBigSmall(blood_temp); } set { blood_temp = value; } }
            public short _ref;// { get { return ChangeBigSmall(_ref); } set { _ref = value; } }
            public short pcwp;// { get { return ChangeBigSmall(pcwp); } set { pcwp = value; } }
        }
        /// <summary>
        /// nmt
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct nmt_group
        {
            public group_hdr hdr;
            public short t1;// { get { return ChangeBigSmall(t1); } set { t1 = value; } }
            public short tratio;// { get { return ChangeBigSmall(tratio); } set { tratio = value; } }
            public short ptc;// { get { return ChangeBigSmall(ptc); } set { ptc = value; } }
        }
        /// <summary>
        /// ecg_extra
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ecg_extra_group
        {
            public short hr_ecg;// { get { return ChangeBigSmall(hr_ecg); } set { hr_ecg = value; } }
            public short hr_max;// { get { return ChangeBigSmall(hr_max); } set { hr_max = value; } }
            public short hr_min;// { get { return ChangeBigSmall(hr_min); } set { hr_min = value; } }
        }
        /// <summary>
        /// svo2
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct svo2_group
        {
            public group_hdr hdr;
            public short svo2;// { get { return ChangeBigSmall(svo2); } set { svo2 = value; } }
        }
        /// <summary>
        /// 分组头部
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct group_hdr
        {
            /// <summary>
            /// 状态，0位标识是否存在模块，1位标识是否启用模块
            /// </summary>
            public uint status;// { get { return ChangeBigSmall(status); } set { status = value; } }
            /// <summary>
            /// 描述信息
            /// </summary>
            public ushort label;// { get { return ChangeBigSmall(label); } set { label = value; } }
        }
        /// <summary>
        /// 头部
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct datex_hdr
        {
            /// <summary>
            /// 记录的总长度，包括header
            /// </summary>
            public short r_len;
            /// <summary>
            /// 记录的编号
            /// </summary>
            public byte r_nbr;// { get { return ChangeBigSmall(r_nbr); } set { r_nbr = value; } }
            /// <summary>
            /// 支持的级别
            /// </summary>
            public byte dri_level;// { get { return ChangeBigSmall(dri_level); } set { dri_level = value; } }
            /// <summary>
            /// 监视器的插件标识编号
            /// </summary>
            public ushort plug_id;//{ get { return ChangeBigSmall(plug_id); } set { plug_id = value; } }
            /// <summary>
            /// 传输时间，大部分从1970.1.1开始，部分从1900.1.1开始
            /// </summary>
            public uint r_time;// { get { return ChangeBigSmall(r_time); } set { r_time = value; } }
            /// <summary>
            /// 保留字段，传输必须为0
            /// </summary>
            public byte reserved1;// { get { return ChangeBigSmall(reserved1); } set { reserved1 = value; } }
            /// <summary>
            /// 保留字段，传输必须为0
            /// </summary>
            public byte reserved2;// { get { return ChangeBigSmall(reserved2); } set { reserved2 = value; } }
            /// <summary>
            /// 保留字段，传输必须为0
            /// </summary>
            public ushort reserved3;// { get { return ChangeBigSmall(reserved3); } set { reserved3 = value; } }
            /// <summary>
            /// 记录的类型,0
            /// </summary>
            public short r_maintype;// { get { return ChangeBigSmall(r_maintype); } set { r_maintype = value; } }
            /// <summary>
            /// 描述后续记录的数组
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8, ArraySubType = UnmanagedType.Struct)]
            public sr_desc[] desc;
        };
        /// <summary>
        /// 描述后续记录
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sr_desc
        {
            /// <summary>
            /// 相对偏移量
            /// </summary>
            public short sr_offset;// { get { return ChangeBigSmall(sr_offset); } set { sr_offset = value; } }
            /// <summary>
            /// 子记录的类型，依赖于maintype，FF表示没有更多的子记录
            /// </summary>
            public byte sr_type;// { get { return ChangeBigSmall(sr_type); } set { sr_type = value; } }
        };
        #endregion
    }
}
