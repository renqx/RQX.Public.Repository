﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    /// <summary>
    /// 连接步骤
    /// </summary>
    public enum StepType
    {
        /// <summary>
        /// 无需操作
        /// </summary>
        None = -999,
        /// <summary>
        /// 初始化进入
        /// </summary>
        Init = -1,
        /// <summary>
        /// 当前为采集数据
        /// </summary>
        DataSource = 0,
        /// <summary>
        /// 连接建立完毕，可以开始循环采集
        /// </summary>
        FinishConnect = 1,
        /// <summary>
        /// 各步骤
        /// </summary>
        Setp1 = 2,
        Setp2 = 3,
        Setp3 = 4,
        Setp4 = 5,
        Setp5 = 6,
    }
}
