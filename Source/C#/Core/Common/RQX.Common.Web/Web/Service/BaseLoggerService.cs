﻿using RQX.Common.Web.Core.Dto;
using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.DynamicApi;

namespace RQX.Common.Web.Web.Service
{
    /// <summary>
    /// 日志基础接口
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    /// <typeparam name="TPagger"></typeparam>
    [IgnoreApiService]
    public class BaseLoggerService<TEntity, TDto, TPagger> : ApiService
        where TEntity : BaseEntity, new()
        where TDto : BaseDto, new()
        where TPagger : BasePagger, new()
    {
        private readonly IRepository<TEntity> _Repository;
        public BaseLoggerService(BaseDbContext dbContext)
        {
            _Repository = new BaseRepository<TEntity>(dbContext);
        }

        public virtual TEntity Add(TEntity entity)
        {
            return _Repository.Add(entity);
        }
    }
}
