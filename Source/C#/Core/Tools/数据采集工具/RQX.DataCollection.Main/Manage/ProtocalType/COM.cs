﻿using Microsoft.Extensions.Configuration;
using RQX.Common.Core.Config;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Manage.ProtocalType
{
    public class COM
    {
        /// <summary>
        /// 串口号
        /// </summary>
        public string PortName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRate { get; set; }
        /// <summary>
        /// 数据位
        /// </summary>
        public int DataBits { get; set; }
        /// <summary>
        /// 停止位
        /// </summary>
        public StopBits StopBits { get; set; }
        /// <summary>
        /// 校验位
        /// </summary>
        public Parity Parity { get; set; }
        /// <summary>
        /// 通讯握手协议DTR
        /// </summary>
        public bool DTR { get; set; }
        /// <summary>
        /// 通讯握手协议RTS
        /// </summary>
        public bool RTS { get; set; }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="path"></param>
        public void Save(IConfigurationSection section)
        {
            section.SetValue($"PortName", PortName);
            section.SetValue($"BaudRate", BaudRate);
            section.SetValue($"DataBits", DataBits);
            section.SetValue($"StopBits", StopBits);
            section.SetValue($"Parity", Parity);
            section.SetValue($"DTR", DTR);
            section.SetValue($"RTS", RTS);
        }
        /// <summary>
        /// 获取参数字符串
        /// </summary>
        /// <returns></returns>
        public string GetParamStr() => $"{PortName};{BaudRate};{DataBits};Stop:{StopBits};Parity:{Parity};DTR:{DTR};RTS:{RTS}";
    }
}
