﻿using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RQX.Common.Web.Web.Service.Interface
{
    public interface IEntityService<TEntity, TPagger>
        where TEntity : BaseEntity
        where TPagger : IPagger
    {
        #region add
        TEntity AddEntity(TEntity entity);
        IEnumerable<TEntity> AddEntity(IEnumerable<TEntity> entities);
        #endregion

        #region update
        TEntity UpdateEntity(TEntity entity);
        IEnumerable<TEntity> UpdateEntity(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> UpdateEntity(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, IEnumerable<TEntity> entities);
        #endregion

        #region remove
        TEntity RemoveEntity(TEntity entity);
        IEnumerable<TEntity> RemoveEntity(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> RemoveEntity(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, IEnumerable<TEntity> entities);
        #endregion

        #region get
        TEntity GetEntityInfo(int primaryKey);
        IEnumerable<TEntity> GetEntityInfo(params int[] primaryKeys);
        IQueryable<TEntity> GetAllEntities(bool isTracking = true);
        IQueryable<TEntity> GetAllEntities(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, bool isTracking = true);
        IEnumerable<TEntity> GetEntityList(TPagger pagger, out int rowCount);
        IEnumerable<TEntity> GetEntityList(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, TPagger pagger, out int rowCount);
        #endregion
    }
}
