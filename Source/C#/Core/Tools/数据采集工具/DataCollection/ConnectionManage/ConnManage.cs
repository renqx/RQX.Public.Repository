﻿using DataCollection.Manage;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DataCollection.ConnectionManage
{

    public class ConnManage
    {
        public Dictionary<string, ConnInfo> dicConnInfo = new Dictionary<string, ConnInfo>();
        public ConnManage()
        {
        }
        #region 客户端模式
        public bool Connect(Action<string> showMsg, string localIP, DeviceInfo info)
        {
            MYSQLDB.ShowMsgHandler = showMsg;
            ConnInfo conn = new ConnInfo()
            {
                GUID = info.GUID,
                ProType = info.ProType,
            };
            object structureModel = null;
            switch (info.ProType)
            {
                case ProType.TCPClient:
                    var tcpClient = new TcpSocketClient(info.TCPClient.TargetIP, info.TCPClient.TargetPort);
                    structureModel = new StructureModel<TcpSocketClient>
                    {
                        Conn = tcpClient,
                        LocalIP = localIP,
                        MaxRecvTime = 30 * 1000,
                        Ext1 = info.TCPClient.TargetIP,
                        Ext2 = info.TCPClient.TargetPort,
                    };
                    conn.tcpClient = tcpClient;
                    break;
                case ProType.UDP:
                    var udpSocket = new UdpSocket(info.UDP.TargetIP, info.UDP.TargetPort, info.UDP.LocalPort);
                    structureModel = new StructureModel<UdpSocket>
                    {
                        Conn = udpSocket,
                        LocalIP = localIP,
                        MaxRecvTime = 30 * 1000,
                        Ext1 = info.UDP.TargetIP,
                        Ext2 = info.UDP.TargetPort,
                        Ext3 = info.UDP.LocalPort,
                    };
                    conn.udpClient = udpSocket;
                    break;
                case ProType.COM:
                default:
                    break;
            }
            if (dicConnInfo.ContainsKey(info.GUID))
            {
                dicConnInfo.Remove(info.GUID);
            }
            dicConnInfo.Add(info.GUID, conn);
            var instance = (IIocCollection)Activator.CreateInstance(info.GetDeviceClassType(), structureModel);
            instance.DealDataHandler = (datas) =>
            {
                if (datas.Count > 0)
                {
                    MYSQLDB.BuildDataFormart(datas, info.GUID);
                }
            };
            instance.ShowMsgHandler = showMsg;
            try
            {
                instance.Start();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        #endregion

        #region 服务器模式
        public bool StartService(Action<string> showMsg, string localIP, DeviceInfo info)
        {
            //ConnInfo conn = null;
            //if (!dicConnInfo.ContainsKey(vsGuid))
            //{
            //    conn = new ConnInfo();
            //    conn.sGuid = vsGuid;
            //    conn.TerminalIp = vsIP;
            //    conn.ServerPort = viPort;
            //    conn.ConnType = vType;
            //    conn.OldConnType = vType;
            //    conn.Protocol = vType.ToString();
            //    conn.ConnSocket = new Socket(AddressFamily.InterNetwork, vType == ProType.TCP ? SocketType.Stream : SocketType.Dgram, vType == ProType.TCP ? ProtocolType.Tcp : ProtocolType.Udp);
            //    //发送和接收超时  Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //    //conn.ConnSocket.ReceiveTimeout = 5000;
            //    // conn.ConnSocket.SendTimeout = 5000;
            //    if (conn.ServerPort.ToString().Trim().Length > 2 && conn.Protocol == "TCP")
            //    {
            //        conn.iServiceMode = 1;
            //    }
            //    else
            //    {
            //        conn.iServiceMode = 0;
            //    }
            //    dicConnInfo.Add(vsGuid, conn);
            //}
            //else
            //{
            //    conn = dicConnInfo[vsGuid];
            //    conn.TerminalIp = vsIP;
            //    conn.ServerPort = viPort;
            //    conn.ConnType = vType;
            //    conn.OldConnType = vType;
            //    conn.Protocol = vType.ToString();

            //    conn.mutex.WaitOne();
            //    conn.ConnSocket = new Socket(AddressFamily.InterNetwork, vType == ProType.TCP ? SocketType.Stream : SocketType.Dgram, vType == ProType.TCP ? ProtocolType.Tcp : ProtocolType.Udp);
            //    //发送和接收超时
            //    conn.ConnSocket.ReceiveTimeout = 5000;
            //    conn.ConnSocket.SendTimeout = 5000;
            //    conn.mutex.ReleaseMutex();

            //    conn.OldConnType = conn.ConnType;
            //    conn.ConnType = vType;
            //}

            //if (conn.mThread != null)
            //{
            //    conn.mThread.Abort();
            //    conn.mThread = null;
            //}
            //conn.mThread = new Thread(_Connect);
            //conn.mThread.IsBackground = true;
            //conn.mThread.Start(conn);
            return true;
        }
        #endregion

        #region 串口模式
        public bool ConnectCom(Action<string> showMsg, string ip, DeviceInfo info)
        {
            var com = info.COM;
            MyCom myCom = new MyCom(com.BaudRate, com.PortName, com.DataBits, com.StopBits, com.Parity);
            ConnInfo conn = new ConnInfo
            {
                GUID = info.GUID,
                ProType = ProType.COM,
                myCom = myCom
            };
            try
            {
                myCom.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            dicConnInfo.Add(info.GUID, conn);
            StructureModel<MyCom> structureModel = new StructureModel<MyCom>()
            {
                Conn = myCom,
                LocalIP = ip,
                MaxRecvTime = 30 * 1000,
            };
            var instance = (IIocCollection)Activator.CreateInstance(info.GetDeviceClassType(), structureModel);
            instance.DealDataHandler = (datas) =>
            {
                MYSQLDB.BuildDataFormart(datas, info.GUID);
            };
            instance.ShowMsgHandler = showMsg;
            instance.Start();
            return true;
        }
        #endregion

    }
}
