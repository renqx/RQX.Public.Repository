﻿using RQX.Common.Web.Core.Enum;

namespace RQX.Common.Web.Logger
{
    public enum LoggerUpdateType
    {
        /// <summary>
        /// 未捕获异常记录
        /// </summary>
        [EnumDescription("未捕获异常记录")]
        UnCatchException,
        /// <summary>
        /// 异常接口简要信息
        /// </summary>
        [EnumDescription("异常接口简要信息")]
        ExceptionInterfaceBrief,
        /// <summary>
        /// 异常接口详细信息
        /// </summary>
        [EnumDescription("异常接口详细信息")]
        ExceptionInterfaceDetail,
        /// <summary>
        /// 所有异常记录
        /// </summary>
        [EnumDescription("所有异常记录")]
        AllException,
        /// <summary>
        /// 所有接口简要信息
        /// </summary>
        [EnumDescription("所有接口简要信息")]
        AllInterfaceBrief,
        /// <summary>
        /// 所有接口详细信息
        /// </summary>
        [EnumDescription("所有接口详细信息")]
        AllInterfaceDetail,
    }
}
