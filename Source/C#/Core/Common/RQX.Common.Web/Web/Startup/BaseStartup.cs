﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RQX.Common.Web.Core.AutoMapper;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Swagger;

namespace RQX.Common.Web.Web.Startup
{
    public class BaseStartup
    {
        public BaseStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AllowSyncIO();
            services.AddContainers();
            services.AddDbContext();
            services.AddDynamicApiService();
            services.AddAutoMapper();
            services.AddSwaggerService();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwaggerConfigure();
            app.UseStateAutoMapper();
            app.UseCors();
        }
    }
}
