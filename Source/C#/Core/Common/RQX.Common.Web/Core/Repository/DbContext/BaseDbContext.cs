﻿using Microsoft.EntityFrameworkCore;
using RQX.Common.Web.Config;
using RQX.Common.Web.Web.Container;
using System.Linq;


namespace RQX.Common.Web.Core.Repository.DbContext
{
    public class BaseDbContext : Microsoft.EntityFrameworkCore.DbContext, IIoCManager
    {
        public BaseDbContext() { }

        public BaseDbContext(DbContextOptions<BaseDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var sqlConnectionStr = AppConfigurtaion.GetConfigValue($"{GlobalConfig.DB_SectionName}:{GlobalConfig.DB_ApolloConnectionName}");
                optionsBuilder.UseSqlServer(sqlConnectionStr);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.NoAction;
            }
            base.OnModelCreating(modelBuilder);
            //modelBuilder.AddEntityConfigurationsFromAssembly(Assembly.GetEntryAssembly());
        }
    }
}
