﻿using System.Drawing;
using System.Windows.Forms.VisualStyles;

namespace DataCollection.SimulateTool
{
    partial class Simulate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Simulate));
            this.cklist_type = new System.Windows.Forms.CheckedListBox();
            this.txt_log = new System.Windows.Forms.RichTextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.dgv_use = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgv_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_random = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgv_default = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // cklist_type
            // 
            this.cklist_type.CheckOnClick = true;
            this.cklist_type.FormattingEnabled = true;
            this.cklist_type.Location = new System.Drawing.Point(846, 12);
            this.cklist_type.Name = "cklist_type";
            this.cklist_type.Size = new System.Drawing.Size(262, 130);
            this.cklist_type.TabIndex = 1;
            this.cklist_type.SelectedIndexChanged += new System.EventHandler(this.cklist_type_SelectedIndexChanged);
            // 
            // txt_log
            // 
            this.txt_log.Location = new System.Drawing.Point(846, 147);
            this.txt_log.Name = "txt_log";
            this.txt_log.Size = new System.Drawing.Size(367, 529);
            this.txt_log.TabIndex = 2;
            this.txt_log.Text = "";
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(1114, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(99, 129);
            this.btn_start.TabIndex = 3;
            this.btn_start.Text = "开始生成";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // dgv_use
            // 
            this.dgv_use.HeaderText = "启用";
            this.dgv_use.Name = "dgv_use";
            this.dgv_use.Width = 60;
            // 
            // dgv_type
            // 
            this.dgv_type.HeaderText = "类型";
            this.dgv_type.Name = "dgv_type";
            this.dgv_type.ReadOnly = true;
            this.dgv_type.Width = 80;
            // 
            // dgv_code
            // 
            this.dgv_code.HeaderText = "编号";
            this.dgv_code.Name = "dgv_code";
            this.dgv_code.ReadOnly = true;
            this.dgv_code.Width = 80;
            // 
            // dgv_name
            // 
            this.dgv_name.HeaderText = "名称";
            this.dgv_name.Name = "dgv_name";
            this.dgv_name.ReadOnly = true;
            // 
            // dgv_desc
            // 
            this.dgv_desc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_desc.HeaderText = "描述";
            this.dgv_desc.Name = "dgv_desc";
            this.dgv_desc.ReadOnly = true;
            // 
            // dgv_low
            // 
            this.dgv_low.HeaderText = "下限";
            this.dgv_low.Name = "dgv_low";
            this.dgv_low.ReadOnly = true;
            this.dgv_low.Width = 80;
            // 
            // dgv_high
            // 
            this.dgv_high.HeaderText = "上限";
            this.dgv_high.Name = "dgv_high";
            this.dgv_high.ReadOnly = true;
            this.dgv_high.Width = 80;
            // 
            // dgv_random
            // 
            this.dgv_random.HeaderText = "随机生成";
            this.dgv_random.Name = "dgv_random";
            this.dgv_random.Width = 60;
            // 
            // dgv_default
            // 
            this.dgv_default.HeaderText = "默认值";
            this.dgv_default.Name = "dgv_default";
            // 
            // Simulate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 689);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_use,
            this.dgv_type,
            this.dgv_code,
            this.dgv_name,
            this.dgv_desc,
            this.dgv_low,
            this.dgv_high,
            this.dgv_random,
            this.dgv_default});
            this.grid.Location = new System.Drawing.Point(12, 12);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(828, 665);
            this.grid.TabIndex = 0;
            this.grid.Text = "dataGridView1";
            this.grid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellEndEdit);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.txt_log);
            this.Controls.Add(this.cklist_type);
            this.Controls.Add(this.grid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Simulate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "模拟采集工具";
            this.Load += new System.EventHandler(this.Simulate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.CheckedListBox cklist_type;
        private System.Windows.Forms.RichTextBox txt_log;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgv_use;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_high;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgv_random;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_default;
    }
}