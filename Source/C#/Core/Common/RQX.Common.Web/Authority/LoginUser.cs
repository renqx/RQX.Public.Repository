﻿using Newtonsoft.Json;
using RQX.Common.Web.Tools;
using RQX.Common.Web.Web.Filter;
using System;

namespace RQX.Common.Web.Authority
{
    public class LoginUser
    {
        /// <summary>
        /// 机构ID
        /// </summary>
        public int OrgnizationId { get; set; }
        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrgnizationName { get; set; }
        /// <summary>
        /// 科室ID
        /// </summary>
        public int DepartId { get; set; }
        /// <summary>
        /// 科室名称
        /// </summary>
        public string DepartName { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 员工编码
        /// </summary>
        public string UserCode { get; set; }
        /// <summary>
        /// 角色 ID
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 是否为系统管理员
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 用户权限字符串
        /// </summary>
        /// <returns></returns>
        public string UserRight { get; set; }
        //public static string ClientId;      //客户端ID
        //public static string ClientName;    //客户端ID

        public DateTime LoginTime { get; set; }

        public LoginUser() { }

        public static bool CheckToken(string token)
        {
            try
            {
                var userStr = CypherHelper.DESDecrypt(token);
                var user = JsonConvert.DeserializeObject<LoginUser>(userStr);
                FakeSession.loginUserStr.Value = userStr;
                if (user.LoginTime.AddHours(15) < DateTime.Now)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
#warning 登录已过期测试
                throw new ApolloException(ApolloExceptionType.登录已过期, ex.ToString());
                //return false;
            }
        }

        public string CreateToken()
        {
            return CypherHelper.DESEncrypt(JsonConvert.SerializeObject(this));
        }

        #region 静态方法
        public static LoginUser GetCurrentUser()
        {
            var userStr = FakeSession.loginUserStr.Value;
            if (userStr.IsNullOrEmpty())
            {
                return new LoginUser() { UserName = "未登录用户", UserId = -1 };
            }
            else
            {
                return JsonConvert.DeserializeObject<LoginUser>(userStr);
            }
        }
        /// <summary>
        /// 获取密文密码
        /// </summary>
        /// <param name="originalPwd"></param>
        /// <returns></returns>
        public static string GetSafePassword(string originalPwd)
        {
            return originalPwd;
        }
        #endregion
    }
}
