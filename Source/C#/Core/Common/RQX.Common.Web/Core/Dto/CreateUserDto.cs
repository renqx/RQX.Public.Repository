﻿using RQX.Common.Web.Core.Interface;
using RQX.Common.Web.Core.Valid;
using System;

namespace RQX.Common.Web.Core.Dto
{
    /// <summary>
    /// 带创建者的传输实体
    /// </summary>
    public class CreateUserDto : BaseDto, ICreateUser
    {
        /// <summary>
        /// 创建人ID
        /// </summary>
        public int create_user_id { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        [ApolloMaxLength(50)]
        public string create_user_name { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; } = DateTime.Now;
    }
}
