﻿using RQX.Common.Web.Tools;
using System.ComponentModel.DataAnnotations;

namespace RQX.Common.Web.Core.Valid
{
    public class ApolloRequiredAttribute : RequiredAttribute
    {
        //The Code field is required."
        private const string DefaultErrorMessage = "参数{0}是必须的";
        public ApolloRequiredAttribute() : base() { }
        public ApolloRequiredAttribute(string errorMessage) : this()
        {
            ErrorMessage = errorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            if (ErrorMessage.IsNullOrEmpty())
            {
                return string.Format(DefaultErrorMessage, name);
            }
            else
            {
                return ErrorMessage;
            }
        }

    }
}
