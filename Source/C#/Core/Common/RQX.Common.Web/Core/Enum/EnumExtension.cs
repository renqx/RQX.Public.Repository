﻿using System.Linq;

namespace RQX.Common.Web.Core.Enum
{
    public static class EnumExtension
    {
        public static string ToDescription(this System.Enum enumType)
        {
            var type = enumType.GetType();
            var fieldInfo = type.GetField(enumType.ToString());
            if (fieldInfo == null) return null;
            var attributeType = typeof(EnumDescriptionAttribute);
            if (fieldInfo.IsDefined(attributeType, false))
            {
                return fieldInfo.GetCustomAttributes(attributeType, false).Cast<EnumDescriptionAttribute>().First().Name;
            }
            else
            {
                return enumType.ToString();
            }
        }
    }
}
