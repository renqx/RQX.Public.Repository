﻿using RQX.DataCollection.Main.Manage;
using RQX.DataCollection.Main.Simulate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQX.DataCollection.Main
{
    public partial class Main : Form
    {
        #region Init
        public Main()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 界面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, EventArgs e)
        {
            //DeviceInfo.list
        }
        #endregion
        #region Menu
        /// <summary>
        /// 进入模拟采集程序
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_simulate_Click(object sender, EventArgs e)
        {
            SimulateMain simulate = new SimulateMain();
            this.Hide();
            if (simulate.ShowDialog() == DialogResult.Cancel)
            {
                this.Show();
            }
        }
        /// <summary>
        /// 进入设备终端管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_collection_device_Click(object sender, EventArgs e)
        {
            DeviceManage deviceManage = new DeviceManage();
            this.Hide();
            if (deviceManage.ShowDialog() == DialogResult.Cancel)
            {
                this.Show();
            }
        }
        #endregion
    }
}
