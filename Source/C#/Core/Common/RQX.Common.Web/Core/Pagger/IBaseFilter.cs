﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 过滤条件接口
    /// </summary>
    public interface IBaseFilter
    {
        /// <summary>
        /// 过滤条件
        /// </summary>
        string Filter { get; set; }
    }
}
