﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.Net;
using RQX.Common.Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    /// <summary>
    /// TCP客户端采集抽象类
    /// </summary>
    public abstract class AbstractTcpClientCollection : AbstractBaseCollection<TcpSocketClient>
    {
        #region 协议默认参数
        public override ProType ProType => ProType.TCPClient;
        public abstract string IP { get; }
        public abstract int Port { get; }
        #endregion

        #region Init
        protected AbstractTcpClientCollection() { }
        protected AbstractTcpClientCollection(StructureModel<TcpSocketClient> structureModel) : base(structureModel)
        {
            if (_conn.IsNotNull())
            {
                _conn.HandleMsgReceived = (obj, buffer) =>
                {
                    RecvData(buffer);
                };
                //_conn.HandleException = (c, e) => LogUtils.WriteLogToConsole(e.ToString());
            }
        }
        #endregion

        #region 接口
        public override void Start(bool isUseReStart = true)
        {
            _conn?.Start();
            Thread.Sleep(1000);
            base.Start(isUseReStart);
        }

        public override void Dispose()
        {
            _conn?.Close();
            base.Dispose();
        }

        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn.Send(buffer);
        }
        #endregion
    }
}
