﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Db.EF.LocalModel
{
    public class GridViewTable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 最小值
        /// </summary>
        public double MinValue { get; set; }
        /// <summary>
        /// 最大值
        /// </summary>
        public double MaxValue { get; set; }
        /// <summary>
        /// 小数位数
        /// </summary>
        public int Digits { get; set; }
        /// <summary>
        /// 当前值
        /// </summary>
        public double CurrentValue { get; set; }
    }
}
