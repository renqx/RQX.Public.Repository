﻿using Microsoft.EntityFrameworkCore.Storage;
using RQX.Common.Core.Db.EF.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Repository
{
    public interface IRepository<TEntity>
        where TEntity : BaseEntity
    {
        void Include(Expression<Func<TEntity, object>> expression, params Expression<Func<object, object>>[] thenInclude);

        TEntity Find(int primaryKey);
        List<TEntity> Find(List<int> primaryKeys);
        IEnumerable<TEntity> Find(params int[] primarykeyList);

        IEnumerable<TEntity> Add(IEnumerable<TEntity> entities);
        TEntity Add(TEntity entity);

        TEntity Update(TEntity entity);
        IEnumerable<TEntity> Update(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> Update(IEnumerable<TEntity> entities, Func<IQueryable<TEntity>, IQueryable<TEntity>> func);

        TEntity Remove(TEntity entity);
        IEnumerable<TEntity> Remove(IEnumerable<TEntity> entities);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> WhereIf(bool isRun, Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> GetAll(bool isTracking = true);

        IDbContextTransaction GetBeginTransaction();
        int Commit();
    }
}
