﻿
using RQX.Common.Web.Core.Entity;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带创建者的树形实体
    /// </summary>
    public class CreateUserTreeEntity : CreateUserEntity
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
