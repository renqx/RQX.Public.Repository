﻿using RQX.Common.Web.Core.Interface;

namespace RQX.Common.Web.Core.Dto
{
    /// <summary>
    /// 支持软删除的传输实体
    /// </summary>
    public class DeleteDto : BaseDto, IDelete
    {
        /// <summary>
        /// 能否被删除
        /// </summary>
        public bool can_dele { get; set; } = true;
        /// <summary>
        /// 是否已删除
        /// </summary>
        public bool is_remove { get; set; } = false;
    }
}
