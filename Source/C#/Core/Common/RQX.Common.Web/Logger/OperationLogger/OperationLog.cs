﻿using RQX.Common.Web.Core.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace RQX.Common.Web.Logger.OperationLogger
{
    /// <summary>
    /// 操作日志记录
    /// </summary>
    [Table("BS_LOG_OPERATION")]
    public class OperationLog : CreateUserEntity
    {
        /// <summary>
        /// 详情
        /// </summary>
        public string message { get; set; }
        public OperationLog() { }
        public OperationLog(string msg)
        {
            message = msg;
        }
    }
}
