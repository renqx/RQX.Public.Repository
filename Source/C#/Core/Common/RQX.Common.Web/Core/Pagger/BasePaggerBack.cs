﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 分页返回类
    /// </summary>
    public class BasePaggerBack
    {
        /// <summary>
        /// 数据总数
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public object Data { get; set; }
        public BasePaggerBack(int count, object data)
        {
            Count = count;
            Data = data;
        }
    }
}
