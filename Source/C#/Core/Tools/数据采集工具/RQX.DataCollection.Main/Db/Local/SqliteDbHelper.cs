﻿using RQX.Common.Core.Db.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Db.Local
{
    public class SqliteDbHelper : BaseSqliteHelper
    {
        protected override string GetConnectionStr()
        {
            return $"Data Source=data.db";
        }

        //protected override string GetConnectionStr()
        //{
            
        //}
    }
}
