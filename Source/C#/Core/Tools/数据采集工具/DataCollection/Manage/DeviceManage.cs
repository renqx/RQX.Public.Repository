﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DataCollection.Manage
{
    public partial class DeviceManage : Form
    {
        private const string Column_GUID = "dgv_guid";
        private const string Column_DeviceName = "dgv_device_name";
        #region Init
        public DeviceManage()
        {
            InitializeComponent();
        }

        private void DeviceManage_Load(object sender, EventArgs e)
        {
            BindGrid();
        }
        /// <summary>
        /// 绑定Grid
        /// </summary>
        private void BindGrid()
        {
            grid.Rows.Clear();

            DeviceInfo.list.ForEach(info =>
            {
                var paramStr = "";
                switch (info.ProType)
                {
                    case RQX.Common.Core.Hardware.BaseCollection.ProType.UDP:
                        paramStr = info.UDP.GetParamStr();
                        break;
                    case RQX.Common.Core.Hardware.BaseCollection.ProType.TCPClient:
                        paramStr = info.TCPClient.GetParamStr();
                        break;
                    case RQX.Common.Core.Hardware.BaseCollection.ProType.TCPServer:
                        paramStr = info.TCPServer.GetParamStr();
                        break;
                    case RQX.Common.Core.Hardware.BaseCollection.ProType.COM:
                        paramStr = info.COM.GetParamStr();
                        break;
                }
                grid.Rows.Add(info.GUID, info.DeviceName, info.PlugName, info.ProType.ToString(), paramStr);
            });
        }
        #endregion

        #region 事件
        /// <summary>
        /// 添加设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_add_Click(object sender, EventArgs e)
        {
            DeviceEdit device = new DeviceEdit(Guid.NewGuid().ToString());
            this.Hide();
            if (device.ShowDialog() == DialogResult.Cancel)
            {
                BindGrid();
                this.Show();
            }
        }
        /// <summary>
        /// 编辑设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_edit_Click(object sender, EventArgs e)
        {
            if (grid.CurrentRow == null && grid.Rows.Count == 0)
            {
                FormUtils.ShowMsg("没有可编辑的终端设备！");
                return;
            }
            if (grid.SelectedRows.Count > 1)
            {
                FormUtils.ShowMsg("只能选择一行进行修改！");
                return;
            }

            var guid = grid.CurrentRow?.Cells[Column_GUID].Value ?? grid.Rows[0].Cells[Column_GUID].Value;
            DeviceEdit edit = new DeviceEdit(guid?.ToString());
            this.Hide();
            if (edit.ShowDialog() == DialogResult.Cancel)
            {
                BindGrid();
                this.Show();
            }
        }
        /// <summary>
        /// 删除终端设备配置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_del_Click(object sender, EventArgs e)
        {
            if (grid.CurrentRow == null && grid.Rows.Count == 0)
            {
                FormUtils.ShowMsg("没有可删除的终端设备！");
                return;
            }
            List<string> guidList = new List<string>();
            var names = "";
            for (var i = 0; i < grid.SelectedRows.Count; i++)
            {
                names += $"{grid.SelectedRows[i].Cells[Column_DeviceName].Value}\n";
                guidList.Add(grid.SelectedRows[i].Cells[Column_GUID].Value.ToString());
            }
            if (MessageBox.Show($"是否确定删除：\n{names}", "删除确认", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                guidList.ForEach(guid => DeviceInfo.list.Remove(DeviceInfo.GetDevice(guid?.ToString())));
                DeviceInfo.Save();
                BindGrid();
            }
        }
        #endregion


    }
}
