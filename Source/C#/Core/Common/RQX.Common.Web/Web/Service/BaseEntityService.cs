﻿using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RQX.Common.Web.Web.Service
{
    [IgnoreApiService]
    public class BaseEntityService<TEntity, TPagger> : CommonBaseService<TEntity>, IEntityService<TEntity, TPagger>
        where TEntity : BaseEntity
        where TPagger : IPagger
    {
        #region Init
        public BaseEntityService(BaseDbContext context) : base(context) { }
        #endregion

        #region Add
        /// <summary>
        /// 添加单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity AddEntity(TEntity entity)
        {
            return GetRepository().Add(entity);
        }
        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> AddEntity(IEnumerable<TEntity> entities)
        {
            return GetRepository().Add(entities);
        }
        #endregion

        #region Update
        /// <summary>
        /// 更新单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity UpdateEntity(TEntity entity)
        {
            return GetRepository().Update(entity);
        }
        /// <summary>
        /// 批量更新实体
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> UpdateEntity(IEnumerable<TEntity> entities)
        {
            return GetRepository().Update(entities);
        }
        /// <summary>
        /// 带条件的批量更新实体
        /// </summary>
        /// <param name="func"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> UpdateEntity(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, IEnumerable<TEntity> entities)
        {
            var updateEntities = DoFilter(entities, func);
            return UpdateEntity(updateEntities);
        }
        #endregion

        #region Remove
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity RemoveEntity(TEntity entity)
        {
            return GetRepository().Remove(entity);
        }
        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> RemoveEntity(IEnumerable<TEntity> entities)
        {
            return GetRepository().Remove(entities);
        }
        /// <summary>
        /// 带条件的批量删除实体
        /// </summary>
        /// <param name="func"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> RemoveEntity(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, IEnumerable<TEntity> entities)
        {
            var removeEntities = DoFilter(entities, func);
            return RemoveEntity(removeEntities);
        }
        #endregion

        #region Get
        /// <summary>
        /// 通过主键ID获取实体详情
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public TEntity GetEntityInfo(int primaryKey)
        {
            return Find(primaryKey).FirstOrDefault();
        }
        /// <summary>
        /// 通过主键ID列表，获取实体详情列表
        /// </summary>
        /// <param name="primaryKeys"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetEntityInfo(params int[] primaryKeys)
        {
            return Find(primaryKeys);
        }
        /// <summary>
        /// 获取所有实体
        /// </summary>
        /// <param name="isTracking"></param>
        /// <returns></returns>
        public IQueryable<TEntity> GetAllEntities(bool isTracking = true)
        {
            return GetRepository().GetAll(isTracking);
        }
        /// <summary>
        /// 带条件的获取所有实体
        /// </summary>
        /// <param name="func"></param>
        /// <param name="isTracking"></param>
        /// <returns></returns>
        public IQueryable<TEntity> GetAllEntities(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, bool isTracking = true)
        {
            var allEntities = GetAllEntities(isTracking);
            return func(allEntities);
        }
        /// <summary>
        /// 获取实体分页信息
        /// </summary>
        /// <param name="pagger"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetEntityList(TPagger pagger, out int rowCount)
        {
            var queryList = GetAllEntities();
            rowCount = queryList.Count();
            return GetEntityPage(queryList, pagger);
        }
        /// <summary>
        /// 带过滤条件的获取实体分页信息
        /// </summary>
        /// <param name="func"></param>
        /// <param name="pagger"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetEntityList(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, TPagger pagger, out int rowCount)
        {
            var queryList = GetAllEntities();
            queryList = func(queryList);
            rowCount = queryList.Count();
            return GetEntityPage(queryList, pagger);
        }
        /// <summary>
        /// 分页数据过滤与排序
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="pagger"></param>
        /// <returns></returns>
        private IEnumerable<TEntity> GetEntityPage(IQueryable<TEntity> entities, TPagger pagger)
        {
            var list = entities.OrderBy(pagger).Skip(pagger.GetSkipNum()).Take(pagger.PageSize);
            return list;
        }
        #endregion
    }
}
