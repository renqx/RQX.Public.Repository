﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Probe_Diy
{
    class Program
    {
        static void Main(string[] args)
        {
            StructureModel<MyCom> model = new StructureModel<MyCom>
            {
                Conn = new MyCom(115200, "COM3", 8, StopBits.One, Parity.None)
            };
            Probe probe = new Probe(model);
            probe.DealDataHandler = (dic) =>
            {
                dic.ForEach(item =>
                {
                    Console.WriteLine($"{item.Key}\t{item.Value}");
                });
            };
            probe.Start();
            Console.ReadKey();
        }
    }
}
