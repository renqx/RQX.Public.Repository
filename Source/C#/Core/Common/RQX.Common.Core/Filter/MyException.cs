﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Filter
{
    /// <summary>
    /// 系统异常
    /// </summary>
    public class MyException : Exception
    {
        public readonly MyExceptionType ExceptionType;
        public MyException(string message) : this(MyExceptionType.异常提示, message) { }
        public MyException(MyExceptionType type) : this(type, type.ToString()) { }
        public MyException(MyExceptionType type, string message = null) : this(type, message, null) { }
        public MyException(MyExceptionType type, string msg = null, Exception ex = null) : base(msg, ex)
        {
            ExceptionType = type;
        }
    }
}
