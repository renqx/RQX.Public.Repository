﻿namespace DataCollection.Config
{
    partial class ConfigSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigSetting));
            this.ddl_localIP = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ckb_run_poweron = new System.Windows.Forms.CheckBox();
            this.ckb_notifyicon_run = new System.Windows.Forms.CheckBox();
            this.ckb_autorun_plug = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // ddl_localIP
            // 
            this.ddl_localIP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddl_localIP.FormattingEnabled = true;
            this.ddl_localIP.ItemHeight = 17;
            this.ddl_localIP.Items.AddRange(new object[] {
            "MINDRAY_T5"});
            this.ddl_localIP.Location = new System.Drawing.Point(82, 6);
            this.ddl_localIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ddl_localIP.MaxLength = 17;
            this.ddl_localIP.Name = "ddl_localIP";
            this.ddl_localIP.Size = new System.Drawing.Size(171, 25);
            this.ddl_localIP.TabIndex = 4;
            this.ddl_localIP.SelectedIndexChanged += new System.EventHandler(this.ddl_localIP_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "本机IP地址";
            // 
            // ckb_run_poweron
            // 
            this.ckb_run_poweron.AutoSize = true;
            this.ckb_run_poweron.Location = new System.Drawing.Point(82, 38);
            this.ckb_run_poweron.Margin = new System.Windows.Forms.Padding(4);
            this.ckb_run_poweron.Name = "ckb_run_poweron";
            this.ckb_run_poweron.Size = new System.Drawing.Size(87, 21);
            this.ckb_run_poweron.TabIndex = 17;
            this.ckb_run_poweron.Text = "开机自启动";
            this.ckb_run_poweron.UseVisualStyleBackColor = true;
            this.ckb_run_poweron.CheckedChanged += new System.EventHandler(this.ckb_run_poweron_CheckedChanged);
            // 
            // ckb_notifyicon_run
            // 
            this.ckb_notifyicon_run.AutoSize = true;
            this.ckb_notifyicon_run.Location = new System.Drawing.Point(82, 67);
            this.ckb_notifyicon_run.Name = "ckb_notifyicon_run";
            this.ckb_notifyicon_run.Size = new System.Drawing.Size(75, 21);
            this.ckb_notifyicon_run.TabIndex = 18;
            this.ckb_notifyicon_run.Text = "托盘启动";
            this.ckb_notifyicon_run.UseVisualStyleBackColor = true;
            this.ckb_notifyicon_run.CheckedChanged += new System.EventHandler(this.ckb_notifyicon_run_CheckedChanged);
            // 
            // ckb_autorun_plug
            // 
            this.ckb_autorun_plug.AutoSize = true;
            this.ckb_autorun_plug.Location = new System.Drawing.Point(82, 95);
            this.ckb_autorun_plug.Name = "ckb_autorun_plug";
            this.ckb_autorun_plug.Size = new System.Drawing.Size(159, 21);
            this.ckb_autorun_plug.TabIndex = 19;
            this.ckb_autorun_plug.Text = "启动时自动运行采集插件";
            this.ckb_autorun_plug.UseVisualStyleBackColor = true;
            this.ckb_autorun_plug.CheckedChanged += new System.EventHandler(this.ckb_autorun_plug_CheckedChanged);
            // 
            // ConfigSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 132);
            this.Controls.Add(this.ckb_autorun_plug);
            this.Controls.Add(this.ckb_notifyicon_run);
            this.Controls.Add(this.ckb_run_poweron);
            this.Controls.Add(this.ddl_localIP);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置";
            this.Load += new System.EventHandler(this.ConfigSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddl_localIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckb_run_poweron;
        private System.Windows.Forms.CheckBox ckb_notifyicon_run;
        private System.Windows.Forms.CheckBox ckb_autorun_plug;
    }
}