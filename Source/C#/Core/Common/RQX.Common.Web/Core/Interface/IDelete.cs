﻿namespace RQX.Common.Web.Core.Interface
{
    /// <summary>
    /// 包含软删除信息
    /// </summary>
    public interface IDelete
    {
        /// <summary>
        /// 能否被删除
        /// </summary>
        public bool can_dele { get; set; }
        /// <summary>
        /// 是否已删除
        /// </summary>
        public bool is_remove { get; set; }
    }
}
