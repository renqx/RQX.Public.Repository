﻿using System.Windows.Forms;

namespace RQX.DataCollection.Main.Simulate
{
    partial class SimulateMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu_main = new System.Windows.Forms.MenuStrip();
            this.menu_dbconfig = new System.Windows.Forms.ToolStripMenuItem();
            this.gridView = new System.Windows.Forms.DataGridView();
            this.dtp_point_time = new System.Windows.Forms.DateTimePicker();
            this.rbtn_ginsert_single = new System.Windows.Forms.RadioButton();
            this.rbtn_ginsert_circle = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_circle_time = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtn_gtime_point = new System.Windows.Forms.RadioButton();
            this.rbtn_gtime_current = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_random_value = new System.Windows.Forms.Button();
            this.rbtn_gvalue_random = new System.Windows.Forms.RadioButton();
            this.rbtn_gvalue_fixed = new System.Windows.Forms.RadioButton();
            this.btn_start = new System.Windows.Forms.Button();
            this.txt_log = new System.Windows.Forms.RichTextBox();
            this.menu_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu_main
            // 
            this.menu_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_dbconfig});
            this.menu_main.Location = new System.Drawing.Point(0, 0);
            this.menu_main.Name = "menu_main";
            this.menu_main.Size = new System.Drawing.Size(1036, 25);
            this.menu_main.TabIndex = 0;
            this.menu_main.Text = "主菜单";
            // 
            // menu_dbconfig
            // 
            this.menu_dbconfig.Name = "menu_dbconfig";
            this.menu_dbconfig.Size = new System.Drawing.Size(80, 21);
            this.menu_dbconfig.Text = "数据库配置";
            this.menu_dbconfig.Click += new System.EventHandler(this.menu_dbconfig_Click);
            // 
            // gridView
            // 
            this.gridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridView.Location = new System.Drawing.Point(14, 37);
            this.gridView.Name = "gridView";
            this.gridView.Size = new System.Drawing.Size(676, 718);
            this.gridView.TabIndex = 0;
            // 
            // dtp_point_time
            // 
            this.dtp_point_time.Location = new System.Drawing.Point(125, 59);
            this.dtp_point_time.Name = "dtp_point_time";
            this.dtp_point_time.Size = new System.Drawing.Size(193, 23);
            this.dtp_point_time.TabIndex = 1;
            // 
            // rbtn_ginsert_single
            // 
            this.rbtn_ginsert_single.AutoSize = true;
            this.rbtn_ginsert_single.Location = new System.Drawing.Point(8, 28);
            this.rbtn_ginsert_single.Name = "rbtn_ginsert_single";
            this.rbtn_ginsert_single.Size = new System.Drawing.Size(74, 21);
            this.rbtn_ginsert_single.TabIndex = 2;
            this.rbtn_ginsert_single.TabStop = true;
            this.rbtn_ginsert_single.Text = "单次插入";
            this.rbtn_ginsert_single.UseVisualStyleBackColor = true;
            // 
            // rbtn_ginsert_circle
            // 
            this.rbtn_ginsert_circle.AutoSize = true;
            this.rbtn_ginsert_circle.Location = new System.Drawing.Point(8, 55);
            this.rbtn_ginsert_circle.Name = "rbtn_ginsert_circle";
            this.rbtn_ginsert_circle.Size = new System.Drawing.Size(98, 21);
            this.rbtn_ginsert_circle.TabIndex = 3;
            this.rbtn_ginsert_circle.TabStop = true;
            this.rbtn_ginsert_circle.Text = "间隔循环插入";
            this.rbtn_ginsert_circle.UseVisualStyleBackColor = true;
            this.rbtn_ginsert_circle.CheckedChanged += new System.EventHandler(this.rbtn_ginsert_circle_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.rbtn_ginsert_single);
            this.groupBox2.Controls.Add(this.rbtn_ginsert_circle);
            this.groupBox2.Controls.Add(this.txt_circle_time);
            this.groupBox2.Location = new System.Drawing.Point(699, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 93);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "插入方式";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "毫秒";
            // 
            // txt_circle_time
            // 
            this.txt_circle_time.Location = new System.Drawing.Point(122, 54);
            this.txt_circle_time.Name = "txt_circle_time";
            this.txt_circle_time.Size = new System.Drawing.Size(67, 23);
            this.txt_circle_time.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtn_gtime_point);
            this.groupBox1.Controls.Add(this.rbtn_gtime_current);
            this.groupBox1.Controls.Add(this.dtp_point_time);
            this.groupBox1.Location = new System.Drawing.Point(699, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 96);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "起始时间设置";
            // 
            // rbtn_gtime_point
            // 
            this.rbtn_gtime_point.AutoSize = true;
            this.rbtn_gtime_point.Location = new System.Drawing.Point(8, 61);
            this.rbtn_gtime_point.Name = "rbtn_gtime_point";
            this.rbtn_gtime_point.Size = new System.Drawing.Size(98, 21);
            this.rbtn_gtime_point.TabIndex = 1;
            this.rbtn_gtime_point.TabStop = true;
            this.rbtn_gtime_point.Text = "使用指定时间";
            this.rbtn_gtime_point.UseVisualStyleBackColor = true;
            this.rbtn_gtime_point.CheckedChanged += new System.EventHandler(this.rbtn_gtime_point_CheckedChanged);
            // 
            // rbtn_gtime_current
            // 
            this.rbtn_gtime_current.AutoSize = true;
            this.rbtn_gtime_current.Location = new System.Drawing.Point(8, 30);
            this.rbtn_gtime_current.Name = "rbtn_gtime_current";
            this.rbtn_gtime_current.Size = new System.Drawing.Size(98, 21);
            this.rbtn_gtime_current.TabIndex = 0;
            this.rbtn_gtime_current.TabStop = true;
            this.rbtn_gtime_current.Text = "使用当前时间";
            this.rbtn_gtime_current.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_random_value);
            this.groupBox3.Controls.Add(this.rbtn_gvalue_random);
            this.groupBox3.Controls.Add(this.rbtn_gvalue_fixed);
            this.groupBox3.Location = new System.Drawing.Point(699, 241);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(327, 86);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "模拟值";
            // 
            // btn_random_value
            // 
            this.btn_random_value.Location = new System.Drawing.Point(112, 26);
            this.btn_random_value.Name = "btn_random_value";
            this.btn_random_value.Size = new System.Drawing.Size(108, 33);
            this.btn_random_value.TabIndex = 2;
            this.btn_random_value.Text = "随机生成一次";
            this.btn_random_value.UseVisualStyleBackColor = true;
            // 
            // rbtn_gvalue_random
            // 
            this.rbtn_gvalue_random.AutoSize = true;
            this.rbtn_gvalue_random.Location = new System.Drawing.Point(8, 57);
            this.rbtn_gvalue_random.Name = "rbtn_gvalue_random";
            this.rbtn_gvalue_random.Size = new System.Drawing.Size(86, 21);
            this.rbtn_gvalue_random.TabIndex = 1;
            this.rbtn_gvalue_random.TabStop = true;
            this.rbtn_gvalue_random.Text = "随机生成值";
            this.rbtn_gvalue_random.UseVisualStyleBackColor = true;
            // 
            // rbtn_gvalue_fixed
            // 
            this.rbtn_gvalue_fixed.AutoSize = true;
            this.rbtn_gvalue_fixed.Location = new System.Drawing.Point(8, 30);
            this.rbtn_gvalue_fixed.Name = "rbtn_gvalue_fixed";
            this.rbtn_gvalue_fixed.Size = new System.Drawing.Size(86, 21);
            this.rbtn_gvalue_fixed.TabIndex = 0;
            this.rbtn_gvalue_fixed.TabStop = true;
            this.rbtn_gvalue_fixed.Text = "固定当前值";
            this.rbtn_gvalue_fixed.UseVisualStyleBackColor = true;
            // 
            // btn_start
            // 
            this.btn_start.Font = new System.Drawing.Font("黑体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_start.Location = new System.Drawing.Point(698, 333);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(328, 45);
            this.btn_start.TabIndex = 10;
            this.btn_start.Text = "插入数据";
            this.btn_start.UseVisualStyleBackColor = true;
            // 
            // txt_log
            // 
            this.txt_log.Location = new System.Drawing.Point(699, 384);
            this.txt_log.Name = "txt_log";
            this.txt_log.Size = new System.Drawing.Size(322, 368);
            this.txt_log.TabIndex = 11;
            this.txt_log.Text = "";
            // 
            // SimulateMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 771);
            this.Controls.Add(this.txt_log);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.menu_main);
            this.Controls.Add(this.gridView);
            this.Name = "SimulateMain";
            this.Text = "模拟采集工具";
            this.Load += new System.EventHandler(this.SimulateMain_Load);
            this.menu_main.ResumeLayout(false);
            this.menu_main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu_main;
        private System.Windows.Forms.ToolStripMenuItem menu_dbconfig;
        private System.Windows.Forms.DataGridView gridView;
        private DateTimePicker dtp_point_time;
        private RadioButton rbtn_ginsert_single;
        private RadioButton rbtn_ginsert_circle;
        private GroupBox groupBox2;
        private Label label2;
        private TextBox txt_circle_time;
        private GroupBox groupBox1;
        private RadioButton rbtn_gtime_point;
        private RadioButton rbtn_gtime_current;
        private GroupBox groupBox3;
        private Button btn_random_value;
        private RadioButton rbtn_gvalue_random;
        private RadioButton rbtn_gvalue_fixed;
        private Button btn_start;
        private RichTextBox txt_log;
    }
}