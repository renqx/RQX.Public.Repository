﻿using Microsoft.Extensions.DependencyInjection;
using RQX.Common.Web.Web.Startup;

namespace RQX.Common.Web.Web.DynamicApi
{
    public static class DynamicApiServiceExtension
    {
        public static void AddDynamicApiService(this IServiceCollection services)
        {
            var partManager = services.GetSingletonInstanceOrNull<ApplicationPartManager>();

            //var removeFeature = partManager.FeatureProviders.Where(k => k.GetType() == typeof(DynamicApiControllerFeatureProvider));
            //removeFeature.ToList().ForEach(feature => partManager.FeatureProviders.Remove(feature));

            partManager.FeatureProviders.Add(new DynamicApiControllerFeatureProvider());

            services.Configure<MvcOptions>(o =>
            {
                //o.Conventions.Clear();
                // Register Controller Routing Information Converter
                o.Conventions.Add(new ApiServiceConvention(services));
            });
        }

    }
}
