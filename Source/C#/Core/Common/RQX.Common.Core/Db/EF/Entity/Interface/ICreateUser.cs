﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity.Interface
{
    /// <summary>
    /// 包含添加的信息
    /// </summary>
    public interface ICreateUser
    {
        /// <summary>
        /// 创建人ID
        /// </summary>
        int CreateUserId { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        string CreateUserName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime CreateTime { get; set; }
    }
}
