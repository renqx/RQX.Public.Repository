﻿namespace RQX.Common.Web.Logger
{
    /// <summary>
    /// 日志级别
    /// </summary>
    public enum LoggerLevel
    {
        Level0 = 0,
        Level1, //LoggerLevel.UnCatchException | Level0,
        Level2,
        Level3,
        Level4,
        Level5,
        Level6,
    }
}
