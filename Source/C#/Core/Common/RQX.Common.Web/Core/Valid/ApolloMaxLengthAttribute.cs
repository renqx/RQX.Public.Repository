﻿using RQX.Common.Web.Tools;
using System.ComponentModel.DataAnnotations;

namespace RQX.Common.Web.Core.Valid
{
    public class ApolloMaxLengthAttribute : MaxLengthAttribute
    {
        //The field Name must be a string or array type with a maximum length of '2'.
        private const string DefaultErrorMessage = "参数{0}支持的最大长度为{1}";
        public ApolloMaxLengthAttribute(int length) : base(length)
        {
        }

        public ApolloMaxLengthAttribute(int length, string errorMessage) : this(length)
        {
            ErrorMessage = errorMessage;
        }
        public override string FormatErrorMessage(string name)
        {
            if (ErrorMessage.IsNullOrEmpty())
            {
                return string.Format(DefaultErrorMessage, name, Length);
            }
            else
            {
                return ErrorMessage;
            }
        }
    }
}
