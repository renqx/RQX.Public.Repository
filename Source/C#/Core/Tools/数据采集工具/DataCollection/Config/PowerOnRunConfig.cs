﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataCollection.Config
{
    public class PowerOnRunConfig
    {
        private static bool Is64 = Environment.Is64BitOperatingSystem;
        private static RegistryKey LocalMachine = Registry.LocalMachine;
        private const string RunPath64 = @"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Run";
        private const string RunPath32 = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        private static string RunPath = Is64 ? RunPath64 : RunPath32;
        /// <summary>
        /// 判断是否已经设置自启动
        /// </summary>
        /// <returns></returns>
        public static bool CheckIsSet()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            GetRegisterList(LocalMachine, RunPath, dic);
            var list = dic.Where(k => k.Key.Equals(AppDomain.CurrentDomain.FriendlyName) || k.Key.Equals($"{AppDomain.CurrentDomain.FriendlyName}.exe"));
            if (list.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 设置自启动/取消自启动
        /// </summary>
        /// <param name="isPowerOnRun"></param>
        public static void Set(bool isPowerOnRun, bool IsInit = true)
        {
            if (IsInit) return;
            if (isPowerOnRun)
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.FriendlyName);
                using (RegistryKey runKey = Registry.LocalMachine.CreateSubKey(RunPath))
                {
                    runKey.SetValue(AppDomain.CurrentDomain.FriendlyName, path, RegistryValueKind.String);
                }
                DisableUAC();
            }
            else
            {
                using (RegistryKey runKey = Registry.LocalMachine.OpenSubKey(RunPath, true))
                {
                    try
                    {
                        runKey.DeleteValue(AppDomain.CurrentDomain.FriendlyName);
                        runKey.DeleteValue($"{AppDomain.CurrentDomain.FriendlyName}.exe");
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// 获取注册表相关信息
        /// </summary>
        /// <param name="baseKey"></param>
        /// <param name="path"></param>
        /// <param name="dic"></param>
        private static void GetRegisterList(RegistryKey baseKey, string path, Dictionary<string, string> dic)
        {
            var basePath = $@"{baseKey}\{path}";
            using (RegistryKey runKey = baseKey.OpenSubKey(path))
            {
                var names = runKey?.GetValueNames();
                if (names == null) return;
                foreach (var name in names)
                {
                    dic.Add(name, basePath);
                }
            }
        }

        private static void DisableUAC()
        {
            string path = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System";
            string uac = "EnableLUA";
            using (RegistryKey key = Registry.LocalMachine.CreateSubKey(path))
            {
                if (key != null)
                {
                    key.SetValue(uac, 0, RegistryValueKind.DWord);
                }
            }
        }
    }
}
