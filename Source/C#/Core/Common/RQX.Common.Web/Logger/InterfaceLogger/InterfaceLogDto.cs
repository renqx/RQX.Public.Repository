﻿using RQX.Common.Web.Core.AutoMapper;
using RQX.Common.Web.Core.Dto;
using System;

namespace RQX.Common.Web.Logger.InterfaceLogger
{
    [AutoMapper(typeof(InterfaceLog))]
    public class InterfaceLogDto : CreateUserDto
    {
        /// <summary>
        /// 请求来源IP地址
        /// </summary>
        public string source_ip { get; set; }
        /// <summary>
        /// 请求来源端口
        /// </summary>
        public int source_port { get; set; }
        /// <summary>
        /// 请求目标IP地址
        /// </summary>
        public string target_ip { get; set; }
        /// <summary>
        /// 请求目标端口
        /// </summary>
        public int target_port { get; set; }
        /// <summary>
        /// HTTP请求方式
        /// </summary>
        public string http_method { get; set; }
        /// <summary>
        /// 请求area名称
        /// </summary>
        public string area_name { get; set; }
        /// <summary>
        /// 请求controller名称
        /// </summary>
        public string controller_name { get; set; }
        /// <summary>
        /// 请求action名称
        /// </summary>
        public string action_name { get; set; }
        /// <summary>
        /// 请求完整路由地址
        /// </summary>
        public string request_route { get; set; }
        /// <summary>
        /// 请求url地址
        /// </summary>
        public string request_url { get; set; }
        /// <summary>
        /// 请求参数
        /// </summary>
        public string request_params { get; set; }
        /// <summary>
        /// 请求头
        /// </summary>
        public string request_header { get; set; }
        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime request_time { get; set; }
        /// <summary>
        /// 响应时间
        /// </summary>
        public DateTime response_time { get; set; }
        /// <summary>
        /// 响应json数据
        /// </summary>
        public string response_json { get; set; }
        /// <summary>
        /// 接口耗时，单位毫秒
        /// </summary>
        public double use_time { get; set; }
        /// <summary>
        /// 异常编号
        /// </summary>
        public string exception_code { get; set; }
    }
}
