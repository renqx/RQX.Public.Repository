﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.BaseCollection;
using RQX.Common.Framework.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TCP_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClientDemo demo = new TcpClientDemo(new StructureModel<TcpSocketClient>()
            {
                Conn = new TcpSocketClient(5050)
            });
            demo.DealDataHandler = (dic) => dic.ForEach(item => Console.WriteLine($"{item.Key}\t{item.Value}"));
            demo.Start();
            Console.ReadKey();
        }
    }
}
