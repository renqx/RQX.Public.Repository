﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Plugs.Drager_Primus
{
    class Program
    {
        static void Main(string[] args)
        {
            StructureModel<MyCom> model = new StructureModel<MyCom>
            {
                Conn = new MyCom("COM3")
            };
            Primus primus = new Primus(model);
            primus.DealDataHandler = (dic) =>
            {
                dic.ForEach(item =>
                {
                    Console.WriteLine($"{item.Key}\t{item.Value}");
                });
            };
            primus.Start();
        }
    }
}
