﻿using RQX.Common.Core.Db.EF.Entity.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity
{
    /// <summary>
    /// 带创建者的实体
    /// </summary>
    public class CreateUserEntity : BaseEntity, ICreateUser
    {
        /// <summary>
        /// 创建人ID
        /// </summary>
        [Column("create_user_id")]
        public int CreateUserId { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        [Column("create_user_name")]
        [MaxLength(50)]
        public string CreateUserName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("create_time", TypeName = "datetime")]
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
