﻿
using RQX.Common.Web.Core.Dto;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带修改者的树形传输实体
    /// </summary>
    public class UpdateUserTreeDto : UpdateUserDto
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
