﻿using Microsoft.AspNetCore.Mvc.Filters;
using RQX.Common.Web.Config;
using RQX.Common.Web.Logger.ExceptionLogger;
using RQX.Common.Web.Logger.InterfaceLogger;
using RQX.Common.Web.Tools;
using RQX.Common.Web.Web.Filter;
using System;

namespace RQX.Common.Web.Logger
{
    /// <summary>
    /// 日志过滤处理
    /// </summary>
    public class LoggerFilter
    {
        private static int LoggerLevel = -1;
        static LoggerFilter()
        {
            var level = AppConfigurtaion.GetConfigValueInt(GlobalConfig.LoggerLevel);
            if (level != null)
            {
                LoggerLevel = level.Value;
            }
        }


        /// <summary>
        /// 过滤器进入，初始化接口调用参数
        /// </summary>
        /// <param name="context"></param>
        public static void InterfaceLogInit(ActionExecutingContext context)
        {
            if (LoggerLevel >= 2)//2开始需要记录接口信息
            {
                try
                {
                    //构建接口日志信息
                    FakeSession.interfaceEntity.Value = new InterfaceLog(context);
                }
                catch (Exception ex)
                {
                    LoggerHelper.WriteLocalLog($"初始化" + ex.ToString());
                }
            }
        }
        /// <summary>
        /// 过滤器正常返回，处理日志记录
        /// </summary>
        /// <param name="service"></param>
        /// <param name="resultValue"></param>
        public static void InterfaceLogSuccessed(InterfaceLogService service, object resultValue)
        {
            if (LoggerLevel >= 7)//7开始正常返回简要信息
            {
                try
                {
                    var interfaceDto = FakeSession.interfaceEntity.Value;
                    interfaceDto.AddResponseJson(resultValue.ToJson());
                    if (LoggerLevel == 7)//7正常返回接口简要信息
                    {
                        service.Add(interfaceDto.TransferToSimple());
                    }
                    else//8正常返回接口详细信息
                    {
                        service.Add(interfaceDto);
                    }
                }
                catch (Exception ex)
                {
                    var obj = FakeSession.interfaceEntity.Value.ToJson();
                    LoggerHelper.WriteLocalLog($"返回:\n{ex.ToString()}\n{obj}\n{resultValue.ToJson()}\n");
                }
            }
        }

        /// <summary>
        /// 过滤器异常返回，处理异常日志，并返回异常代码
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="service"></param>
        public static string ExceptionLogException(Exception ex, ExceptionLogService service)
        {
            if (LoggerLevel >= 1)//0不记录
            {
                try
                {
                    string typeName;
                    string code = string.Empty;
                    if (ex is ApolloException apolloException)
                    {
                        if (LoggerLevel >= 4)//4 开始记录所有异常
                        {
                            typeName = apolloException.ExceptionType.ToString();
                            code = service.Add(new ExceptionLog(ex, typeName)).code;
                        }
                    }
                    else
                    {
                        typeName = "未经捕获的异常";
                        code = service.Add(new ExceptionLog(ex, typeName)).code;
                    }
                    return code;
                }
                catch (Exception ex1)
                {
                    var obj = FakeSession.interfaceEntity.Value.ToJson();
                    LoggerHelper.WriteLocalLog($"异常捕获1\n{ex.ToString()}\n{ex1.ToString()}\n{obj}");
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 过滤器异常返回，处理接口日志
        /// </summary>
        /// <param name="isApolloException"></param>
        /// <param name="exceptionCode"></param>
        /// <param name="responseJson"></param>
        /// <param name="service"></param>
        public static void ExceptionLogInterface(bool isApolloException, string exceptionCode, string responseJson, InterfaceLogService service)
        {
            if (LoggerLevel >= 2)//2开始记录异常接口简要信息
            {
                try
                {
                    var interfaceDto = FakeSession.interfaceEntity.Value;
                    interfaceDto.AddResponseJson(responseJson);
                    interfaceDto.exception_code = exceptionCode;
                    if (isApolloException)//捕获的异常
                    {
                        if (LoggerLevel == 5)//捕获的异常接口简要信息
                        {
                            service.Add(interfaceDto.TransferToSimple());
                        }
                        else if (LoggerLevel > 5)//捕获的异常接口详细信息
                        {
                            service.Add(interfaceDto);
                        }
                    }
                    else//未捕获的异常
                    {
                        if (LoggerLevel == 2)//未捕获的异常接口简要信息
                        {
                            service.Add(interfaceDto.TransferToSimple());
                        }
                        else//未捕获的异常接口详细信息
                        {
                            service.Add(interfaceDto);
                        }
                    }
                }
                catch (Exception ex2)
                {
                    var obj = FakeSession.interfaceEntity.Value.ToJson();
                    LoggerHelper.WriteLocalLog($"异常捕获2\n{ex2.ToString()}\n{obj}");
                }
            }

        }

    }
}
