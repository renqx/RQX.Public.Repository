﻿using System.Drawing;

namespace DataCollection.Manage
{
    partial class DeviceManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeviceManage));
            this.menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_add = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_edit = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_del = new System.Windows.Forms.ToolStripMenuItem();
            this.dgv_guid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_device_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_plug_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_protype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_proparam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grid = new System.Windows.Forms.DataGridView();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_add,
            this.menu_edit,
            this.menu_del});
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(101, 70);
            // 
            // menu_add
            // 
            this.menu_add.Name = "menu_add";
            this.menu_add.Size = new System.Drawing.Size(100, 22);
            this.menu_add.Text = "添加";
            this.menu_add.Click += new System.EventHandler(this.menu_add_Click);
            // 
            // menu_edit
            // 
            this.menu_edit.Name = "menu_edit";
            this.menu_edit.Size = new System.Drawing.Size(100, 22);
            this.menu_edit.Text = "修改";
            this.menu_edit.Click += new System.EventHandler(this.menu_edit_Click);
            // 
            // menu_del
            // 
            this.menu_del.Name = "menu_del";
            this.menu_del.Size = new System.Drawing.Size(100, 22);
            this.menu_del.Text = "删除";
            this.menu_del.Click += new System.EventHandler(this.menu_del_Click);
            // 
            // dgv_guid
            // 
            this.dgv_guid.HeaderText = "唯一标识";
            this.dgv_guid.Name = "dgv_guid";
            this.dgv_guid.ReadOnly = true;
            this.dgv_guid.Visible = false;
            // 
            // dgv_device_name
            // 
            this.dgv_device_name.HeaderText = "终端设备名称";
            this.dgv_device_name.Name = "dgv_device_name";
            this.dgv_device_name.ReadOnly = true;
            this.dgv_device_name.Width = 120;
            // 
            // dgv_plug_name
            // 
            this.dgv_plug_name.HeaderText = "采集插件名称";
            this.dgv_plug_name.Name = "dgv_plug_name";
            this.dgv_plug_name.ReadOnly = true;
            this.dgv_plug_name.Width = 200;
            // 
            // dgv_protype
            // 
            this.dgv_protype.HeaderText = "协议类型";
            this.dgv_protype.Name = "dgv_protype";
            this.dgv_protype.ReadOnly = true;
            this.dgv_protype.Width = 80;
            // 
            // dgv_proparam
            // 
            this.dgv_proparam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_proparam.HeaderText = "协议参数";
            this.dgv_proparam.Name = "dgv_proparam";
            this.dgv_proparam.ReadOnly = true;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_guid,
            this.dgv_device_name,
            this.dgv_plug_name,
            this.dgv_protype,
            this.dgv_proparam});
            this.grid.Location = new System.Drawing.Point(13, 13);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(1009, 360);
            this.grid.TabIndex = 0;
            // 
            // DeviceManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 383);
            this.ContextMenuStrip = this.menu;
            this.Controls.Add(this.grid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeviceManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设备终端管理";
            this.Load += new System.EventHandler(this.DeviceManage_Load);
            this.menu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menu_add;
        private System.Windows.Forms.ToolStripMenuItem menu_edit;
        private System.Windows.Forms.ToolStripMenuItem menu_del;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_guid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_device_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_plug_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_protype;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_proparam;
    }
}