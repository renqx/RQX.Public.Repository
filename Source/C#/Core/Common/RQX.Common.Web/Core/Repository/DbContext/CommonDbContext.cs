﻿using Microsoft.EntityFrameworkCore;
using RQX.Common.Web.Logger.ExceptionLogger;
using RQX.Common.Web.Logger.InterfaceLogger;
using RQX.Common.Web.Logger.OperationLogger;

namespace RQX.Common.Web.Core.Repository.DbContext
{
    public class CommonDbContext : BaseDbContext
    {
        public CommonDbContext() { }

        public CommonDbContext(DbContextOptions<BaseDbContext> options)
            : base(options)
        {
        }

        public DbSet<ExceptionLog> ExceptionLogs { get; set; }
        public DbSet<InterfaceLog> InterfaceLogs { get; set; }
        public DbSet<OperationLog> OperationLogs { get; set; }
    }
}
