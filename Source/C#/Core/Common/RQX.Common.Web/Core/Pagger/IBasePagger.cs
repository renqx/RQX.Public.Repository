﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 分页接口
    /// </summary>
    public interface IBasePagger
    {
        /// <summary>
        /// 页数，第一页从1开始
        /// </summary>
        int PageIndex { get; set; }
        /// <summary>
        /// 每页数据量
        /// </summary>
        int PageSize { get; set; }
        /// <summary>
        /// 获取分页跳过的数据数量
        /// </summary>
        /// <returns></returns>
        int GetSkipNum()
        {
            if (PageIndex < 1)
            {
                PageIndex = 1;
            }
            return (PageIndex - 1) * PageSize;
        }
    }
}
