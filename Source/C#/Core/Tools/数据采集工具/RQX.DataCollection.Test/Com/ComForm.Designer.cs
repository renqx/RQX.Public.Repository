﻿namespace RQX.DataCollection.Test.Com
{
    partial class ComForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ddl_StopBits = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ddl_DataBits = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddl_Parity = new System.Windows.Forms.ComboBox();
            this.ddl_BaudRate = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ddl_PortName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_recv_Clean = new System.Windows.Forms.Button();
            this.btn_recv_Save = new System.Windows.Forms.Button();
            this.ckb_recv_Pause = new System.Windows.Forms.CheckBox();
            this.ckb_recv_Hex = new System.Windows.Forms.CheckBox();
            this.ckb_recv_NewLine = new System.Windows.Forms.CheckBox();
            this.ckb_recv_AutoSave = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_send_CircleTime = new System.Windows.Forms.TextBox();
            this.ckb_send_Circle = new System.Windows.Forms.CheckBox();
            this.ckb_send_Hex = new System.Windows.Forms.CheckBox();
            this.ckb_send_AutoClean = new System.Windows.Forms.CheckBox();
            this.ckb_send_File = new System.Windows.Forms.CheckBox();
            this.txt_Recv = new System.Windows.Forms.RichTextBox();
            this.txt_Send = new System.Windows.Forms.RichTextBox();
            this.btn_Send = new System.Windows.Forms.Button();
            this.ckb_DTR = new System.Windows.Forms.CheckBox();
            this.ckb_RTS = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckb_RTS);
            this.groupBox1.Controls.Add(this.ckb_DTR);
            this.groupBox1.Controls.Add(this.btn_Connect);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ddl_StopBits);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ddl_DataBits);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ddl_Parity);
            this.groupBox1.Controls.Add(this.ddl_BaudRate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ddl_PortName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 227);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设置";
            // 
            // btn_Connect
            // 
            this.btn_Connect.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Connect.ForeColor = System.Drawing.Color.Red;
            this.btn_Connect.Location = new System.Drawing.Point(62, 172);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(78, 48);
            this.btn_Connect.TabIndex = 2;
            this.btn_Connect.Text = "连 接";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "停止位";
            // 
            // ddl_StopBits
            // 
            this.ddl_StopBits.FormattingEnabled = true;
            this.ddl_StopBits.Location = new System.Drawing.Point(62, 141);
            this.ddl_StopBits.Name = "ddl_StopBits";
            this.ddl_StopBits.Size = new System.Drawing.Size(78, 25);
            this.ddl_StopBits.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "数据位";
            // 
            // ddl_DataBits
            // 
            this.ddl_DataBits.FormattingEnabled = true;
            this.ddl_DataBits.Location = new System.Drawing.Point(62, 110);
            this.ddl_DataBits.Name = "ddl_DataBits";
            this.ddl_DataBits.Size = new System.Drawing.Size(78, 25);
            this.ddl_DataBits.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "校验位";
            // 
            // ddl_Parity
            // 
            this.ddl_Parity.FormattingEnabled = true;
            this.ddl_Parity.Location = new System.Drawing.Point(62, 79);
            this.ddl_Parity.Name = "ddl_Parity";
            this.ddl_Parity.Size = new System.Drawing.Size(78, 25);
            this.ddl_Parity.TabIndex = 1;
            // 
            // ddl_BaudRate
            // 
            this.ddl_BaudRate.FormattingEnabled = true;
            this.ddl_BaudRate.Location = new System.Drawing.Point(62, 48);
            this.ddl_BaudRate.Name = "ddl_BaudRate";
            this.ddl_BaudRate.Size = new System.Drawing.Size(78, 25);
            this.ddl_BaudRate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "波特率";
            // 
            // ddl_PortName
            // 
            this.ddl_PortName.FormattingEnabled = true;
            this.ddl_PortName.Location = new System.Drawing.Point(62, 17);
            this.ddl_PortName.Name = "ddl_PortName";
            this.ddl_PortName.Size = new System.Drawing.Size(78, 25);
            this.ddl_PortName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "串口号";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_recv_Clean);
            this.groupBox2.Controls.Add(this.btn_recv_Save);
            this.groupBox2.Controls.Add(this.ckb_recv_Pause);
            this.groupBox2.Controls.Add(this.ckb_recv_Hex);
            this.groupBox2.Controls.Add(this.ckb_recv_NewLine);
            this.groupBox2.Controls.Add(this.ckb_recv_AutoSave);
            this.groupBox2.Location = new System.Drawing.Point(12, 245);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 191);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "接收区设置";
            // 
            // btn_recv_Clean
            // 
            this.btn_recv_Clean.Location = new System.Drawing.Point(12, 160);
            this.btn_recv_Clean.Name = "btn_recv_Clean";
            this.btn_recv_Clean.Size = new System.Drawing.Size(128, 23);
            this.btn_recv_Clean.TabIndex = 1;
            this.btn_recv_Clean.Text = "清除接收区显示";
            this.btn_recv_Clean.UseVisualStyleBackColor = true;
            this.btn_recv_Clean.Click += new System.EventHandler(this.btn_recv_Clean_Click);
            // 
            // btn_recv_Save
            // 
            this.btn_recv_Save.Location = new System.Drawing.Point(12, 131);
            this.btn_recv_Save.Name = "btn_recv_Save";
            this.btn_recv_Save.Size = new System.Drawing.Size(128, 23);
            this.btn_recv_Save.TabIndex = 1;
            this.btn_recv_Save.Text = "保存接收区数据";
            this.btn_recv_Save.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_Pause
            // 
            this.ckb_recv_Pause.AutoSize = true;
            this.ckb_recv_Pause.Location = new System.Drawing.Point(17, 103);
            this.ckb_recv_Pause.Name = "ckb_recv_Pause";
            this.ckb_recv_Pause.Size = new System.Drawing.Size(99, 21);
            this.ckb_recv_Pause.TabIndex = 0;
            this.ckb_recv_Pause.Text = "暂停接收显示";
            this.ckb_recv_Pause.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_Hex
            // 
            this.ckb_recv_Hex.AutoSize = true;
            this.ckb_recv_Hex.Location = new System.Drawing.Point(17, 76);
            this.ckb_recv_Hex.Name = "ckb_recv_Hex";
            this.ckb_recv_Hex.Size = new System.Drawing.Size(99, 21);
            this.ckb_recv_Hex.TabIndex = 0;
            this.ckb_recv_Hex.Text = "十六进制显示";
            this.ckb_recv_Hex.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_NewLine
            // 
            this.ckb_recv_NewLine.AutoSize = true;
            this.ckb_recv_NewLine.Location = new System.Drawing.Point(17, 49);
            this.ckb_recv_NewLine.Name = "ckb_recv_NewLine";
            this.ckb_recv_NewLine.Size = new System.Drawing.Size(75, 21);
            this.ckb_recv_NewLine.TabIndex = 0;
            this.ckb_recv_NewLine.Text = "自动换行";
            this.ckb_recv_NewLine.UseVisualStyleBackColor = true;
            // 
            // ckb_recv_AutoSave
            // 
            this.ckb_recv_AutoSave.AutoSize = true;
            this.ckb_recv_AutoSave.Location = new System.Drawing.Point(17, 22);
            this.ckb_recv_AutoSave.Name = "ckb_recv_AutoSave";
            this.ckb_recv_AutoSave.Size = new System.Drawing.Size(111, 21);
            this.ckb_recv_AutoSave.TabIndex = 0;
            this.ckb_recv_AutoSave.Text = "自动保存到文件";
            this.ckb_recv_AutoSave.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txt_send_CircleTime);
            this.groupBox3.Controls.Add(this.ckb_send_Circle);
            this.groupBox3.Controls.Add(this.ckb_send_Hex);
            this.groupBox3.Controls.Add(this.ckb_send_AutoClean);
            this.groupBox3.Controls.Add(this.ckb_send_File);
            this.groupBox3.Location = new System.Drawing.Point(13, 442);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 162);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "发送区设置";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "毫秒";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "循环间隔";
            // 
            // txt_send_CircleTime
            // 
            this.txt_send_CircleTime.Location = new System.Drawing.Point(62, 130);
            this.txt_send_CircleTime.Name = "txt_send_CircleTime";
            this.txt_send_CircleTime.Size = new System.Drawing.Size(47, 23);
            this.txt_send_CircleTime.TabIndex = 1;
            // 
            // ckb_send_Circle
            // 
            this.ckb_send_Circle.AutoSize = true;
            this.ckb_send_Circle.Location = new System.Drawing.Point(17, 103);
            this.ckb_send_Circle.Name = "ckb_send_Circle";
            this.ckb_send_Circle.Size = new System.Drawing.Size(75, 21);
            this.ckb_send_Circle.TabIndex = 0;
            this.ckb_send_Circle.Text = "循环发送";
            this.ckb_send_Circle.UseVisualStyleBackColor = true;
            // 
            // ckb_send_Hex
            // 
            this.ckb_send_Hex.AutoSize = true;
            this.ckb_send_Hex.Location = new System.Drawing.Point(17, 76);
            this.ckb_send_Hex.Name = "ckb_send_Hex";
            this.ckb_send_Hex.Size = new System.Drawing.Size(99, 21);
            this.ckb_send_Hex.TabIndex = 0;
            this.ckb_send_Hex.Text = "十六进制发送";
            this.ckb_send_Hex.UseVisualStyleBackColor = true;
            // 
            // ckb_send_AutoClean
            // 
            this.ckb_send_AutoClean.AutoSize = true;
            this.ckb_send_AutoClean.Location = new System.Drawing.Point(17, 49);
            this.ckb_send_AutoClean.Name = "ckb_send_AutoClean";
            this.ckb_send_AutoClean.Size = new System.Drawing.Size(111, 21);
            this.ckb_send_AutoClean.TabIndex = 0;
            this.ckb_send_AutoClean.Text = "发送完自动清空";
            this.ckb_send_AutoClean.UseVisualStyleBackColor = true;
            // 
            // ckb_send_File
            // 
            this.ckb_send_File.AutoSize = true;
            this.ckb_send_File.Location = new System.Drawing.Point(17, 22);
            this.ckb_send_File.Name = "ckb_send_File";
            this.ckb_send_File.Size = new System.Drawing.Size(111, 21);
            this.ckb_send_File.TabIndex = 0;
            this.ckb_send_File.Text = "从文件读取发送";
            this.ckb_send_File.UseVisualStyleBackColor = true;
            // 
            // txt_Recv
            // 
            this.txt_Recv.Location = new System.Drawing.Point(169, 13);
            this.txt_Recv.Name = "txt_Recv";
            this.txt_Recv.Size = new System.Drawing.Size(511, 484);
            this.txt_Recv.TabIndex = 3;
            this.txt_Recv.Text = "";
            // 
            // txt_Send
            // 
            this.txt_Send.Location = new System.Drawing.Point(168, 503);
            this.txt_Send.Name = "txt_Send";
            this.txt_Send.Size = new System.Drawing.Size(422, 101);
            this.txt_Send.TabIndex = 4;
            this.txt_Send.Text = "";
            // 
            // btn_Send
            // 
            this.btn_Send.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Send.Location = new System.Drawing.Point(596, 503);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(84, 101);
            this.btn_Send.TabIndex = 5;
            this.btn_Send.Text = "发 送";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // ckb_DTR
            // 
            this.ckb_DTR.AutoSize = true;
            this.ckb_DTR.Location = new System.Drawing.Point(12, 172);
            this.ckb_DTR.Name = "ckb_DTR";
            this.ckb_DTR.Size = new System.Drawing.Size(51, 21);
            this.ckb_DTR.TabIndex = 3;
            this.ckb_DTR.Text = "DTR";
            this.ckb_DTR.UseVisualStyleBackColor = true;
            // 
            // ckb_RTS
            // 
            this.ckb_RTS.AutoSize = true;
            this.ckb_RTS.Location = new System.Drawing.Point(12, 199);
            this.ckb_RTS.Name = "ckb_RTS";
            this.ckb_RTS.Size = new System.Drawing.Size(49, 21);
            this.ckb_RTS.TabIndex = 4;
            this.ckb_RTS.Text = "RTS";
            this.ckb_RTS.UseVisualStyleBackColor = true;
            // 
            // ComForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 613);
            this.Controls.Add(this.btn_Send);
            this.Controls.Add(this.txt_Send);
            this.Controls.Add(this.txt_Recv);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ComForm";
            this.Text = "串口采集测试工具";
            this.Load += new System.EventHandler(this.ComForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddl_StopBits;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddl_DataBits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ddl_Parity;
        private System.Windows.Forms.ComboBox ddl_BaudRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddl_PortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_recv_Clean;
        private System.Windows.Forms.Button btn_recv_Save;
        private System.Windows.Forms.CheckBox ckb_recv_Pause;
        private System.Windows.Forms.CheckBox ckb_recv_Hex;
        private System.Windows.Forms.CheckBox ckb_recv_NewLine;
        private System.Windows.Forms.CheckBox ckb_recv_AutoSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_send_CircleTime;
        private System.Windows.Forms.CheckBox ckb_send_Circle;
        private System.Windows.Forms.CheckBox ckb_send_Hex;
        private System.Windows.Forms.CheckBox ckb_send_AutoClean;
        private System.Windows.Forms.CheckBox ckb_send_File;
        private System.Windows.Forms.RichTextBox txt_Recv;
        private System.Windows.Forms.RichTextBox txt_Send;
        private System.Windows.Forms.Button btn_Send;
        private System.Windows.Forms.CheckBox ckb_DTR;
        private System.Windows.Forms.CheckBox ckb_RTS;
    }
}