﻿namespace RQX.DataCollection.Test
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_com = new System.Windows.Forms.Button();
            this.btn_tcp_client = new System.Windows.Forms.Button();
            this.btn_tcp_server = new System.Windows.Forms.Button();
            this.btn_udp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_com
            // 
            this.btn_com.Location = new System.Drawing.Point(13, 13);
            this.btn_com.Name = "btn_com";
            this.btn_com.Size = new System.Drawing.Size(227, 32);
            this.btn_com.TabIndex = 0;
            this.btn_com.Text = "串口采集测试工具";
            this.btn_com.UseVisualStyleBackColor = true;
            this.btn_com.Click += new System.EventHandler(this.btn_com_Click);
            // 
            // btn_tcp_client
            // 
            this.btn_tcp_client.Location = new System.Drawing.Point(12, 51);
            this.btn_tcp_client.Name = "btn_tcp_client";
            this.btn_tcp_client.Size = new System.Drawing.Size(227, 32);
            this.btn_tcp_client.TabIndex = 0;
            this.btn_tcp_client.Text = "TCP客户端采集测试工具";
            this.btn_tcp_client.UseVisualStyleBackColor = true;
            this.btn_tcp_client.Click += new System.EventHandler(this.btn_tcp_client_Click);
            // 
            // btn_tcp_server
            // 
            this.btn_tcp_server.Location = new System.Drawing.Point(12, 89);
            this.btn_tcp_server.Name = "btn_tcp_server";
            this.btn_tcp_server.Size = new System.Drawing.Size(227, 32);
            this.btn_tcp_server.TabIndex = 0;
            this.btn_tcp_server.Text = "TCP服务端采集测试工具";
            this.btn_tcp_server.UseVisualStyleBackColor = true;
            this.btn_tcp_server.Click += new System.EventHandler(this.btn_tcp_server_Click);
            // 
            // btn_udp
            // 
            this.btn_udp.Location = new System.Drawing.Point(12, 127);
            this.btn_udp.Name = "btn_udp";
            this.btn_udp.Size = new System.Drawing.Size(227, 32);
            this.btn_udp.TabIndex = 0;
            this.btn_udp.Text = "UDP采集测试工具";
            this.btn_udp.UseVisualStyleBackColor = true;
            this.btn_udp.Click += new System.EventHandler(this.btn_udp_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 170);
            this.Controls.Add(this.btn_udp);
            this.Controls.Add(this.btn_tcp_server);
            this.Controls.Add(this.btn_tcp_client);
            this.Controls.Add(this.btn_com);
            this.Name = "Main";
            this.Text = "数据采集测试工具";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_com;
        private System.Windows.Forms.Button btn_tcp_client;
        private System.Windows.Forms.Button btn_tcp_server;
        private System.Windows.Forms.Button btn_udp;
    }
}

