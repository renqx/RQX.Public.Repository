﻿namespace 开机启动项设置工具
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_program = new System.Windows.Forms.TextBox();
            this.dgv_grid = new System.Windows.Forms.DataGridView();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "要设置的程序";
            // 
            // txt_program
            // 
            this.txt_program.Location = new System.Drawing.Point(99, 10);
            this.txt_program.Name = "txt_program";
            this.txt_program.Size = new System.Drawing.Size(619, 23);
            this.txt_program.TabIndex = 1;
            this.txt_program.Click += new System.EventHandler(this.txt_program_Click);
            // 
            // dgv_grid
            // 
            this.dgv_grid.AllowUserToAddRows = false;
            this.dgv_grid.AllowUserToDeleteRows = false;
            this.dgv_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_grid.Location = new System.Drawing.Point(13, 39);
            this.dgv_grid.Name = "dgv_grid";
            this.dgv_grid.ReadOnly = true;
            this.dgv_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_grid.Size = new System.Drawing.Size(705, 276);
            this.dgv_grid.TabIndex = 2;
            this.dgv_grid.Text = "dataGridView1";
            this.dgv_grid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_grid_CellClick);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(724, 10);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(114, 44);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "添加到开机启动";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(724, 60);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(114, 44);
            this.btn_remove.TabIndex = 3;
            this.btn_remove.Text = "从开机启动移除";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 326);
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.dgv_grid);
            this.Controls.Add(this.txt_program);
            this.Controls.Add(this.label1);
            this.Name = "Main";
            this.Text = "开机启动项设置工具";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_program;
        private System.Windows.Forms.DataGridView dgv_grid;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_remove;
    }
}

