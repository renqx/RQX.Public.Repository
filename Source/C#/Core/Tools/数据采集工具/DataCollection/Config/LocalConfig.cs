﻿using RQX.Common.Core.Config;

namespace DataCollection.Config
{
    public class LocalConfig
    {
        /// <summary>
        /// 本机名称/IP
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否托盘启动
        /// </summary>
        public bool NotifyIconRun { get; set; }
        /// <summary>
        /// 启动后是否自动运行采集插件
        /// </summary>
        public bool AutoRunPlug { get; set; }

        public static LocalConfig info { get; set; }

        static LocalConfig()
        {
            var root = ConfigUtils.GetConfigurationRoot().GetSection("LocalConfig");
            info = ConfigUtils.BuildClass<LocalConfig>(root);
        }

        public static void Save()
        {
            var root = ConfigUtils.GetConfigurationRoot().GetSection("LocalConfig");
            root.SetValue($"Name", info.Name);
            root.SetValue($"NotifyIconRun", info.NotifyIconRun);
            root.SetValue($"AutoRunPlug", info.AutoRunPlug);
            ConfigUtils.SaveToFile();
        }

    }
}
