﻿using RQX.Common.Web.Core.AutoMapper;
using RQX.Common.Web.Core.Dto;

namespace RQX.Common.Web.Logger.OperationLogger
{
    /// <summary>
    /// 操作日志记录
    /// </summary>
    [AutoMapper(typeof(OperationLog))]
    public class OperationLogDto : CreateUserDto
    {
        /// <summary>
        /// 详情
        /// </summary>
        public string message { get; set; }



    }
}
