﻿using RQX.Common.Core.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Test.Config
{
    /// <summary>
    /// 通用配置
    /// </summary>
    public class CommonConfig
    {
        #region Recv
        /// <summary>
        /// 自动保存到文件
        /// </summary>
        public bool Recv_AutoSave { get; set; }
        /// <summary>
        /// 自动换行
        /// </summary>
        public bool Recv_NewLine { get; set; }
        /// <summary>
        /// 十六进制显示
        /// </summary>
        public bool Recv_Hex { get; set; }
        /// <summary>
        /// 暂停接收显示
        /// </summary>
        public bool Recv_Stop { get; set; }
        #endregion
        #region Send
        /// <summary>
        /// 从文件读取发送
        /// </summary>
        public bool Send_File { get; set; }
        /// <summary>
        /// 发送完自动清空
        /// </summary>
        public bool Send_AutoClean { get; set; }
        /// <summary>
        /// 十六进制发送
        /// </summary>
        public bool Send_Hex { get; set; }
        /// <summary>
        /// 循环发送
        /// </summary>
        public bool Send_Circle { get; set; }
        /// <summary>
        /// 循环发送间隔
        /// </summary>
        public string Send_Circle_Time { get; set; }
        #endregion

        public virtual void SaveConfig(string path)
        {
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Recv_AutoSave", Recv_AutoSave);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Recv_NewLine", Recv_NewLine);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Recv_Hex", Recv_Hex);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Recv_Stop", Recv_Stop);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Send_File", Send_File);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Send_AutoClean", Send_AutoClean);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Send_Hex", Send_Hex);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Send_Circle", Send_Circle);
            ConfigUtils.GetConfigurationRoot().SetValue($"{path}:Send_Circle_Time", Send_Circle_Time);
        }
    }
}
