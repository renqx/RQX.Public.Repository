﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drager_Primus
{
    class Program
    {
        static void Main(string[] args)
        {
            StructureModel<MyCom> model = new StructureModel<MyCom>
            {
                Conn = new MyCom(1200, "COM4", 8, StopBits.One, Parity.Even)
            };
            Primus primus = new Primus(model);
            primus.DealDataHandler = (dic) =>
            {
                dic.ForEach(item =>
                {
                    Console.WriteLine($"{item.Key}\t{item.Value}");
                });
            };
            primus.Start();
            Console.ReadKey();
        }
    }
}
