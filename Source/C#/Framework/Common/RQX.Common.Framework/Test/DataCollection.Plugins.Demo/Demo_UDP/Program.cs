﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.BaseCollection;
using RQX.Common.Framework.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_UDP
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpDemo demo = new UdpDemo(new StructureModel<UdpSocket>()
            {
                Conn = new UdpSocket(5080),
            });
            demo.DealDataHandler = (dic) => dic.ForEach(item => Console.WriteLine($"{item.Key}\t{item.Value}"));
            demo.Start();
            Console.ReadKey();
        }
    }
}
