﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace RQX.Common.Web.Web.Container
{
    /// <summary>
    /// 容器注入标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class IocInjectionAttribute : Attribute
    {
        public ServiceLifetime ServiceLifeTime { get; set; } = ServiceLifetime.Scoped;
    }
}
