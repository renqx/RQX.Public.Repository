﻿using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Hardware.Net;

namespace DataCollection.ConnectionManage
{
    public class ConnInfo
    {
        public string GUID;
        public ProType ProType;
        public TcpSocketServer tcpServer;//TCP服务端
        public TcpSocketClient tcpClient;//TCP客户端
        public UdpSocket udpClient;//UDP客户端
        public MyCom myCom;//COM         连接
    }
}
