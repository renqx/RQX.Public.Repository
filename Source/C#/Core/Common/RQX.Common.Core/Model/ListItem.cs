﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Model
{
    public class ListItem
    {
        /// <summary>
        /// 显示值
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 实际值
        /// </summary>
        public string Value { get; set; }

        public ListItem() : this("", "") { }
        public ListItem(int value, string text) : this(value.ToString(), text) { }
        public ListItem(int value) : this(value, value.ToString()) { }
        public ListItem(string value) : this(value, value) { }
        public ListItem(string value, string text)
        {
            this.Value = value;
            this.Text = text;
        }
    }
}
