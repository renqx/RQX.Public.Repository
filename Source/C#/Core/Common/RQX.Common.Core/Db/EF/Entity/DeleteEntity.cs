﻿using RQX.Common.Core.Db.EF.Entity.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.EF.Entity
{
    /// <summary>
    /// 支持软删除的基础实体
    /// </summary>
    public class DeleteEntity : BaseEntity, IDelete
    {
        /// <summary>
        /// 能否被删除
        /// </summary>
        [Column("can_dele")]
        public bool CanDele { get; set; } = true;
        /// <summary>
        /// 是否已删除
        /// </summary>
        [Column("is_remove")]
        public bool IsRemove { get; set; } = false;
    }
}
