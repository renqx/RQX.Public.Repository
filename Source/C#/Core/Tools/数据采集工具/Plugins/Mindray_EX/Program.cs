﻿using RQX.Common.Core.Db.SqlServer;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using RQX.Common.Core.Hardware.Net;
using RQX.Common.Core.Logger;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mindray_EX
{
    class Program
    {
        static void Main(string[] args)
        {
            EX55 ex55 = new EX55(new StructureModel<MyCom>()
            {
                Conn = new MyCom(57600, "COM3", 8, StopBits.One, Parity.None),
                LocalIP = "127.0.0.1",
                MaxRecvTime = 1 * 30 * 1000
            });
            ex55.DealDataHandler = (dic) => dic.ForEach(item => Console.WriteLine($"{item.Key}\t{item.Value}"));
            ex55.Start();
            Console.ReadKey();
        }
    }
}
