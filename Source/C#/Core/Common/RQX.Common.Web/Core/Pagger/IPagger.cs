﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 分页汇总接口
    /// </summary>
    public interface IPagger : IBasePagger, IBaseFilter, IBaseSort
    {
    }
}
