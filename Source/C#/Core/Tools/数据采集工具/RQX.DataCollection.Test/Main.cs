﻿using RQX.DataCollection.Test.Com;
using RQX.DataCollection.Test.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQX.DataCollection.Test
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 进入串口测试界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_com_Click(object sender, EventArgs e)
        {
            ComForm comForm = new ComForm();
            this.Hide();
            if (comForm.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
            }
        }
        /// <summary>
        /// 进入TCP客户端测试界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_tcp_client_Click(object sender, EventArgs e)
        {
            TcpClientForm tcpClientForm = new TcpClientForm();
            this.Hide();
            if (tcpClientForm.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
            }
        }
        /// <summary>
        /// 进入TCP服务端端测试界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_tcp_server_Click(object sender, EventArgs e)
        {
            TcpServerForm tcpServerForm = new TcpServerForm();
            this.Hide();
            if (tcpServerForm.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
            }
        }
        /// <summary>
        /// 进入UDP测试界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_udp_Click(object sender, EventArgs e)
        {
            UdpForm udpForm = new UdpForm();
            this.Hide();
            if (udpForm.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
            }
        }
    }
}
