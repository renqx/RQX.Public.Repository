﻿using RQX.Common.Core.Db.Sqlite;
using RQX.Common.Core.Db.SqlServer;
using RQX.Common.Core.Extension;
using RQX.DataCollection.Main.Db.EF.LocalModel;
using RQX.DataCollection.Main.Db.Local;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQX.DataCollection.Main.Simulate
{
    public partial class DbConfig : Form
    {
        #region Init
        public DbConfig()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 加载界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DbConfig_Load(object sender, EventArgs e)
        {
            txt_account.Text = "sa";
            txt_ip.Text = "127.0.0.1";
            ddl_db_type.SelectedIndex = 0;
        }
        #endregion
        /// <summary>
        /// 连接测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_connect_test_Click(object sender, EventArgs e)
        {
            var item = GetConnectionStr();
            if (item.IsNull() || item.ConnectStr.IsNullOrEmpty()) return;

            var typeName = ddl_db_type.SelectedItem.ToString();
            switch (typeName)
            {
                case "SqlServer":
                    BaseSqlHelper helper = new BaseSqlHelper(item.ConnectStr);
                    try
                    {
                        helper.ExcuteSqlNoQuery("select 1");
                        MessageBox.Show("测试成功！");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"测试失败！{ex.Message}");
                    }
                    break;
                case "MySql":
                case "Oracle":
                    // TODO 数据库连接
                    //MessageBox.Show("当前未支持！");
                    break;
                default:
                    //MessageBox.Show("数据库类型异常！");
                    break;
            }
        }
        /// <summary>
        /// 数据库类型，与端口号联动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddl_db_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            var typeName = ddl_db_type.SelectedItem.ToString();
            //if (typeName.IsNullOrEmpty()) return;
            switch (typeName)
            {
                case "SqlServer":
                    txt_port.Text = "1433";
                    break;
                case "MySql":
                    txt_port.Text = "3306";
                    break;
                case "Oracle":
                    txt_port.Text = "1521";
                    break;
                default:
                    MessageBox.Show("数据库类型异常！");
                    break;
            }
            var item = DbConfigTable.GetItem(typeName);
            if (item.IsNotNull())
            {
                txt_account.Text = item.Account;
                txt_db_name.Text = item.DbName;
                txt_instance.Text = item.Instance;
                txt_ip.Text = item.IP;
                txt_password.Text = item.Password;
                txt_port.Text = item.Port;
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_save_Click(object sender, EventArgs e)
        {
            var item = GetConnectionStr();
            var flag = item?.Save();
            if(flag.HasValue && flag.Value > 0)
            {
                MessageBox.Show("保存成功！");
            }
        }
        /// <summary>
        /// 获取到数据库连接字符串
        /// </summary>
        /// <returns></returns>
        private DbConfigTable GetConnectionStr()
        {
            var item = new DbConfigTable()
            {
                IP = txt_ip.Text.Trim(),
                Port = txt_port.Text.Trim(),
                Instance = txt_instance.Text.Trim(),
                Account = txt_account.Text.Trim(),
                Password = txt_password.Text.Trim(),
                DbName = txt_db_name.Text.Trim(),
            };

            if (item.IP.IsNullOrEmpty())
            {
                MessageBox.Show("IP地址不能为空!");
                return null;
            }
            if (item.Password.IsNullOrEmpty())
            {
                MessageBox.Show("密码不能为空！");
                return null;
            }
            if (item.DbName.IsNullOrEmpty())
            {
                MessageBox.Show("数据库名不能为空！");
                return null;
            }

            var typeName = ddl_db_type.SelectedItem.ToString();
            switch (typeName)
            {
                case "SqlServer":
                    var instanceStr = item.Instance.IsNullOrEmpty() ? "" : $"\\{item.Instance}";
                    string str = $"Server={item.IP}{instanceStr},{item.Port};User Id={item.Account};Password={item.Password};Database={item.DbName};";
                    item.ConnectStr = str;
                    return item;
                case "MySql":
                case "Oracle":
                    // TODO 数据库连接
                    MessageBox.Show("当前未支持！");
                    return null;
                default:
                    MessageBox.Show("数据库类型异常！");
                    return null;
            }
        }
    }
}
