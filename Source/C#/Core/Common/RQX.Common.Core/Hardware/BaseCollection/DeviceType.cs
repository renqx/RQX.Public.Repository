﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    /// <summary>
    /// 设备类型
    /// </summary>
    public enum DeviceType
    {
        监护仪,
        麻醉机,
        呼吸机,
    }
}
