﻿namespace RQX.DataCollection.Main
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu_main = new System.Windows.Forms.MenuStrip();
            this.menu_collection = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_collection_device = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_collection_local = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_simulate = new System.Windows.Forms.ToolStripMenuItem();
            this.tab_control = new System.Windows.Forms.TabControl();
            this.tab_device = new System.Windows.Forms.TabPage();
            this.dgv_grid = new System.Windows.Forms.DataGridView();
            this.tab_realtime = new System.Windows.Forms.TabPage();
            this.tab_debug = new System.Windows.Forms.TabPage();
            this.tab_error = new System.Windows.Forms.TabPage();
            this.menu_main.SuspendLayout();
            this.tab_control.SuspendLayout();
            this.tab_device.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // menu_main
            // 
            this.menu_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_collection,
            this.menu_simulate});
            this.menu_main.Location = new System.Drawing.Point(0, 0);
            this.menu_main.Name = "menu_main";
            this.menu_main.Size = new System.Drawing.Size(838, 25);
            this.menu_main.TabIndex = 0;
            this.menu_main.Text = "菜单";
            // 
            // menu_collection
            // 
            this.menu_collection.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_collection_device,
            this.menu_collection_local});
            this.menu_collection.Name = "menu_collection";
            this.menu_collection.Size = new System.Drawing.Size(68, 21);
            this.menu_collection.Text = "采集管理";
            // 
            // menu_collection_device
            // 
            this.menu_collection_device.Name = "menu_collection_device";
            this.menu_collection_device.Size = new System.Drawing.Size(148, 22);
            this.menu_collection_device.Text = "设备终端管理";
            this.menu_collection_device.Click += new System.EventHandler(this.menu_collection_device_Click);
            // 
            // menu_collection_local
            // 
            this.menu_collection_local.Name = "menu_collection_local";
            this.menu_collection_local.Size = new System.Drawing.Size(148, 22);
            this.menu_collection_local.Text = "本机信息配置";
            // 
            // menu_simulate
            // 
            this.menu_simulate.Name = "menu_simulate";
            this.menu_simulate.Size = new System.Drawing.Size(92, 21);
            this.menu_simulate.Text = "模拟采集工具";
            this.menu_simulate.Click += new System.EventHandler(this.menu_simulate_Click);
            // 
            // tab_control
            // 
            this.tab_control.Controls.Add(this.tab_device);
            this.tab_control.Controls.Add(this.tab_realtime);
            this.tab_control.Controls.Add(this.tab_debug);
            this.tab_control.Controls.Add(this.tab_error);
            this.tab_control.Location = new System.Drawing.Point(13, 29);
            this.tab_control.Name = "tab_control";
            this.tab_control.SelectedIndex = 0;
            this.tab_control.Size = new System.Drawing.Size(813, 445);
            this.tab_control.TabIndex = 1;
            // 
            // tab_device
            // 
            this.tab_device.Controls.Add(this.dgv_grid);
            this.tab_device.Location = new System.Drawing.Point(4, 26);
            this.tab_device.Name = "tab_device";
            this.tab_device.Padding = new System.Windows.Forms.Padding(3);
            this.tab_device.Size = new System.Drawing.Size(805, 415);
            this.tab_device.TabIndex = 0;
            this.tab_device.Text = "终端设备";
            this.tab_device.UseVisualStyleBackColor = true;
            // 
            // dgv_grid
            // 
            this.dgv_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_grid.Location = new System.Drawing.Point(4, 4);
            this.dgv_grid.Name = "dgv_grid";
            this.dgv_grid.Size = new System.Drawing.Size(798, 408);
            this.dgv_grid.TabIndex = 0;
            this.dgv_grid.Text = "dataGridView1";
            // 
            // tab_realtime
            // 
            this.tab_realtime.Location = new System.Drawing.Point(4, 26);
            this.tab_realtime.Name = "tab_realtime";
            this.tab_realtime.Padding = new System.Windows.Forms.Padding(3);
            this.tab_realtime.Size = new System.Drawing.Size(805, 415);
            this.tab_realtime.TabIndex = 1;
            this.tab_realtime.Text = "实时信息";
            this.tab_realtime.UseVisualStyleBackColor = true;
            // 
            // tab_debug
            // 
            this.tab_debug.Location = new System.Drawing.Point(4, 26);
            this.tab_debug.Name = "tab_debug";
            this.tab_debug.Size = new System.Drawing.Size(805, 415);
            this.tab_debug.TabIndex = 2;
            this.tab_debug.Text = "调试日志";
            // 
            // tab_error
            // 
            this.tab_error.Location = new System.Drawing.Point(4, 26);
            this.tab_error.Name = "tab_error";
            this.tab_error.Size = new System.Drawing.Size(805, 415);
            this.tab_error.TabIndex = 3;
            this.tab_error.Text = "错误日志";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 486);
            this.Controls.Add(this.tab_control);
            this.Controls.Add(this.menu_main);
            this.Name = "Main";
            this.Text = "数据采集工具";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menu_main.ResumeLayout(false);
            this.menu_main.PerformLayout();
            this.tab_control.ResumeLayout(false);
            this.tab_device.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu_main;
        private System.Windows.Forms.ToolStripMenuItem menu_collection;
        private System.Windows.Forms.ToolStripMenuItem menu_simulate;
        private System.Windows.Forms.ToolStripMenuItem menu_collection_device;
        private System.Windows.Forms.ToolStripMenuItem menu_collection_local;
        private System.Windows.Forms.TabControl tab_control;
        private System.Windows.Forms.TabPage tab_device;
        private System.Windows.Forms.TabPage tab_realtime;
        private System.Windows.Forms.TabPage tab_debug;
        private System.Windows.Forms.TabPage tab_error;
        private System.Windows.Forms.DataGridView dgv_grid;
    }
}