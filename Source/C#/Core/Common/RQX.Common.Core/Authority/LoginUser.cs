﻿using Newtonsoft.Json;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Filter;
using RQX.Common.Core.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Authority
{
    public class LoginUser
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserCode { get; set; }
        /// <summary>
        /// 是否为系统管理员
        /// </summary>
        public bool IsAdmin { get; set; }

        public DateTime LoginTime { get; set; }

        public LoginUser() { }

        public static bool CheckToken(string token)
        {
            try
            {
                var userStr = CypherUtils.DESDecrypt(token);
                var user = JsonConvert.DeserializeObject<LoginUser>(userStr);
                FakeSession.LoginUserStr.Value = userStr;
                if (user.LoginTime.AddHours(15) < DateTime.Now)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new MyException(MyExceptionType.登录已过期, ex.ToString());
            }
        }

        public string CreateToken()
        {
            return CypherUtils.DESEncrypt(JsonConvert.SerializeObject(this));
        }

        #region 静态方法
        public static LoginUser GetCurrentUser()
        {
            var userStr = FakeSession.LoginUserStr.Value;
            if (userStr.IsNullOrEmpty())
            {
                return new LoginUser() { UserName = "未登录用户", UserId = -1 };
            }
            else
            {
                return JsonConvert.DeserializeObject<LoginUser>(userStr);
            }
        }
        /// <summary>
        /// 获取密文密码
        /// </summary>
        /// <param name="originalPwd"></param>
        /// <returns></returns>
        public static string GetSafePassword(string originalPwd)
        {
            return originalPwd;
        }
        #endregion
    }
}
