﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Net;

namespace Philips_MX550
{
    public class MX550 : AbstractUdpCollection
    {
        #region 初始属性
        public override DeviceType DeviceType => DeviceType.监护仪;
        public override string IP => "";

        public override int Port => 24105;

        public override int Port_Local => 24105;
        #endregion

        private CreateEvent ce;
        private bool IsCircleSend = false;

        public MX550() { }
        public MX550(StructureModel<UdpSocket> structureModel) : base(structureModel)
        {
        }

        protected override void Connect(StepType stepNum, string msg)
        {
            switch (stepNum)
            {
                case StepType.Init:
                    Start_Init_1();
                    break;
                case StepType.Setp1://CreateEvent
                    Start_Recv_Send_Create_Event(msg.HexStringToByte());
                    Thread.Sleep(1000);
                    CircleSend();
                    break;
            }
        }

        protected override string CutToOneMsg(StringBuilder sb) => sb.ToString();

        protected override StepType CheckRecvMsgType(string lineStr)
        {
            var buffer = lineStr.HexStringToByte();
            var tmp = (CreateEvent)ByteToStruct(buffer, typeof(CreateEvent));
            if (ChangeBigSmall(tmp.sppdu.session_id).Equals(0xE100))
            {
                switch (ChangeBigSmall(tmp.roapdus.ro_type))
                {
                    case 0x0001://收到的是CreateEvent
                        WriteDebugLogMethod("这是CreateEvent");
                        return StepType.Setp1;
                    case 0x0002://收到的是采集数据PollDataResult
                        WriteDebugLogMethod("收到的是采集数据 PollDataResult");
                        return StepType.DataSource;
                    case 0x0003://收到的是错误信息
                        WriteDebugLogMethod("收到的是错误信息");
                        return StepType.None;
                    case 0x0005://收到的是采集部分数据PollDataResultLinked
                        WriteDebugLogMethod("收到的是部分数据 PollDataResultLinked");
                        return StepType.DataSource;
                }
            }
            return StepType.None;
        }

        protected override Dictionary<string, string> AnalysisData(string msg)
        {
            var buffer = msg.HexStringToByte();
            Dictionary<string, string> dic_result = new Dictionary<string, string>();
            var pdresponselink = (PollDataResponseLinked)ByteToStruct(buffer, typeof(PollDataResponseLinked));
            int offset;
            unsafe
            {
                offset = sizeof(PollDataResponseLinked);
            }
            var listCount = ChangeBigSmall(pdresponselink.reply.poll_info_list.count);

            for (var i = 0; i < listCount; i++)
            {
                unsafe
                {
                    var obj2 = ByteToStruct(buffer, typeof(SingleContextPoll), offset);
                    if (obj2 == null) continue;
                    var singleConext = (SingleContextPoll)obj2;
                    offset += sizeof(SingleContextPoll);
                    int count = ChangeBigSmall(singleConext.info.count);
                    for (var j = 0; j < count; j++)
                    {
                        var obj1 = ByteToStruct(buffer, typeof(ObservationPoll), offset);
                        if (obj1 == null) continue;
                        var observationPoll = (ObservationPoll)obj1;
                        offset += sizeof(ObservationPoll);
                        string lableStr = "";
                        string value = "";

                        var attrCount = ChangeBigSmall(observationPoll.attributes.count);
                        for (var m = 0; m < attrCount; m++)
                        {
                            var obj = ByteToStruct(buffer, typeof(AVATypeValue), offset);
                            if (obj == null) continue;
                            var attributes = (AVATypeValue)obj;
                            offset += sizeof(AVATypeValue);
                            var id = ChangeBigSmall(attributes.attribute_id);
                            var length = ChangeBigSmall(attributes.length);

                            switch (id)
                            {
                                case 0x0950: //NOM_ATTR_NU_VAL_OBS
                                    var item = DealNuObsValue(ref buffer, offset);
                                    lableStr = item.Key;
                                    value = item.Value;
                                    offset += sizeof(NuObsValue);
                                    break;
                                case 0x0927://Lable String
                                    lableStr = DealLableStr(ref buffer, offset);
                                    offset += length;
                                    break;
                                case 0x094B://NOM_ATTR_NU_CMPD_VAL_OBS
                                    lableStr = "";
                                    Start_Recv_NuObsCMPDValue(dic_result, ref buffer, ref offset);
                                    break;
                                default:
                                    offset += length;
                                    break;
                            }
                        }
                        if (!string.IsNullOrEmpty(lableStr) && !dic_result.ContainsKey(lableStr) && value.IsNotNullOrEmpty())
                        {
                            if (lableStr.Equals("NOM_PULS_OXIM_SAT_O2")) value = (double.Parse(value) / 100.0).ToString();
                            dic_result.Add(lableStr, value);
                        }
                    }
                }
            }
            var items = dic_result.Where(k => k.Value.Equals("-999999"));
            if (items.Count() > 0)
            {
                var deleteList = items.Select(k => new KeyValuePair<string, string>(k.Key, k.Value));
                WriteAnalysisLogMethod(items);
                deleteList.ToList().ForEach(item => dic_result.Remove(item.Key));
            }
            return dic_result;
        }
        protected override void CircleSend()
        {
            if (IsCircleSend == false)
            {
                IsCircleSend = true;
                Task.Run(() =>
                {
                    while (true)
                    {
                        Start_Get_Sleep_1500Sec();
                        Thread.Sleep(1500);
                    }
                });
            }
        }
        #region 内部方法
        public void Start_Init_1()
        {
            var userInfo = new MDSEUserInfoStd();
            userInfo.protocol_version = 0x00000080;
            userInfo.nomenclature_version = 0x00000040;
            userInfo.functional_units = 0x00000000;
            userInfo.system_type = 0x00000080;
            userInfo.startup_mode = 0x00000020;
            userInfo.option_list_count = 0x0000;
            userInfo.option_list_length = 0x0000;
            userInfo.supported_aprofiles.count = 0x0100;
            userInfo.supported_aprofiles.length = 0x2c00;
            userInfo.supported_aprofiles.value.attribute_id = 0x0100;//NOM_POLL_PROFILE_SUPPORT
            userInfo.supported_aprofiles.value.length = 0x0000;

            var support = new PollProfileSupport();
            support.poll_profile_revision = 0x00000080;
            support.min_poll_period = 0xE8030000;//1000ms
            support.max_mtu_rx = 0xE8030000;
            support.max_mtu_tx = 0xE8030000;
            support.max_bw_tx = 0xffffffff;
            support.options = 0x00000040;//0x00000040
            support.optional_packages.count = 0x0100;
            support.optional_packages.length = 0x0000;
            support.optional_packages.value.attribute_id = 0x01f0;//NOM_ATTR_POLL_PROFILE_EXT   61441
            support.optional_packages.value.length = 0x0000;

            var ext = new PollProfileExt();
            ext.options = 0x00000080;//#define POLL_EXT_PERIOD_NU_1SEC 0x80000000    //0x00000080
            ext.count = 0x0000;
            ext.length = 0x0000;

            unsafe
            {
                var a = sizeof(PollProfileExt);
                var b = sizeof(PollProfileSupport);
                support.optional_packages.value.length = ChangeBigSmall((ushort)(a + 16));
                support.optional_packages.length = ChangeBigSmall((ushort)(a + 4));
                userInfo.supported_aprofiles.value.length = ChangeBigSmall((ushort)(a + b));
            }

            var byte_userInfo = StructToBytes(userInfo);
            var byte_support = StructToBytes(support);
            var byte_ext = StructToBytes(ext);
            var byte_MDSEUserInfoStd = ConcateByte(byte_userInfo, ConcateByte(byte_support, byte_ext));
            byte[] byte_ASNLength;
            var userDataLength = byte_MDSEUserInfoStd.Length;
            byte_ASNLength = new byte[1];
            byte_ASNLength[0] = (byte)userDataLength;

            byte[] byte_AssocReqSessionHeader = new byte[] { 0x0D, 0x00 };
            byte[] byte_AssocReqSessionData = new byte[] { 0x05, 0x08, 0x13, 0x01, 0x00, 0x16, 0x01, 0x02, 0x80, 0x00, 0x14, 0x02, 0x00, 0x02 };
            byte[] byte_AssocReqPresentationHeader = new byte[] { 0xC1, 0x00, 0x31, 0x80, 0xA0, 0x80, 0x80, 0x01, 0x01, 0x00, 0x00, 0xA2, 0x80, 0xA0, 0x03, 0x00, 0x00, 0x01, 0xA4, 0x80, 0x30, 0x80, 0x02, 0x01, 0x01, 0x06, 0x04, 0x52, 0x01, 0x00, 0x01, 0x30, 0x80, 0x06, 0x02, 0x51, 0x01, 0x00, 0x00, 0x00, 0x00, 0x30, 0x80, 0x02, 0x01, 0x02, 0x06, 0x0C, 0x2A, 0x86, 0x48, 0xCE, 0x14, 0x02, 0x01, 0x00, 0x00, 0x00, 0x01, 0x01, 0x30, 0x80, 0x06, 0x0C, 0x2A, 0x86, 0x48, 0xCE, 0x14, 0x02, 0x01, 0x00, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x61, 0x80, 0x30, 0x80, 0x02, 0x01, 0x01, 0xA0, 0x80, 0x60, 0x80, 0xA1, 0x80, 0x06, 0x0C, 0x2A, 0x86, 0x48, 0xCE, 0x14, 0x02, 0x01, 0x00, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00, 0xBE, 0x80, 0x28, 0x80, 0x06, 0x0C, 0x2A, 0x86, 0x48, 0xCE, 0x14, 0x02, 0x01, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x01, 0x02, 0x81 };
            byte[] byte_userData = ConcateByte(byte_ASNLength, byte_MDSEUserInfoStd);
            byte[] byte_AssorReqPresentationTrailer = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            byte_AssocReqPresentationHeader[1] = (byte)(byte_AssorReqPresentationTrailer.Length + byte_userData.Length + byte_AssocReqPresentationHeader.Length - 2);
            byte_AssocReqSessionHeader[1] = (byte)(byte_AssocReqSessionData.Length + byte_AssocReqPresentationHeader.Length + byte_userData.Length + byte_AssorReqPresentationTrailer.Length);

            var sendBuffer = ConcateByte(byte_AssocReqSessionHeader, ConcateByte(byte_AssocReqSessionData, ConcateByte(byte_AssocReqPresentationHeader, ConcateByte(byte_userData, byte_AssorReqPresentationTrailer))));
            Send(sendBuffer);
        }
        public void Start_Get_Sleep_1500Sec()
        {
            var pdrequest = new PollDataRequest()
            {
                sppdu = new SPpdu()
                {
                    session_id = 0x00E1,
                    p_context_id = ce.sppdu.p_context_id
                },
                roapdus = new ROapdus()
                {
                    ro_type = 0x0100,
                    length = 0x1C00,
                },
                roivapdu = new ROIVapdu()
                {
                    invoke_id = 0x0100,
                    command_type = 0x0700,
                    length = 0x1600
                },
                argument = new ActionArgument()
                {
                    managed_object = ce.argument.managed_object,
                    scope = 0x00000000,
                    action_type = 0x160C,
                    length = 0x0800
                },
                dataReq = new PollMdibDataReq()
                {
                    poll_number = 0x0100,
                    polled_obj_type = new TYPE()
                    {
                        partition = 0x0100,
                        code = 0x0600
                    },
                    polled_attr_grp = 0x0000// 0x4BB8// //0x5009
                }
            };
            var sendBuffer = StructToBytes(pdrequest);
            Send(sendBuffer);
        }
        public void Start_Recv_Send_Create_Event(byte[] buffer)
        {
            ce = (CreateEvent)ByteToStruct(buffer, typeof(CreateEvent));

            var result = new CreateEventResult()
            {
                sppdu = new SPpdu()
                {
                    session_id = ce.sppdu.session_id,
                    p_context_id = ce.sppdu.p_context_id
                },
                roapdus = new ROapdus()
                {
                    ro_type = 0x0200,
                    length = 0x1400
                },
                rorsapdu = new RORSapdu()
                {
                    invoke_id = ce.roivapdu.invoke_id,
                    command_type = 0x0100,
                    length = 0x0E00
                },
                result = new EventReportResult()
                {
                    managed_object = ce.argument.managed_object,
                    current_time = ce.argument.event_time,
                    event_type = 0x060D,
                    length = 0x0000
                }
            };
            Thread.Sleep(1000);
            var sendBuffer = StructToBytes(result);
            Send(sendBuffer);
        }
        private void Start_Recv_NuObsCMPDValue(Dictionary<string, string> dic_result, ref byte[] buffer, ref int offset)
        {
            var obj = ByteToStruct(buffer, typeof(NuObsValueCmp), offset);
            if (obj != null)
            {
                var nuObsCmp = (NuObsValueCmp)obj;
                offset += 4;
                var count = ChangeBigSmall(nuObsCmp.count);
                for (var i = 0; i < count; i++)
                {
                    var kv = DealNuObsValue(ref buffer, offset);
                    offset += 10;
                    if (!dic_result.ContainsKey(kv.Key))
                    {
                        dic_result.Add(kv.Key, kv.Value);
                    }
                }
            }
        }
        private string DealLableStr(ref byte[] buffer, int offset)
        {
            var length = GetShortOrWord(ref buffer, offset) / 2;
            //var lableBuffer = SubBytes(ref buffer, offset + 2, length);
            StringBuilder sb = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                byte[] by = new byte[2];
                by[0] = buffer[offset + 2 + (i * 2 + 1)];
                by[1] = buffer[offset + 2 + (i * 2)];
                string str = Encoding.Unicode.GetString(by);
                if (string.IsNullOrEmpty(str.Trim())) break;
                sb.Append(str);
            }
            return sb.ToString();
        }
        private KeyValuePair<string, string> DealNuObsValue(ref byte[] buffer, int offset)
        {
            double value = -999999;
            string key = "";
            var obj = ByteToStruct(buffer, typeof(NuObsValue), offset);
            if (obj != null)
            {
                var nuObs = (NuObsValue)obj;
                var id = ChangeBigSmall(nuObs.physio_id);
                if (dic_Attr.ContainsKey(id))
                {

                }
                if (dic.ContainsKey(id))
                {
                    key = dic[id];
                }
                else
                {
                    Console.WriteLine($"没有ID：{id}");
                }
                value = GetFloat(ChangeBigSmall(nuObs.value));
            }
            return new KeyValuePair<string, string>(key, value.ToString());
        }
        private double GetFloat(uint inValue)
        {
            byte exponent = (byte)((inValue & 0xff000000) >> 24);
            int mantissa = (int)(inValue & 0x00ffffff);
            if ((exponent & 0x80) >> 7 == 1) exponent = (byte)(~exponent + 1);
            if ((mantissa & 0x00800000) >> 23 == 1) mantissa = (int)(~mantissa + 1);

            if (mantissa.Equals(0x7fffff)) return -999999;
            double value = (mantissa) * (Math.Pow(10, exponent));
            return value;
        }
        #endregion

        #region CommonMethod

        public static ulong GetBufferValue(ref byte[] buffer, int beginIndex, int size)
        {
            ulong result = 0;
            for (var i = beginIndex; i < size + beginIndex; i++)
            {
                result |= ((ulong)buffer[i] << ((i - beginIndex) * 8));
            }
            return result;
        }
        public static byte[] SubBytes(ref byte[] buffer, int offset, int length)
        {
            byte[] result = new byte[length];
            for (var i = 0; i < length; i++)
            {
                result[i] = buffer[offset + i];
            }
            return result;
        }

        public static uint ChangeBigSmall(uint value) => (uint)ChangeBigSmall(value, 4);
        public static ushort ChangeBigSmall(ushort value) => (ushort)ChangeBigSmall(value, 2);
        public static short ChangeBigSmall(short value) => (short)ChangeBigSmall((ulong)value, 2);
        public static byte ChangeBigSmall(byte value) => (byte)ChangeBigSmall(value, 1);

        public static ulong ChangeBigSmall(ulong value, int bytes)
        {
            ulong result = 0;
            switch (bytes)
            {
                case 4: result = (value & 0xFF) << 24 | (value & 0xFF00) << 8 | (value & 0xFF0000) >> 8 | (value & 0xFF000000) >> 24; break;
                case 3: result = (value & 0xFF) << 16 | (value & 0xFF00) | (value & 0xFF0000) >> 16; break;
                case 2: result = (value & 0xFF) << 8 | (value & 0xFF00) >> 8; break;
                default:
                    result = value;
                    break;
            }
            return result;
        }

        //将Byte转换为结构体类型
        public static object ByteToStruct(byte[] bytes, Type type, int beginIndex = 0)
        {
            unsafe
            {
                int size = Marshal.SizeOf(type);
                //分配结构体内存空间
                object obj = default(Type);
                //将byte数组拷贝到分配好的内存空间
                if (beginIndex < bytes.Length)
                {
                    IntPtr structPtr = Marshal.AllocHGlobal(size);
                    Marshal.Copy(bytes, beginIndex, structPtr, Math.Min(size, Math.Max(bytes.Length - beginIndex, 0)));
                    //将内存空间转换为目标结构体
                    obj = Marshal.PtrToStructure(structPtr, type);
                    //释放内存空间
                    Marshal.FreeHGlobal(structPtr);
                }
                return obj;
            }
        }
        //// <summary>
        /// 结构体转byte数组
        /// </summary>
        /// <param name="structObj">要转换的结构体</param>
        /// <returns>转换后的byte数组</returns>
        public static byte[] StructToBytes(object structObj)
        {
            //得到结构体的大小
            int size = Marshal.SizeOf(structObj);
            //创建byte数组
            byte[] bytes = new byte[size];
            //分配结构体大小的内存空间
            IntPtr structPtr = Marshal.AllocHGlobal(size);
            //将结构体拷到分配好的内存空间
            Marshal.StructureToPtr(structObj, structPtr, false);
            //从内存空间拷到byte数组
            Marshal.Copy(structPtr, bytes, 0, size);
            //释放内存空间
            Marshal.FreeHGlobal(structPtr);
            //返回byte数组
            return bytes;
        }
        /// <summary>
        /// 合并数组
        /// </summary>
        /// <param name="firstBuffer"></param>
        /// <param name="lastBuffer"></param>
        /// <returns></returns>
        public static byte[] ConcateByte(byte[] firstBuffer, byte[] lastBuffer)
        {
            byte[] newBuffer = new byte[firstBuffer.Length + lastBuffer.Length];
            firstBuffer.CopyTo(newBuffer, 0);
            lastBuffer.CopyTo(newBuffer, firstBuffer.Length);
            return newBuffer;
        }

        private ushort GetShortOrWord(ref byte[] buffer, int beginNum)
        {
            return (ushort)GetValue(ref buffer, 2, beginNum);
        }

        private ulong GetValue(ref byte[] buffer, int byteLength, int beginNum)
        {
            ulong value = 0;
            for (var i = 0; i < byteLength; i++)
            {
                value |= (ulong)buffer[beginNum + i] << ((byteLength - i - 1) * 8);
            }
            return value;
        }
        #endregion
        #region 结构体
        //AssociationRequestMessage::=          =>
        //    <AssocReqSessionHeader>           0D <LI>
        //    <AssocReqSessionData>             05 08 13 01 00 16 01 02 80 00 14 02 00 02
        //    <AssocReqPresentationHeader>      C1 <LI> 31 80 A0 80 80 01 01 00 00 A2 80 A0 03 00 00 01 A4 80 30 80 02 01 01 06 04 52 01 00 01 30 80 06 02 51 01 00 00 00 00 30 80 02 01 02 06 0C 2A 86 48 CE 14 02 01 00 00 00 01 01 30 80 06 0C 2A 86 48 CE 14 02 01 00 00 00 02 01 00 00 00 00 00 00 61 80 30 80 02 01 01 A0 80 60 80 A1 80 06 0C 2A 86 48 CE 14 02 01 00 00 00 03 01 00 00 BE 80 28 80 06 0C 2A 86 48 CE 14 02 01 00 00 00 01 01 02 01 02 81 
        //    <AssocReqUserData>                P65
        //    <AssorReqPresentationTrailer>     00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
        //AssociationResponseMessage::=         <=
        //    <AssocRespSessionHeader>          0E <LI>
        //    <AssocRespSessionData>            05 08 13 01 00 16 01 02 80 00 14 02 00 02
        //    <AssocRespPresentationHeader>     C1 <LI> 31 80 A0 80 80 01 01 00 00 A2 80 A0 03 00 00 01 A5 80 30 80 80 01 00 81 02 51 01 00 00 30 80 80 01 00 81 0C 2A 86 48 CE 14 02 01 00 00 00 02 01 00 00 00 00 61 80 30 80 02 01 01 A0 80 61 80 A1 80 06 0C 2A 86 48 CE 14 02 01 00 00 00 03 01 00 00 A2 03 02 01 00 A3 05 A1 03 02 01 00 BE 80 28 80 02 01 02 81
        //    <AssocRespUserData>               P65
        //    <AssorRespPresentationTrailer>    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
        //MDSCreateEventReport ::=                                      <=
        //     <SPpdu>                                                  E1 00 00 02
        //     <ROapdus(ro_type := ROIV_APDU)>                          00 01               XX XX
        //     <ROIVapdu(command_type := CMD_CONFIRMED_EVENT_REPORT)>   00 01 00 01 XX XX
        //     <EventReportArgument
        //        (managed_object := { NOM_MOC_VMS_MDS, 0, 0},
        //         event_type := NOM_NOTI_MDS_CREAT)>
        //     <MDSCreateInfo>
        //MDSCreateEventResult::=               =>
        //     <SPpdu>
        //     <ROapdus(ro_type := RORS_APDU)>
        //     <RORSapdu
        //        (invoke_id := mirrored from event report,
        //         command_type := CMD_CONFIRMED_EVENT_REPORT)>
        //     <EventReportResult
        //        (managed_object := mirrored from event report,
        //         event_type := NOM_NOTI_MDS_CREAT)
        //         length := 0 >
        //MDSPollAction::=                      =>
        //     <SPpdu>
        //     <ROapdus(ro_type := ROIV_APDU)>
        //     <ROIVapdu(command_type := CMD_CONFIRMED_ACTION)>
        //     <ActionArgument
        //        (managed_object := { NOM_MOC_VMS_MDS, 0, 0},
        //         action_type := NOM_ACT_POLL_MDIB_DATA)>
        //     <PollMdibDataReq>
        //MDSPollActionResult::=                <=
        //     <SPpdu>
        //     <ROapdus(ro_type := RORS_APDU)>
        //     <RORSapdu(invoke_id := "mirrored from request message"
        //        command_type := CMD_CONFIRMED_ACTION)>
        //     <ActionResult
        //        (managed_object := { NOM_MOC_VMS_MDS, 0, 0},
        //         action_type := NOM_ACT_POLL_MDIB_DATA)>
        //     <PollMdibDataReply>




        //AssocReqSessionHeader::=
        //    <SessionHead(type := CN_SPDU_SI)>
        //AssocReqUserData::=
        //    <ASNLength> 127 LI
        //    <MDSEUserInfoStd>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SessionHeader
        {
            /// <summary>
            /// CN_SPDU_SI 0x0D
            /// AC_SPDU_SI 0x0E
            /// RF_SPDU_SI 0x0C
            /// FN_SPDU_SI 0x09
            /// DN_SPDU_SI 0x0A
            /// AB_SPDU_SI 0x19
            /// </summary>
            public byte type;
            /// <summary>
            /// LI   if <=254 1byte  if >254 3byte first is 0xff
            /// </summary>
            public byte length;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AssocReqSessionData
        {
            /// <summary>
            /// 05 08 13 01 00 16 01 02 80 00 14 02 00 02
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
            public byte[] buffer;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct MDSEUserInfoStd
        {
            public uint protocol_version;// ProtocolVersion #define MDDL_VERSION1 0x80000000
            public uint nomenclature_version;// NomenclatureVersion #define NOMEN_VERSION 0x40000000;
            public uint functional_units;//FunctionalUnits
            public uint system_type;//SystemType  #define SYST_CLIENT 0x80000000  #define SYST_SERVER 0x00800000
            public uint startup_mode;//StartupMode  #define HOT_START 0x80000000  #define WARM_START 0x40000000  #define COLD_START 0x20000000
            public ushort option_list_count;
            public ushort option_list_length;
            public AttributeList1 supported_aprofiles;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AttributeList1
        {
            public ushort count;
            public ushort length;
            public AVAType1 value;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AVAType1
        {
            public ushort attribute_id;//OIDType
            public ushort length;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AttributeList
        {
            public ushort count;
            public ushort length;
            //public AVAType[] value;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AVAType
        {
            public ushort attribute_id;//OIDType
            public ushort length;
            public ushort attribute_val;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AVATypeValue
        {
            public ushort attribute_id;//OIDType
            public ushort length;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollProfileSupport
        {
            public uint poll_profile_revision;//PollProfileRevision  #define POLL_PROFILE_REV_0 0x80000000
            public uint min_poll_period;
            public uint max_mtu_rx;
            public uint max_mtu_tx;
            public uint max_bw_tx;
            public uint options;//PollProfileOptions  #define P_OPT_DYN_CREATE_OBJECTS 0x40000000  #define P_OPT_DYN_DELETE_OBJECTS 0x20000000
            public AttributeList1 optional_packages;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollProfileExt
        {
            public uint options;
            public ushort count;
            public ushort length;
        };


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CreateEventResult
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public RORSapdu rorsapdu;
            public EventReportResult result;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RORSapdu
        {
            public ushort invoke_id; /* mirrored back from op. invoke */
            /// <summary>
            /// CMD_EVENT_REPORT 0 
            /// CMD_CONFIRMED_EVENT_REPORT 1
            /// CMD_GET 3
            /// CMD_SET 4
            /// CMD_CONFIRMED_SET 5
            /// CMD_CONFIRMED_ACTION 7
            /// </summary>
            public ushort command_type; /* identifies type of command */
            public ushort length; /* no of bytes in rest of message */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct EventReportResult
        {
            public ManagedObjectId managed_object; /* mirrored from EvRep */
            /// <summary>
            /// RelativeTime
            /// </summary>
            public uint current_time; /* result time stamp */
            /// <summary>
            /// OIDType
            /// </summary>
            public ushort event_type; /* identification of event */
            public ushort length; /* size of appended data */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CreateEvent
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public ROIVapdu roivapdu;
            public EventReportArgument argument;
            public MDSCreateInfo createInfo;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SPpdu
        {
            /// <summary>
            /// 0xE100
            /// </summary>
            public ushort session_id;
            public ushort p_context_id;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ROapdus
        {
            /// <summary>
            /// #define ROIV_APDU 1
            /// #define RORS_APDU 2
            /// #define ROER_APDU 3
            /// #define ROLRS_APDU 5
            /// </summary>
            public ushort ro_type; /* ID for operation */
            public ushort length; /* bytes to follow */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ROIVapdu
        {
            public ushort invoke_id;
            /// <summary>
            /// CMD_EVENT_REPORT 0 
            /// CMD_CONFIRMED_EVENT_REPORT 1
            /// CMD_GET 3
            /// CMD_SET 4
            /// CMD_CONFIRMED_SET 5
            /// CMD_CONFIRMED_ACTION 7
            /// </summary>
            public ushort command_type;
            public ushort length;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct EventReportArgument
        {
            public ManagedObjectId managed_object; /* ident. of sender */
            /// <summary>
            /// RelativeTime
            /// </summary>
            public uint event_time; /* event time stamp */
            /// <summary>
            /// OIDType
            /// </summary>
            public ushort event_type; /* identification of event */
            public ushort length; /* size of appended data */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MDSCreateInfo
        {
            public ManagedObjectId managed_object;
            public AttributeList attribute_list;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ManagedObjectId
        {
            /// <summary>
            /// OIDType
            /// </summary>
            public ushort m_obj_class;
            /// <summary>
            /// GlbHandle
            /// </summary>
            public GlbHandle m_obj_inst;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct GlbHandle
        {
            public ushort context_id;
            public ushort handle;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollDataRequest
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public ROIVapdu roivapdu;
            public ActionArgument argument;
            public PollMdibDataReq dataReq;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ActionArgument
        {
            public ManagedObjectId managed_object;/* addressed object */
            public uint scope; /* fixed value 0 */
            /// <summary>
            /// NOM_ACT_POLL_MDIB_DATA 3094 
            /// NOM_ACT_POLL_MDIB_DATA_EXT 61755
            /// </summary>
            public ushort action_type; /* identification of method */
            public ushort length; /* size of appended data */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollMdibDataReq
        {
            /// <summary>
            /// 用于计数
            /// </summary>
            public ushort poll_number;
            /// <summary>
            /// 0x0001 NUMERICS   NOM_MOC_VMO_METRIC_NU
            /// </summary>
            public TYPE polled_obj_type;
            /// <summary>
            /// 轮训哪些属性的数据
            /// </summary>
            public ushort polled_attr_grp;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollDataResponse
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public RORSapdu rorsapdu;
            public ActionResult result;
            public PollMdibDataReply reply;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollDataResponseLinked
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public ROLRSapdu rolrsapdu;
            public ActionResult result;
            public PollMdibDataReply reply;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ROLRSapdu
        {
            public RorlsId linked_id;
            public ushort invoke_id;
            public ushort command_type;
            public ushort length;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RorlsId
        {
            /// <summary>
            /// 1 first
            /// 2 mid
            /// 3 last
            /// </summary>
            public byte state;
            public byte count;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ActionResult
        {
            public ManagedObjectId managed_object;
            public ushort action_type; /* identification of method */
            public ushort length; /* size of appended data */
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollMdibDataReply
        {
            public ushort poll_number;
            /// <summary>
            /// RelativeTime
            /// </summary>
            public uint rel_time_stamp;
            public AbsoluteTime abs_time_stamp;
            public TYPE polled_obj_type;
            public ushort polled_attr_grp;
            public PollInfoList poll_info_list;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PollInfoList
        {
            public ushort count;
            public ushort length;
            //public SingleContextPoll[] value;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SingleContextPoll
        {
            public ushort context_id;
            public poll_info info;

        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct poll_info
        {
            public ushort count;
            public ushort length;
            //public ObservationPoll[] value;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ObservationPoll
        {
            public ushort obj_handle;
            public AttributeList attributes;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AbsoluteTime
        {
            public byte century;
            public byte year;
            public byte month;
            public byte day;
            public byte hour;
            public byte minute;
            public byte second;
            public byte sec_fractions;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TYPE
        {
            /// <summary>
            /// NOM_PART_OBJ 1
            /// NOM_PART_SCADA 2
            /// NOM_PART_EVT 3
            /// NOM_PART_DIM 4
            /// NOM_PART_PGRP 6
            /// NOM_PART_INFRASTRUCT 8
            /// </summary>
            public ushort partition;
            /// <summary>
            /// OIDType
            /// </summary>
            public ushort code;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ErrorEvent
        {
            public SPpdu sppdu;
            public ROapdus roapdus;
            public ROERapdu roerapdu;
            //public ErrorData
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ROERapdu
        {
            public ushort invoke_id;
            /// <summary>
            /// NO_SUCH_OBJECT_CLASS 0
            /// NO_SUCH_OBJECT_INSTANCE 1
            /// ACCESS_DENIED 2
            /// GET_LIST_ERROR 7
            /// SET_LIST_ERROR 8
            /// NO_SUCH_ACTION 9
            /// PROCESSING_FAILURE 10
            /// INVALID_ARGUMENT_VALUE 15
            /// INVALID_SCOPE 16
            /// INVALID_OBJECT_INSTANCE 17
            /// </summary>
            public ushort error_value;
            public ushort length;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct NuObsValue
        {
            public ushort physio_id;
            /// <summary>
            /// INVALID 0x8000
            /// QUESTIONABLE 0x4000
            /// UNAVAILABLE 0x2000
            /// CALIBRATION_ONGOING 0x1000
            /// TEST_DATA 0x0800
            /// DEMO_DATA 0x0400
            /// VALIDATED_DATA 0x0080
            /// EARLY_INDICATION 0x0040
            /// MSMT_ONGOING 0x0020
            /// MSMT_STATE_IN_ALARM 0x0002
            /// MSMT_STATE_AL_INHIBITED 0x0001
            /// </summary>
            public ushort state;
            public ushort unit_code;
            public uint value;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct NuObsValueCmp
        {
            public ushort count;
            public ushort length;
            //public NuObsValue value[1];
        };
        #endregion
        #region AttributesID

        public static Dictionary<ushort, string> dic_Attr = new Dictionary<ushort, string>()
        {
            {0x0902,"NOM_ATTR_AL_MON_P_AL_LIST"},
            {0x0904,"NOM_ATTR_AL_MON_T_AL_LIST"},
            {0x090C,"NOM_ATTR_ALTITUDE"},
            {0x090D,"NOM_ATTR_AREA_APPL"},
            {0x0911,"NOM_ATTR_COLOR"},
            {0x0916,"NOM_ATTR_DEV_AL_COND"},
            {0x0917,"NOM_ATTR_DISP_RES"},
            {0x091A,"NOM_ATTR_GRID_VIS_I16"},
            {0x091D,"NOM_ATTR_ID_ASSOC_NO"},
            {0x091E,"NOM_ATTR_ID_BED_LABEL"},
            {0x0921,"NOM_ATTR_ID_HANDLE"},
            {0x0924,"NOM_ATTR_ID_LABEL"},
            {0x0927,"NOM_ATTR_ID_LABEL_STRING"},
            {0x0928,"NOM_ATTR_ID_MODEL"},
            {0x092D,"NOM_ATTR_ID_PROD_SPECN"},
            {0x092F,"NOM_ATTR_ID_TYPE"},
            {0x0935,"NOM_ATTR_LINE_FREQ"},
            {0x0937,"NOM_ATTR_LOCALIZN"},
            {0x093C,"NOM_ATTR_METRIC_INFO_LABEL"},
            {0x093D,"NOM_ATTR_METRIC_INFO_LABEL_STR"},
            {0x093F,"NOM_ATTR_METRIC_SPECN"},
            {0x0940,"NOM_ATTR_METRIC_STAT"},
            {0x0945,"NOM_ATTR_MODE_MSMT"},
            {0x0946,"NOM_ATTR_MODE_OP"},
            {0x0948,"NOM_ATTR_NOM_VERS"},
            {0x094B,"NOM_ATTR_NU_CMPD_VAL_OBS"},
            {0x0950,"NOM_ATTR_NU_VAL_OBS"},
            {0x0956,"NOM_ATTR_PT_BSA"},
            {0x0957,"NOM_ATTR_PT_DEMOG_ST"},
            {0x0958,"NOM_ATTR_PT_DOB"},
            {0x095A,"NOM_ATTR_PT_ID"},
            {0x095C,"NOM_ATTR_PT_NAME_FAMILY"},
            {0x095D,"NOM_ATTR_PT_NAME_GIVEN"},
            {0x0961,"NOM_ATTR_PT_SEX"},
            {0x0962,"NOM_ATTR_PT_TYPE"},
            {0x0964,"NOM_ATTR_SA_CALIB_I16"},
            {0x0967,"NOM_ATTR_SA_CMPD_VAL_OBS"},
            {0x096A,"NOM_ATTR_SA_RANGE_PHYS_I16"},
            {0x096D,"NOM_ATTR_SA_SPECN"},
            {0x096E,"NOM_ATTR_SA_VAL_OBS"},
            {0x096F,"NOM_ATTR_SCALE_SPECN_I16"},
            {0x0982,"NOM_ATTR_STD_SAFETY"},
            {0x0984,"NOM_ATTR_SYS_ID"},
            {0x0985,"NOM_ATTR_SYS_SPECN"},
            {0x0986,"NOM_ATTR_SYS_TYPE"},
            {0x0987,"NOM_ATTR_TIME_ABS"},
            {0x098D,"NOM_ATTR_TIME_PD_SAMP"},
            {0x098F,"NOM_ATTR_TIME_REL"},
            {0x0990,"NOM_ATTR_TIME_STAMP_ABS"},
            {0x0991,"NOM_ATTR_TIME_STAMP_REL"},
            {0x0996,"NOM_ATTR_UNIT_CODE"},
            {0x099E,"NOM_ATTR_VAL_ENUM_OBS"},
            {0x09A7,"NOM_ATTR_VMS_MDS_STAT"},
            {0x09D8,"NOM_ATTR_PT_AGE"},
            {0x09DC,"NOM_ATTR_PT_HEIGHT"},
            {0x09DF,"NOM_ATTR_PT_WEIGHT"},
            {0x0A16,"NOM_ATTR_SA_FIXED_VAL_SPECN"},
            {0x0A1E,"NOM_ATTR_PT_PACED_MODE"},
            {0xF001,"NOM_ATTR_PT_ID_INT"},
            {0xF009,"NOM_ATTR_CMPD_REF_LIST"},
            {0xF100,"NOM_ATTR_NET_ADDR_INFO"},
            {0xF101,"NOM_ATTR_PCOL_SUPPORT"},
            {0xF129,"NOM_ATTR_PT_NOTES1"},
            {0xF12A,"NOM_ATTR_PT_NOTES2"},
            {0xF13E,"NOM_ATTR_TIME_PD_POLL"},
            {0xF1EC,"NOM_ATTR_PT_BSA_FORMULA"},
            {0xF1FA,"NOM_ATTR_MDS_GEN_INFO"},
            {0xF228,"NOM_ATTR_POLL_OBJ_PRIO_NUM"},
            {0xF239,"NOM_ATTR_POLL_NU_PRIO_LIST"},
            {0xF23A,"NOM_ATTR_POLL_RTSA_PRIO_LIST"},
            {0xF294,"NOM_ATTR_METRIC_MODALITY"},
            {0x0801,"NOM_ATTR_GRP_AL_MON"},
            {0x0803,"NOM_ATTR_GRP_METRIC_VAL_OBS"},
            {0x0807,"NOM_ATTR_GRP_PT_DEMOG"},
            {0x080A,"NOM_ATTR_GRP_SYS_APPL"},
            {0x080B,"NOM_ATTR_GRP_SYS_ID"},
            {0x080C,"NOM_ATTR_GRP_SYS_PROD"},
            {0x0810,"NOM_ATTR_GRP_VMO_DYN"},
            {0x0811,"NOM_ATTR_GRP_VMO_STATIC"},
        };
        public static Dictionary<ushort, string> dic = new Dictionary<ushort, string>()
        {
            {0xF8CD,"NOM_VOL_MINUTE_AWAY_INSP_HFV"},
            {0xF8CE,"NOM_VOL_URINE_BAL_PD_INSTANT"},
            {0xF8CF,"NOM_VOL_URINE_SHIFT"},
            {0xF8D1,"NOM_VOL_VENT_L_END_SYS_INDEX"},
            {0xF8D3,"NOM_WEIGHT_URINE_COL"},
            {0xF8D9,"NOM_SETT_APNEA_ALARM_DELAY"},
            {0xF8DE,"NOM_SETT_AWAY_RESP_RATE_APNEA"},
            {0xF8DF,"NOM_SETT_AWAY_RESP_RATE_HFV"},
            {0xF8E6,"NOM_SETT_EVOK_CHARGE"},
            {0xF8E7,"NOM_SETT_EVOK_CURR"},
            {0xF8EA,"NOM_SETT_FLOW_AWAY_EXP"},
            {0xF8EB,"NOM_SETT_FLOW_AWAY_HFV"},
            {0xF8EC,"NOM_SETT_FLOW_AWAY_INSP"},
            {0xF8ED,"NOM_SETT_FLOW_AWAY_INSP_APNEA"},
            {0xF8F3,"NOM_SETT_HFV_AMPL"},
            {0xF8FB,"NOM_SETT_PRESS_AWAY_INSP_MAX_LIMIT_LO"},
            {0xF900,"NOM_SETT_RATIO_IE_EXP_PV"},
            {0xF901,"NOM_SETT_RATIO_IE_EXP_PV_APNEA"},
            {0xF902,"NOM_SETT_RATIO_IE_INSP_PV"},
            {0xF903,"NOM_SETT_RATIO_IE_INSP_PV_APNEA"},
            {0xF904,"NOM_SETT_SENS_LEVEL"},
            {0xF908,"NOM_SETT_TIME_PD_EVOK"},
            {0xF909,"NOM_SETT_TIME_PD_MSMT"},
            {0xF90E,"NOM_SETT_VENT_ANALY_CONC_GAS_O2_MODE"},
            {0xF90F,"NOM_SETT_VENT_AWAY_FLOW_BACKGROUND"},
            {0xF910,"NOM_SETT_VENT_AWAY_FLOW_BASE"},
            {0xF911,"NOM_SETT_VENT_AWAY_FLOW_SENSE"},
            {0xF912,"NOM_SETT_VENT_AWAY_PRESS_RATE_INCREASE"},
            {0xF917,"NOM_SETT_VENT_CONC_AWAY_O2_INSP_APNEA"},
            {0xF918,"NOM_SETT_VENT_CONC_AWAY_O2_INSP_PV_APNEA"},
            {0xF919,"NOM_SETT_VENT_CONC_AWAY_O2_LIMIT_HI"},
            {0xF91A,"NOM_SETT_VENT_CONC_AWAY_O2_LIMIT_LO"},
            {0xF91B,"NOM_SETT_VENT_FLOW"},
            {0xF91C,"NOM_SETT_VENT_FLOW_AWAY_ASSIST"},
            {0xF91D,"NOM_SETT_VENT_FLOW_INSP_TRIG"},
            {0xF920,"NOM_SETT_VENT_GAS_PROBE_POSN"},
            {0xF922,"NOM_SETT_VENT_MODE_MAND_CTS_ONOFF"},
            {0xF923,"NOM_SETT_VENT_MODE_SIGH"},
            {0xF924,"NOM_SETT_VENT_MODE_SYNC_MAND_INTERMIT"},
            {0xF926,"NOM_SETT_VENT_O2_CAL_MODE"},
            {0xF927,"NOM_SETT_VENT_O2_PROBE_POSN"},
            {0xF928,"NOM_SETT_VENT_O2_SUCTION_MODE"},
            {0xF92C,"NOM_SETT_VENT_PRESS_AWAY_END_EXP_POS_INTERMIT"},
            {0xF92D,"NOM_SETT_VENT_PRESS_AWAY_EXP_APRV"},
            {0xF92E,"NOM_SETT_VENT_PRESS_AWAY_INSP_APRV"},
            {0xF930,"NOM_SETT_VENT_PRESS_AWAY_LIMIT_HI"},
            {0xF931,"NOM_SETT_VENT_PRESS_AWAY_MAX_PV_APNEA"},
            {0xF933,"NOM_SETT_VENT_PRESS_AWAY_PV_APNEA"},
            {0xF935,"NOM_SETT_VENT_PRESS_AWAY_SUST_LIMIT_HI"},
            {0xF937,"NOM_SETT_VENT_RESP_RATE_LIMIT_HI_PANT"},
            {0xF939,"NOM_SETT_VENT_RESP_RATE_MODE_PPV_INTERMIT_PAP"},
            {0xF93A,"NOM_SETT_VENT_RESP_RATE_PV_APNEA"},
            {0xF93B,"NOM_SETT_VENT_SIGH_MULT_RATE"},
            {0xF93C,"NOM_SETT_VENT_SIGH_RATE"},
            {0xF93F,"NOM_SETT_VENT_TIME_PD_EXP"},
            {0xF940,"NOM_SETT_VENT_TIME_PD_EXP_APRV"},
            {0xF941,"NOM_SETT_VENT_TIME_PD_INSP"},
            {0xF942,"NOM_SETT_VENT_TIME_PD_INSP_APRV"},
            {0xF943,"NOM_SETT_VENT_TIME_PD_INSP_PV"},
            {0xF944,"NOM_SETT_VENT_TIME_PD_INSP_PV_APNEA"},
            {0xF946,"NOM_SETT_VENT_TIME_PD_RAMP_AL"},
            {0xF948,"NOM_SETT_VENT_VOL_AWAY_ASSIST"},
            {0xF949,"NOM_SETT_VENT_VOL_LIMIT_AL_HI_ONOFF"},
            {0xF94B,"NOM_SETT_VENT_VOL_MINUTE_AWAY_LIMIT_HI"},
            {0xF94C,"NOM_SETT_VENT_VOL_MINUTE_AWAY_LIMIT_LO"},
            {0xF94D,"NOM_SETT_VENT_VOL_TIDAL_LIMIT_HI"},
            {0xF94E,"NOM_SETT_VENT_VOL_TIDAL_LIMIT_LO"},
            {0xF951,"NOM_SETT_VOL_AWAY_TIDAL_APNEA"},
            {0xF952,"NOM_SETT_VOL_AWAY_TIDAL_APPLIED"},
            {0xF953,"NOM_SETT_VOL_MINUTE_ALARM_DELAY"},
            {0xF960,"NOM_SAT_O2_TISSUE"},
            {0xF961,"NOM_CEREB_STATE_INDEX"},
            {0xF962,"NOM_SAT_O2_GEN_1"},
            {0xF963,"NOM_SAT_O2_GEN_2"},
            {0xF964,"NOM_SAT_O2_GEN_3"},
            {0xF965,"NOM_SAT_O2_GEN_4"},
            {0xF966,"NOM_TEMP_CORE_GEN_1"},
            {0xF967,"NOM_TEMP_CORE_GEN_2"},
            {0xF968,"NOM_PRESS_BLD_DIFF"},
            {0xF96C,"NOM_PRESS_BLD_DIFF_GEN_1"},
            {0xF970,"NOM_PRESS_BLD_DIFF_GEN_2"},
            {0xF974,"NOM_FLOW_PUMP_HEART_LUNG_MAIN"},
            {0xF975,"NOM_FLOW_PUMP_HEART_LUNG_SLAVE"},
            {0xF976,"NOM_FLOW_PUMP_HEART_LUNG_SUCTION"},
            {0xF977,"NOM_FLOW_PUMP_HEART_LUNG_AUX"},
            {0xF978,"NOM_FLOW_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN"},
            {0xF979,"NOM_FLOW_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE"},
            {0xF97A,"NOM_TIME_PD_PUMP_HEART_LUNG_AUX_SINCE_START"},
            {0xF97B,"NOM_TIME_PD_PUMP_HEART_LUNG_AUX_SINCE_STOP"},
            {0xF97C,"NOM_VOL_DELIV_PUMP_HEART_LUNG_AUX"},
            {0xF97D,"NOM_VOL_DELIV_TOTAL_PUMP_HEART_LUNG_AUX"},
            {0xF97E,"NOM_TIME_PD_PLEGIA_PUMP_HEART_LUNG_AUX"},
            {0xF97F,"NOM_TIME_PD_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN_SINCE_START"},
            {0xF980,"NOM_TIME_PD_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN_SINCE_STOP"},
            {0xF981,"NOM_VOL_DELIV_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN"},
            {0xF982,"NOM_VOL_DELIV_TOTAL_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN"},
            {0xF983,"NOM_TIME_PD_PLEGIA_PUMP_HEART_LUNG_CARDIOPLEGIA_MAIN"},
            {0xF984,"NOM_TIME_PD_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE_SINCE_START"},
            {0xF985,"NOM_TIME_PD_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE_SINCE_STOP"},
            {0xF986,"NOM_VOL_DELIV_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE"},
            {0xF987,"NOM_VOL_DELIV_TOTAL_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE"},
            {0xF988,"NOM_TIME_PD_PLEGIA_PUMP_HEART_LUNG_CARDIOPLEGIA_SLAVE"},
            {0xF990,"NOM_RATIO_INSP_TOTAL_BREATH_SPONT"},
            {0xF991,"NOM_VENT_PRESS_AWAY_END_EXP_POS_TOTAL"},
            {0xF992,"NOM_COMPL_LUNG_PAV"},
            {0xF993,"NOM_RES_AWAY_PAV"},
            {0xF994,"NOM_RES_AWAY_EXP_TOTAL"},
            {0xF995,"NOM_ELAS_LUNG_PAV"},
            {0xF996,"NOM_BREATH_RAPID_SHALLOW_INDEX_NORM"},
            {0x4BB8,"NOM_PULS_OXIM_SAT_O2"},
            {0xF008,"NOM_SAT_O2_TONE_FREQ"},
            {0x4822,"NOM_PLETH_PULS_RATE"},
            {0x4BB0,"NOM_PULS_OXIM_PERF_REL"},
            {0x4A05,"NOM_PRESS_BLD_NONINV_SYS"},
            {0x4A06,"NOM_PRESS_BLD_NONINV_DIA"},
            {0x4A07,"NOM_PRESS_BLD_NONINV_MEAN"},
            {0x0301,"NOM_ECG_AMPL_ST_I"},
            {0x0302,"NOM_ECG_AMPL_ST_II"},
            {0x033D,"NOM_ECG_AMPL_ST_III"},
            {0x033E,"NOM_ECG_AMPL_ST_AVR"},
            {0x033F,"NOM_ECG_AMPL_ST_AVL"},
            {0x0340,"NOM_ECG_AMPL_ST_AVF"},
            {0x0343,"NOM_ECG_AMPL_ST_V"},
            {0x034B,"NOM_ECG_AMPL_ST_MCL"},
            {0x0303,"NOM_ECG_AMPL_ST_V1"},
            {0x0304,"NOM_ECG_AMPL_ST_V2"},
            {0x0305,"NOM_ECG_AMPL_ST_V3"},
            {0x0306,"NOM_ECG_AMPL_ST_V4"},
            {0x0307,"NOM_ECG_AMPL_ST_V5"},
            {0x0308,"NOM_ECG_AMPL_ST_V6"},
            {0x0365,"NOM_ECG_AMPL_ST_AS"},
            {0x0364,"NOM_ECG_AMPL_ST_ES"},
            {0x0366,"NOM_ECG_AMPL_ST_AI"},
            {0x4A15,"NOM_PRESS_BLD_ART_ABP_SYS"},
            {0x4A16,"NOM_PRESS_BLD_ART_ABP_DIA"},
            {0x4A17,"NOM_PRESS_BLD_ART_ABP_MEAN"},
            {0x50B0,"NOM_AWAY_CO2_ET"},
            {0x50BA,"NOM_AWAY_CO2_INSP_MIN"},
            {0x538C,"NOM_CONC_AWAY_AGENT_ET"},
            {0x5390,"NOM_CONC_AWAY_AGENT_INSP"},
            {0x5214,"NOM_CONC_AWAY_DESFL_ET"},
            {0x5268,"NOM_CONC_AWAY_DESFL_INSP"},
            {0x5218,"NOM_CONC_AWAY_ENFL_ET"},
            {0x526C,"NOM_CONC_AWAY_ENFL_INSP"},
            {0x521C,"NOM_CONC_AWAY_HALOTH_ET"},
            {0x5270,"NOM_CONC_AWAY_HALOTH_INSP"},
            {0x5224,"NOM_CONC_AWAY_ISOFL_ET"},
            {0x5278,"NOM_CONC_AWAY_ISOFL_INSP"},
            {0x5380,"NOM_CONC_AWAY_N2_ET"},
            {0x5384,"NOM_CONC_AWAY_N2_INSP"},
            {0x522C,"NOM_CONC_AWAY_N2O_ET"},
            {0x5280,"NOM_CONC_AWAY_N2O_INSP"},
            {0x5378,"NOM_CONC_AWAY_O2_ET"},
            {0x5284,"NOM_CONC_AWAY_O2_INSP"},
            {0x5220,"NOM_CONC_AWAY_SEVOFL_ET"},
            {0x5274,"NOM_CONC_AWAY_SEVOFL_INSP"},
            {0xF05E,"NOM_CONC_AWAY_SUM_MAC_ET"},
            {0xF05F,"NOM_CONC_AWAY_SUM_MAC_INSP"},
            {0x597C,"NOM_EEG_FREQ_PWR_SPEC_CRTX_DOM_MEAN"},
            {0x5984,"NOM_EEG_FREQ_PWR_SPEC_CRTX_PEAK"},
            {0x5988,"NOM_EEG_FREQ_PWR_SPEC_CRTX_SPECTRAL_EDGE"},
            {0x59D4,"NOM_EEG_PWR_SPEC_ALPHA_REL"},
            {0x59D8,"NOM_EEG_PWR_SPEC_BETA_REL"},
            {0x59DC,"NOM_EEG_PWR_SPEC_DELTA_REL"},
            {0x59E0,"NOM_EEG_PWR_SPEC_THETA_REL"},
            {0x4A0E,"NOM_PRESS_BLD_AORT_DIA"},
            {0x4A0F,"NOM_PRESS_BLD_AORT_MEAN"},
            {0x4A0D,"NOM_PRESS_BLD_AORT_SYS"},
            {0xF0C2,"NOM_PRESS_BLD_ART_BRACHIAL_DIA"},
            {0xF0C3,"NOM_PRESS_BLD_ART_BRACHIAL_MEAN"},
            {0xF0C1,"NOM_PRESS_BLD_ART_BRACHIAL_SYS"},
            {0x4A12,"NOM_PRESS_BLD_ART_DIA"},
            {0xF0BE,"NOM_PRESS_BLD_ART_FEMORAL_DIA"},
            {0xF0BF,"NOM_PRESS_BLD_ART_FEMORAL_MEAN"},
            {0xF0BD,"NOM_PRESS_BLD_ART_FEMORAL_SYS"},
            {0x4A13,"NOM_PRESS_BLD_ART_MEAN"},
            {0x4A1E,"NOM_PRESS_BLD_ART_PULM_DIA"},
            {0x4A1F,"NOM_PRESS_BLD_ART_PULM_MEAN"},
            {0x4A1D,"NOM_PRESS_BLD_ART_PULM_SYS"},
            {0x4A11,"NOM_PRESS_BLD_ART_SYS"},
            {0x4A2A,"NOM_PRESS_BLD_ART_UMB_DIA"},
            {0x4A2B,"NOM_PRESS_BLD_ART_UMB_MEAN"},
            {0x4A29,"NOM_PRESS_BLD_ART_UMB_SYS"},
            {0x4A32,"NOM_PRESS_BLD_ATR_LEFT_DIA"},
            {0x4A33,"NOM_PRESS_BLD_ATR_LEFT_MEAN"},
            {0x4A31,"NOM_PRESS_BLD_ATR_LEFT_SYS"},
            {0x4A36,"NOM_PRESS_BLD_ATR_RIGHT_DIA"},
            {0x4A37,"NOM_PRESS_BLD_ATR_RIGHT_MEAN"},
            {0x4A35,"NOM_PRESS_BLD_ATR_RIGHT_SYS"},
            {0x4A02,"NOM_PRESS_BLD_DIA"},
            {0x4A03,"NOM_PRESS_BLD_MEAN"},
            {0x4A01,"NOM_PRESS_BLD_SYS"},
            {0x4A46,"NOM_PRESS_BLD_VEN_CENT_DIA"},
            {0x4A47,"NOM_PRESS_BLD_VEN_CENT_MEAN"},
            {0x4A45,"NOM_PRESS_BLD_VEN_CENT_SYS"},
            {0x4A4A,"NOM_PRESS_BLD_VEN_UMB_DIA"},
            {0x4A4B,"NOM_PRESS_BLD_VEN_UMB_MEAN"},
            {0x4A49,"NOM_PRESS_BLD_VEN_UMB_SYS"},
            {0xF0A6,"NOM_PRESS_GEN_1_DIA"},
            {0xF0A7,"NOM_PRESS_GEN_1_MEAN"},
            {0xF0A5,"NOM_PRESS_GEN_1_SYS"},
            {0xF0AA,"NOM_PRESS_GEN_2_DIA"},
            {0xF0AB,"NOM_PRESS_GEN_2_MEAN"},
            {0xF0A9,"NOM_PRESS_GEN_2_SYS"},
            {0xF0AC,"NOM_PRESS_GEN_3"},
            {0xF0AF,"NOM_PRESS_GEN_3_MEAN"},
            {0xF0AD,"NOM_PRESS_GEN_3_SYS"},
            {0xF0B2,"NOM_PRESS_GEN_4_DIA"},
            {0xF0B3,"NOM_PRESS_GEN_4_MEAN"},
            {0xF0B1,"NOM_PRESS_GEN_4_SYS"},
            {0xF3F6,"NOM_PRESS_GEN_5_DIA"},
            {0xF3F7,"NOM_PRESS_GEN_5_MEAN"},
            {0xF3F5,"NOM_PRESS_GEN_5_SYS"},
            {0xF3FA,"NOM_PRESS_GEN_6_DIA"},
            {0xF3FB,"NOM_PRESS_GEN_6_MEAN"},
            {0xF3F9,"NOM_PRESS_GEN_6_SYS"},
            {0xF3FC,"NOM_PRESS_GEN_7"},
            {0xF3FF,"NOM_PRESS_GEN_7_MEAN"},
            {0xF3FD,"NOM_PRESS_GEN_7_SYS"},
            {0xF402,"NOM_PRESS_GEN_8_DIA"},
            {0xF403,"NOM_PRESS_GEN_8_MEAN"},
            {0xF401,"NOM_PRESS_GEN_8_SYS"},
            {0xF0B6,"NOM_PRESS_INTRA_CRAN_1_DIA"},
            {0xF0B7,"NOM_PRESS_INTRA_CRAN_1_MEAN"},
            {0xF0B5,"NOM_PRESS_INTRA_CRAN_1_SYS"},
            {0xF0BA,"NOM_PRESS_INTRA_CRAN_2_DIA"},
            {0xF0BB,"NOM_PRESS_INTRA_CRAN_2_MEAN"},
            {0xF0B9,"NOM_PRESS_INTRA_CRAN_2_SYS"},
            {0x580A,"NOM_PRESS_INTRA_CRAN_DIA"},
            {0x580B,"NOM_PRESS_INTRA_CRAN_MEAN"},
            {0x5809,"NOM_PRESS_INTRA_CRAN_SYS"},
            {0x514C,"NOM_VOL_MINUTE_AWAY_EXP"},
            {0x5150,"NOM_VOL_MINUTE_AWAY_INSP"},
            {0xF0E5,"NOM_PRESS_BLD_NONINV_PULS_RATE"},
            {0x4182,"NOM_ECG_CARD_BEAT_RATE"},
            {0x5012,"NOM_AWAY_RESP_RATE"},

        };


        #endregion
    }
}
