# [Framework数据采集插件开发指南](https://gitee.com/renqx/RQX.Public.Repository/blob/master/Doc/Framework%E6%95%B0%E6%8D%AE%E9%87%87%E9%9B%86%E6%8F%92%E4%BB%B6%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97.html)

--|--|--
<b><center>版本|<b><center>作者|<b><center>描述
1.0|rqx|初稿

## 框架要求

+ Framework版本：[Framework4.6](https://www.microsoft.com/en-us/download/details.aspx?id=48137)
+ Nuget包：[RQX.Common.Framework](https://gitee.com/renqx/RQX.Public.Repository/tree/master/Source/C%23/Framework/Common/RQX.Common.Framework)

## 继承基础类
--|--|--
<b><center>协议类型|<b><center>继承基类|<b><center>协议类
COM|AbstractComCollection|MyCom
TCP客户端|AbstractTcpClientCollection|TcpSocketClient
TCP服务端|AbstractTcpServerCollection|TcpSocketServer
UDP|AbstractUdpCollection|UdpSocket

## 流程逻辑

1. 连接建立
2. 循环采集
3. 数据解析

## 含义解释

--|--|--
<b><center>类型|<b><center>名字|<b><center>含义
枚举|StepType|当前收到的数据所处步骤<br>Init => 初始化<br>DataSource => 采集数据<br>FinishConnect => 连接建立完毕，可进行循环采集<br>Setp1~5 => 具体连接步骤
虚函数|void Connect(StepType stepNum)|连接建立函数，应根据StepType确定接下来的操作<br>DataSource步骤不会进入改方法
虚函数|void CircleSend()|循环采集函数，当StepType值为FinishConnect时会调用该方法<br>若循环采集可在函数中写循环发送命令
虚函数|string CutToOneMsg(StringBuilder sb)|收到的数据会存入StringBuilder<br>该函数需要从StringBuilder中判断命令结束符<br>并返回所需数据
虚函数|StepType CheckRecvMsgType(string lineStr)|通过切分下来的命令数据，判断具体含义，并告知是处于什么步骤
虚函数|Dictionary<string, double> AnalysisData(byte[] buffer)|当StepType的值为DataSource时会调用，进行具体采集内容解析

## 对外接口

+ 开始进行采集

		public virtual void Start()

+ 关闭采集

		public virtual void Dispose()

+ 解析后的数据处理

		public Action<Dictionary<string, double>> DealDataHandler

## 日志记录

+ 标志是否记录接收数据日志，默认开启

		public bool WriteRecvLog

+ 可重写接收数据日志记录方法

		protected virtual void WriteRecvLogMethod(string msg)

+ 标志是否记录发送数据日志，默认开启

		public bool WriteSendLog

+ 可重写发送数据日志记录方法

		protected virtual void WriteSendLogMethod(string msg)

## [Demo地址](https://gitee.com/renqx/RQX.Public.Repository/tree/master/Source/C%23/Framework/Common/RQX.Common.Framework/Test/DataCollection.Plugins.Demo)

