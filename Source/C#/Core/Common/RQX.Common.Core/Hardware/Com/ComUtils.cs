﻿using RQX.Common.Core.Model;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;

namespace RQX.Common.Core.Hardware.Com
{
    public class ComUtils
    {
        /// <summary>
        /// 获取串口列表
        /// </summary>
        /// <returns></returns>
        public static List<ListItem> GetComList()
        {
            var nameList = SerialPort.GetPortNames().OrderBy(k => k).ToList();
            var list = new List<ListItem>();
            nameList.ForEach(item => list.Add(new ListItem(list.Count(), item)));
            return list;
        }
        /// <summary>
        /// 获取波特率列表
        /// </summary>
        /// <returns></returns>
        public static List<ListItem> GetBaudRateList()
        {
            var list = new List<ListItem>
            {
                new ListItem(110),
                new ListItem(300),
                new ListItem(600),
                new ListItem(1200),
                new ListItem(2400),
                new ListItem(4800),
                new ListItem(9600),
                new ListItem(14400),
                new ListItem(19200),
                new ListItem(38400),
                new ListItem(56000),
                new ListItem(57600),
                new ListItem(115200)
            };
            return list;
        }
        /// <summary>
        /// 获取校验位列表
        /// </summary>
        /// <returns></returns>
        public static List<ListItem> GetParityList()
        {
            var list = new List<ListItem>
            {
                new ListItem(0,Parity.None.ToString()),
                new ListItem(1,Parity.Odd.ToString()),
                new ListItem(2,Parity.Even.ToString()),
                new ListItem(3,Parity.Mark.ToString()),
                new ListItem(4,Parity.Space.ToString()),
            };
            return list;
        }
        /// <summary>
        /// 获取数据位列表
        /// </summary>
        /// <returns></returns>
        public static List<ListItem> GetDataBitsList()
        {
            var list = new List<ListItem>();
            for (var i = 5; i <= 8; i++)
            {
                list.Add(new ListItem(i, $"{i}位"));
            }
            return list;
        }
        /// <summary>
        /// 获取停止位列表
        /// </summary>
        /// <returns></returns>
        public static List<ListItem> GetStopBitsList()
        {
            var list = new List<ListItem>
            {
                new ListItem(0,StopBits.None.ToString()),
                new ListItem(1,StopBits.One.ToString()),
                new ListItem(2,StopBits.OnePointFive.ToString()),
                new ListItem(3,StopBits.Two.ToString()),
            };
            return list;
        }
    }
}
