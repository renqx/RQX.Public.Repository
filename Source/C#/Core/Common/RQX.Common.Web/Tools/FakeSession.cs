﻿using RQX.Common.Web.Logger.InterfaceLogger;
using System.Threading;

namespace RQX.Common.Web.Tools
{
    public class FakeSession
    {
        public static ThreadLocal<string> loginUserStr = new ThreadLocal<string>();
        public static ThreadLocal<int> interfaceLogId = new ThreadLocal<int>();
        public static ThreadLocal<InterfaceLog> interfaceEntity = new ThreadLocal<InterfaceLog>();


    }
}
