﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DataCollection
{
    public class FormUtils
    {
        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="comboBox">下拉框实例</param>
        /// <param name="list">数据源列表</param>
        /// <param name="initValue">初始选中的value值</param>
        /// <param name="initText">初始选中的text值</param>
        public static void BindListItemToDDL(ComboBox comboBox, List<ListItem> list, int? initValue = null, string initText = null)
        {
            comboBox.DataSource = list;
            comboBox.ValueMember = "Value";
            comboBox.DisplayMember = "Text";
            if (initValue.HasValue)
            {
                var item = comboBox.Items.OfType<ListItem>().Where(k => k.Value.Equals(initValue.Value)).FirstOrDefault();
                comboBox.SelectedIndex = comboBox.Items.IndexOf(item);
            }
            else if (initText.IsNotNullOrEmpty())
            {
                var item = comboBox.Items.OfType<ListItem>().Where(k => k.Text.Equals(initText)).FirstOrDefault();
                comboBox.SelectedIndex = comboBox.Items.IndexOf(item);
            }
        }
        /// <summary>
        /// 显示提示信息
        /// </summary>
        /// <param name = "msg" ></ param >
        public static void ShowMsg(string msg)
        {
            MessageBox.Show(msg);
        }
        /// <summary>
        /// try/catch的方法
        /// </summary>
        /// <param name="action"></param>
        public static void TryMethod(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                ShowMsg(ex.Message);
            }
        }
        /// <summary>
        /// 获取下拉框选中项
        /// </summary>
        /// <param name="comboBox"></param>
        /// <returns></returns>
        public static ListItem GetComboBoxSelected(ComboBox comboBox)
        {
            var item = comboBox.Items[comboBox.SelectedIndex];
            if (item is ListItem result)
            {
                return result;
            }
            else
            {
                return new ListItem(Convert.ToInt32(comboBox.SelectedValue), comboBox.SelectedText);
            }
        }
        /// <summary>
        /// 找到下拉框的序号
        /// </summary>
        /// <param name="comboBox"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int FindDDLIndex(ComboBox comboBox, object value)
        {
            var items = comboBox.Items.OfType<ListItem>().Where(k => k.Value.Equals(value.ToString()) || k.Text.Equals(value.ToString()));
            if (items.Count() > 0)
            {
                return comboBox.Items.IndexOf(items.FirstOrDefault());
            }
            else
            {
                return -1;
            }
        }
    }
}
