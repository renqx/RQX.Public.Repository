﻿using System.Drawing;
using System.Windows.Forms;

namespace DataCollection
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tab = new System.Windows.Forms.TabControl();
            this.tab_device = new System.Windows.Forms.TabPage();
            this.grid = new System.Windows.Forms.DataGridView();
            this.dgv_guid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_device_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_protocol_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_plug_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_control = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tab_message = new System.Windows.Forms.TabPage();
            this.txt_real_time_msg = new System.Windows.Forms.RichTextBox();
            this.tab_errorlog = new System.Windows.Forms.TabPage();
            this.rtxt_errorlog = new System.Windows.Forms.RichTextBox();
            this.tv_errorlog = new System.Windows.Forms.TreeView();
            this.tab_debuglog = new System.Windows.Forms.TabPage();
            this.rtxt_debuglog = new System.Windows.Forms.RichTextBox();
            this.tv_debuglog = new System.Windows.Forms.TreeView();
            this.notify_icon = new System.Windows.Forms.NotifyIcon(this.components);
            this.menu_notify_icon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_notify_icon_device_name = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_change_code = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_setting = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_setting_local = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_setting_db = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_tools = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_tools_simulate = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_notify_icon_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menu_device_manage = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_code_manage = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_setting = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_setting_local = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_setting_db = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_tools = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_tools_simulate = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_version = new System.Windows.Forms.ToolStripMenuItem();
            this.tab.SuspendLayout();
            this.tab_device.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.tab_message.SuspendLayout();
            this.tab_errorlog.SuspendLayout();
            this.tab_debuglog.SuspendLayout();
            this.menu_notify_icon.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tab_device);
            this.tab.Controls.Add(this.tab_message);
            this.tab.Controls.Add(this.tab_errorlog);
            this.tab.Controls.Add(this.tab_debuglog);
            this.tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab.Location = new System.Drawing.Point(0, 25);
            this.tab.Margin = new System.Windows.Forms.Padding(4);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(1143, 554);
            this.tab.TabIndex = 4;
            this.tab.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tab_device
            // 
            this.tab_device.Controls.Add(this.grid);
            this.tab_device.Location = new System.Drawing.Point(4, 26);
            this.tab_device.Margin = new System.Windows.Forms.Padding(4);
            this.tab_device.Name = "tab_device";
            this.tab_device.Padding = new System.Windows.Forms.Padding(4);
            this.tab_device.Size = new System.Drawing.Size(1135, 524);
            this.tab_device.TabIndex = 0;
            this.tab_device.Text = "设备列表";
            this.tab_device.UseVisualStyleBackColor = true;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_guid,
            this.dgv_device_name,
            this.dgv_protocol_type,
            this.dgv_plug_name,
            this.dgv_param,
            this.dgv_status,
            this.dgv_control});
            this.grid.Location = new System.Drawing.Point(4, 4);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(1127, 517);
            this.grid.TabIndex = 3;
            this.grid.Text = "dataGridView1";
            this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellContentClick);
            // 
            // dgv_guid
            // 
            this.dgv_guid.HeaderText = "GUID";
            this.dgv_guid.Name = "dgv_guid";
            this.dgv_guid.ReadOnly = true;
            this.dgv_guid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgv_guid.Visible = false;
            // 
            // dgv_device_name
            // 
            this.dgv_device_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_device_name.FillWeight = 150F;
            this.dgv_device_name.HeaderText = "设备名称";
            this.dgv_device_name.Name = "dgv_device_name";
            this.dgv_device_name.ReadOnly = true;
            this.dgv_device_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_protocol_type
            // 
            this.dgv_protocol_type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_protocol_type.HeaderText = "协议类型";
            this.dgv_protocol_type.Name = "dgv_protocol_type";
            this.dgv_protocol_type.ReadOnly = true;
            this.dgv_protocol_type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_plug_name
            // 
            this.dgv_plug_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_plug_name.FillWeight = 150F;
            this.dgv_plug_name.HeaderText = "插件名称";
            this.dgv_plug_name.Name = "dgv_plug_name";
            this.dgv_plug_name.ReadOnly = true;
            this.dgv_plug_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_param
            // 
            this.dgv_param.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_param.FillWeight = 200F;
            this.dgv_param.HeaderText = "连接参数";
            this.dgv_param.Name = "dgv_param";
            this.dgv_param.ReadOnly = true;
            this.dgv_param.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_status
            // 
            this.dgv_status.HeaderText = "状态";
            this.dgv_status.Name = "dgv_status";
            this.dgv_status.ReadOnly = true;
            this.dgv_status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_control
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "启动服务";
            this.dgv_control.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_control.HeaderText = "操作";
            this.dgv_control.Name = "dgv_control";
            this.dgv_control.ReadOnly = true;
            // 
            // tab_message
            // 
            this.tab_message.Controls.Add(this.txt_real_time_msg);
            this.tab_message.Location = new System.Drawing.Point(4, 26);
            this.tab_message.Margin = new System.Windows.Forms.Padding(4);
            this.tab_message.Name = "tab_message";
            this.tab_message.Padding = new System.Windows.Forms.Padding(4);
            this.tab_message.Size = new System.Drawing.Size(1135, 524);
            this.tab_message.TabIndex = 1;
            this.tab_message.Text = "实时信息";
            this.tab_message.UseVisualStyleBackColor = true;
            // 
            // txt_real_time_msg
            // 
            this.txt_real_time_msg.Location = new System.Drawing.Point(10, 10);
            this.txt_real_time_msg.Margin = new System.Windows.Forms.Padding(4);
            this.txt_real_time_msg.Name = "txt_real_time_msg";
            this.txt_real_time_msg.Size = new System.Drawing.Size(1114, 506);
            this.txt_real_time_msg.TabIndex = 0;
            this.txt_real_time_msg.Text = "";
            // 
            // tab_errorlog
            // 
            this.tab_errorlog.Controls.Add(this.rtxt_errorlog);
            this.tab_errorlog.Controls.Add(this.tv_errorlog);
            this.tab_errorlog.Location = new System.Drawing.Point(4, 26);
            this.tab_errorlog.Margin = new System.Windows.Forms.Padding(4);
            this.tab_errorlog.Name = "tab_errorlog";
            this.tab_errorlog.Padding = new System.Windows.Forms.Padding(4);
            this.tab_errorlog.Size = new System.Drawing.Size(1135, 524);
            this.tab_errorlog.TabIndex = 2;
            this.tab_errorlog.Text = "错误日志";
            this.tab_errorlog.UseVisualStyleBackColor = true;
            // 
            // rtxt_errorlog
            // 
            this.rtxt_errorlog.Location = new System.Drawing.Point(285, 10);
            this.rtxt_errorlog.Margin = new System.Windows.Forms.Padding(4);
            this.rtxt_errorlog.Name = "rtxt_errorlog";
            this.rtxt_errorlog.Size = new System.Drawing.Size(840, 510);
            this.rtxt_errorlog.TabIndex = 1;
            this.rtxt_errorlog.Text = "";
            // 
            // tv_errorlog
            // 
            this.tv_errorlog.Location = new System.Drawing.Point(9, 8);
            this.tv_errorlog.Margin = new System.Windows.Forms.Padding(4);
            this.tv_errorlog.Name = "tv_errorlog";
            this.tv_errorlog.Size = new System.Drawing.Size(266, 511);
            this.tv_errorlog.TabIndex = 0;
            this.tv_errorlog.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_errorlog_AfterSelect);
            // 
            // tab_debuglog
            // 
            this.tab_debuglog.Controls.Add(this.rtxt_debuglog);
            this.tab_debuglog.Controls.Add(this.tv_debuglog);
            this.tab_debuglog.Location = new System.Drawing.Point(4, 26);
            this.tab_debuglog.Margin = new System.Windows.Forms.Padding(4);
            this.tab_debuglog.Name = "tab_debuglog";
            this.tab_debuglog.Padding = new System.Windows.Forms.Padding(4);
            this.tab_debuglog.Size = new System.Drawing.Size(1135, 524);
            this.tab_debuglog.TabIndex = 3;
            this.tab_debuglog.Text = "调试日志";
            this.tab_debuglog.UseVisualStyleBackColor = true;
            // 
            // rtxt_debuglog
            // 
            this.rtxt_debuglog.Location = new System.Drawing.Point(284, 8);
            this.rtxt_debuglog.Margin = new System.Windows.Forms.Padding(4);
            this.rtxt_debuglog.Name = "rtxt_debuglog";
            this.rtxt_debuglog.Size = new System.Drawing.Size(840, 510);
            this.rtxt_debuglog.TabIndex = 2;
            this.rtxt_debuglog.Text = "";
            // 
            // tv_debuglog
            // 
            this.tv_debuglog.Location = new System.Drawing.Point(9, 8);
            this.tv_debuglog.Margin = new System.Windows.Forms.Padding(4);
            this.tv_debuglog.Name = "tv_debuglog";
            this.tv_debuglog.Size = new System.Drawing.Size(266, 511);
            this.tv_debuglog.TabIndex = 1;
            this.tv_debuglog.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_debuglog_AfterSelect);
            // 
            // notify_icon
            // 
            this.notify_icon.ContextMenuStrip = this.menu_notify_icon;
            this.notify_icon.Icon = ((System.Drawing.Icon)(resources.GetObject("notify_icon.Icon")));
            this.notify_icon.Text = "手麻采集程序";
            this.notify_icon.Visible = true;
            this.notify_icon.DoubleClick += new System.EventHandler(this.notify_icon_DoubleClick);
            // 
            // menu_notify_icon
            // 
            this.menu_notify_icon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_notify_icon_device_name,
            this.menu_notify_icon_change_code,
            this.menu_notify_icon_setting,
            this.menu_notify_icon_tools,
            this.menu_notify_icon_exit});
            this.menu_notify_icon.Name = "menu_notify_icon";
            this.menu_notify_icon.Size = new System.Drawing.Size(125, 114);
            // 
            // menu_notify_icon_device_name
            // 
            this.menu_notify_icon_device_name.Name = "menu_notify_icon_device_name";
            this.menu_notify_icon_device_name.Size = new System.Drawing.Size(124, 22);
            this.menu_notify_icon_device_name.Text = "设备管理";
            this.menu_notify_icon_device_name.Click += new System.EventHandler(this.menu_device_manage_Click);
            // 
            // menu_notify_icon_change_code
            // 
            this.menu_notify_icon_change_code.Name = "menu_notify_icon_change_code";
            this.menu_notify_icon_change_code.Size = new System.Drawing.Size(124, 22);
            this.menu_notify_icon_change_code.Text = "对码管理";
            this.menu_notify_icon_change_code.Click += new System.EventHandler(this.menu_change_code_Click);
            // 
            // menu_notify_icon_setting
            // 
            this.menu_notify_icon_setting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_notify_icon_setting_local,
            this.menu_notify_icon_setting_db});
            this.menu_notify_icon_setting.Name = "menu_notify_icon_setting";
            this.menu_notify_icon_setting.Size = new System.Drawing.Size(124, 22);
            this.menu_notify_icon_setting.Text = "设置";
            // 
            // menu_notify_icon_setting_local
            // 
            this.menu_notify_icon_setting_local.Name = "menu_notify_icon_setting_local";
            this.menu_notify_icon_setting_local.Size = new System.Drawing.Size(136, 22);
            this.menu_notify_icon_setting_local.Text = "本地设置";
            this.menu_notify_icon_setting_local.Click += new System.EventHandler(this.menu_setting_local_Click);
            // 
            // menu_notify_icon_setting_db
            // 
            this.menu_notify_icon_setting_db.Name = "menu_notify_icon_setting_db";
            this.menu_notify_icon_setting_db.Size = new System.Drawing.Size(136, 22);
            this.menu_notify_icon_setting_db.Text = "数据库设置";
            this.menu_notify_icon_setting_db.Click += new System.EventHandler(this.menu_setting_db_Click);
            // 
            // menu_notify_icon_tools
            // 
            this.menu_notify_icon_tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_notify_icon_tools_simulate});
            this.menu_notify_icon_tools.Name = "menu_notify_icon_tools";
            this.menu_notify_icon_tools.Size = new System.Drawing.Size(124, 22);
            this.menu_notify_icon_tools.Text = "工具";
            // 
            // menu_notify_icon_tools_simulate
            // 
            this.menu_notify_icon_tools_simulate.Name = "menu_notify_icon_tools_simulate";
            this.menu_notify_icon_tools_simulate.Size = new System.Drawing.Size(148, 22);
            this.menu_notify_icon_tools_simulate.Text = "模拟采集工具";
            this.menu_notify_icon_tools_simulate.Click += new System.EventHandler(this.menu_tools_simulate_Click);
            // 
            // menu_notify_icon_exit
            // 
            this.menu_notify_icon_exit.Name = "menu_notify_icon_exit";
            this.menu_notify_icon_exit.Size = new System.Drawing.Size(124, 22);
            this.menu_notify_icon_exit.Text = "退出";
            this.menu_notify_icon_exit.Click += new System.EventHandler(this.menu_notify_icon_exit_Click);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_device_manage,
            this.menu_code_manage,
            this.menu_setting,
            this.menu_tools,
            this.menu_version});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1143, 25);
            this.menu.TabIndex = 6;
            this.menu.Text = "menuStrip1";
            // 
            // menu_device_manage
            // 
            this.menu_device_manage.Name = "menu_device_manage";
            this.menu_device_manage.Size = new System.Drawing.Size(68, 21);
            this.menu_device_manage.Text = "设备管理";
            this.menu_device_manage.Click += new System.EventHandler(this.menu_device_manage_Click);
            // 
            // menu_code_manage
            // 
            this.menu_code_manage.Name = "menu_code_manage";
            this.menu_code_manage.Size = new System.Drawing.Size(68, 21);
            this.menu_code_manage.Text = "对码管理";
            this.menu_code_manage.Click += new System.EventHandler(this.menu_change_code_Click);
            // 
            // menu_setting
            // 
            this.menu_setting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_setting_local,
            this.menu_setting_db});
            this.menu_setting.Name = "menu_setting";
            this.menu_setting.Size = new System.Drawing.Size(44, 21);
            this.menu_setting.Text = "设置";
            // 
            // menu_setting_local
            // 
            this.menu_setting_local.Name = "menu_setting_local";
            this.menu_setting_local.Size = new System.Drawing.Size(136, 22);
            this.menu_setting_local.Text = "本地设置";
            this.menu_setting_local.Click += new System.EventHandler(this.menu_setting_local_Click);
            // 
            // menu_setting_db
            // 
            this.menu_setting_db.Name = "menu_setting_db";
            this.menu_setting_db.Size = new System.Drawing.Size(136, 22);
            this.menu_setting_db.Text = "数据库设置";
            this.menu_setting_db.Click += new System.EventHandler(this.menu_setting_db_Click);
            // 
            // menu_tools
            // 
            this.menu_tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_tools_simulate});
            this.menu_tools.Name = "menu_tools";
            this.menu_tools.Size = new System.Drawing.Size(44, 21);
            this.menu_tools.Text = "工具";
            // 
            // menu_tools_simulate
            // 
            this.menu_tools_simulate.Name = "menu_tools_simulate";
            this.menu_tools_simulate.Size = new System.Drawing.Size(148, 22);
            this.menu_tools_simulate.Text = "模拟采集工具";
            this.menu_tools_simulate.Click += new System.EventHandler(this.menu_tools_simulate_Click);
            // 
            // menu_version
            // 
            this.menu_version.Name = "menu_version";
            this.menu_version.Size = new System.Drawing.Size(68, 21);
            this.menu_version.Text = "版本信息";
            this.menu_version.Click += new System.EventHandler(this.menu_version_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 579);
            this.Controls.Add(this.tab);
            this.Controls.Add(this.menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设备中央监护站";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FConsole_FormClosing);
            this.Load += new System.EventHandler(this.FConsole_Load);
            this.Resize += new System.EventHandler(this.FConsole_Resize);
            this.tab.ResumeLayout(false);
            this.tab_device.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.tab_message.ResumeLayout(false);
            this.tab_errorlog.ResumeLayout(false);
            this.tab_debuglog.ResumeLayout(false);
            this.menu_notify_icon.ResumeLayout(false);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tab_device;
        private System.Windows.Forms.TabPage tab_message;
        private System.Windows.Forms.TabPage tab_errorlog;
        private System.Windows.Forms.TabPage tab_debuglog;
        private System.Windows.Forms.RichTextBox rtxt_errorlog;
        private System.Windows.Forms.TreeView tv_errorlog;
        private System.Windows.Forms.RichTextBox rtxt_debuglog;
        private System.Windows.Forms.TreeView tv_debuglog;
        private System.Windows.Forms.RichTextBox txt_real_time_msg;
        private System.Windows.Forms.NotifyIcon notify_icon;
        private System.Windows.Forms.ContextMenuStrip menu_notify_icon;
        private System.Windows.Forms.ToolStripMenuItem menu_notify_icon_exit;
        private System.Windows.Forms.ToolStripMenuItem menu_notify_icon_setting;
        private DataGridView grid;
        private ToolStripMenuItem menu_notify_icon_change_code;
        private DataGridViewTextBoxColumn dgv_guid;
        private DataGridViewTextBoxColumn dgv_device_name;
        private DataGridViewTextBoxColumn dgv_protocol_type;
        private DataGridViewTextBoxColumn dgv_plug_name;
        private DataGridViewTextBoxColumn dgv_param;
        private DataGridViewTextBoxColumn dgv_status;
        private DataGridViewButtonColumn dgv_control;
        private MenuStrip menu;
        private ToolStripMenuItem menu_device_manage;
        private ToolStripMenuItem menu_code_manage;
        private ToolStripMenuItem menu_setting;
        private ToolStripMenuItem menu_version;
        private ToolStripMenuItem menu_notify_icon_device_name;
        private ToolStripMenuItem menu_setting_local;
        private ToolStripMenuItem menu_setting_db;
        private ToolStripMenuItem menu_notify_icon_setting_local;
        private ToolStripMenuItem menu_notify_icon_setting_db;
        private ToolStripMenuItem menu_tools;
        private ToolStripMenuItem menu_tools_simulate;
        private ToolStripMenuItem menu_notify_icon_tools;
        private ToolStripMenuItem menu_notify_icon_tools_simulate;
    }
}

