﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Form;
using RQX.Common.Core.Hardware.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RQX.DataCollection.Test.Net
{
    public partial class TcpClientForm : Form
    {
        #region Init
        private TcpSocketClient tcpClient;
        public TcpClientForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TcpClientForm_Load(object sender, EventArgs e)
        {
            txt_set_ip.Text = "127.0.0.1";
            ckb_recv_autosave.Enabled = false;
            ckb_send_file.Enabled = false;
            ckb_send_circle.Enabled = false;
            txt_send_circle_time.Enabled = false;
            btn_save.Enabled = false;

            txt_send_circle_time.Text = "1000";
            ckb_recv_hex.Checked = true;
            ckb_recv_newline.Checked = true;
            ckb_send_hex.Checked = true;
        }
        #endregion

        #region 事件
        /// <summary>
        /// 连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_connect_Click(object sender, EventArgs e)
        {
            var ip = txt_set_ip.Text.Trim();
            int.TryParse(txt_set_port.Text.Trim(), out int port);

            if (ip.IsNullOrEmpty())
            {
                MessageBox.Show("请填写目标IP地址！");
                return;
            }

            if (port == 0)
            {
                MessageBox.Show("请填写正确的目标端口号！");
                return;
            }

            if (tcpClient.IsNull())
            {
                tcpClient = new TcpSocketClient(ip, port);
                tcpClient.HandleMsgReceived = (c, buffer) => AppendToRecvText(buffer);
                tcpClient.Start();
                ChangeConnectBtn(true);
            }
            else
            {
                tcpClient.Close();
                tcpClient = null;
                ChangeConnectBtn(false);
            }
        }
        /// <summary>
        /// 清除接收区
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_clean_Click(object sender, EventArgs e)
        {
            txt_recv.Text = "";
        }
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_send_Click(object sender, EventArgs e)
        {
            var sendStr = txt_send.Text.Trim();
            if(tcpClient.IsNull())
            {
                MessageBox.Show("请先打开连接！");
                return;
            }
            if (sendStr.IsNullOrEmpty()) return;
           
            byte[] sendBuffer;
            if (ckb_send_hex.Checked)
            {
                sendBuffer = sendStr.HexStringToByte();
            }
            else
            {
                sendBuffer = Encoding.Default.GetBytes(sendStr);
            }
            tcpClient.Send(sendBuffer);
            if (ckb_send_autoclean.Checked)
            {
                txt_send.Text = "";
            }
        }
        #endregion

        #region private

        private void ChangeConnectBtn(bool isConnected)
        {
            if (isConnected)//已连接
            {
                btn_connect.Text = "断 开";
                btn_connect.ForeColor = Color.Black;
            }
            else
            {
                btn_connect.Text = "连 接";
                btn_connect.ForeColor = Color.Red;
            }
        }
        /// <summary>
        /// 接收端输出buffer
        /// </summary>
        /// <param name="buffer"></param>
        private void AppendToRecvText(byte[] buffer)
        {
            if (ckb_recv_hex.Checked)
            {
                AppendToRecvText(buffer.ToHex(" "));
            }
            AppendToRecvText(Encoding.Default.GetString(buffer));
        }
        /// <summary>
        /// 接收端输出string
        /// </summary>
        /// <param name="msg"></param>
        private void AppendToRecvText(string msg)
        {
            if (ckb_recv_stop.Checked) return;
            if (ckb_recv_newline.Checked)
            {
                msg += "\n";
            }
            txt_recv.Text += msg;
        }


        #endregion

       
    }
}
