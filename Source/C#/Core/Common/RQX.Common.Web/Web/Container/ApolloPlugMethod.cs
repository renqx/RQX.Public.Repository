﻿using System.Reflection;

namespace RQX.Common.Web.Web.Container
{
    public class ApolloPlugMethod
    {
        private readonly object _instance = null;
        private readonly MethodInfo _method = null;

        public ApolloPlugMethod(MethodInfo method, object instance)
        {
            _instance = instance;
            _method = method;
        }

        public object Invoke(params object[] param)
        {
            return _method?.Invoke(_instance, param);
        }
    }
}
