﻿using RQX.Common.Web.Core.Dto;
using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;
using System.Collections.Generic;

namespace RQX.Common.Web.Web.Service
{
    public class CurdService<TEntity, TDto, TPagger> : BaseDtoService<TEntity, TDto, TPagger>
        where TEntity : BaseEntity
        where TDto : BaseDto
        where TPagger : IPagger
    {
        #region Init
        public CurdService(BaseDbContext context) : base(context) { }
        #endregion

        /// <summary>
        /// 平台自带-单个新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public override TDto Add(TDto dto)
        {
            return base.Add(dto);
        }
        /// <summary>
        /// 平台自带-单个更新
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public override TDto Update(TDto dto)
        {
            return base.Update(dto);
        }
        /// <summary>
        /// 平台自带-单个删除
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public override TDto Remove(int primaryKey)
        {
            return base.Remove(primaryKey);
        }
        /// <summary>
        /// 平台自带-单个详情
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public override TDto GetInfo(int primaryKey)
        {
            return base.GetInfo(primaryKey);
        }
        /// <summary>
        /// 平台自带-全部获取
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<TDto> GetAll()
        {
            return base.GetAll();
        }
        /// <summary>
        /// 平台自带-分页查询
        /// </summary>
        /// <param name="pagger"></param>
        /// <returns></returns>
        public override BasePaggerBack GetList(TPagger pagger)
        {
            return base.GetList(pagger);
        }
    }
}
