﻿using RQX.Common.Web.Core.Dto;
using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Pagger;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RQX.Common.Web.Web.Service.Interface
{
    public interface IDtoService<TEntity, TDto, TPagger>
        where TEntity : BaseEntity
        where TDto : BaseDto
        where TPagger : IPagger
    {
        #region add
        TDto Add(TDto dto);
        IEnumerable<TDto> AddList(IEnumerable<TDto> dtos);
        #endregion

        #region update
        TDto Update(TDto dto);
        TDto Update(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, TDto dto);
        IEnumerable<TDto> UpdateList(IEnumerable<TDto> dtos);
        IEnumerable<TDto> UpdateList(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, IEnumerable<TDto> dtos);
        #endregion

        #region remove
        TDto Remove(int primaryKey);
        IEnumerable<TDto> RemoveList(params int[] primaryKeys);
        IEnumerable<TDto> Remove(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, params int[] primaryKeys);
        #endregion

        #region get
        TDto GetInfo(int primaryKey);
        IEnumerable<TDto> GetInfoList(params int[] primaryKeys);
        IEnumerable<TDto> GetAll();
        BasePaggerBack GetList(TPagger pagger);
        BasePaggerBack GetList(Func<IQueryable<TEntity>, IQueryable<TEntity>> func, TPagger pagger);
        #endregion
    }
}
