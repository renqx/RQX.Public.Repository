﻿using System;

namespace RQX.Common.Web.Core.Interface
{
    /// <summary>
    /// 包含更新的信息
    /// </summary>
    public interface IUpdateUser : ICreateUser
    {
        /// <summary>
        /// 更新者ID
        /// </summary>
        int? update_user_id { get; set; }
        /// <summary>
        /// 更新者名字
        /// </summary>
        string? update_user_name { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        DateTime? update_time { get; set; }
    }
}
