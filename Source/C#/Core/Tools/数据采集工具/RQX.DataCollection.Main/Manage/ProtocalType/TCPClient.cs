﻿using Microsoft.Extensions.Configuration;
using RQX.Common.Core.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Manage.ProtocalType
{
    /// <summary>
    /// TCP客户端
    /// </summary>
    public class TCPClient
    {
        /// <summary>
        /// 目标IP
        /// </summary>
        public string TargetIP { get; set; }
        /// <summary>
        /// 目标端口
        /// </summary>
        public int TargetPort { get; set; }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="path"></param>
        public void Save(IConfigurationSection section)
        {
            section.SetValue($"TargetIP", TargetIP);
            section.SetValue($"TargetPort", TargetPort);
        }
        /// <summary>
        /// 获取参数字符串
        /// </summary>
        /// <returns></returns>
        public string GetParamStr() => $"{TargetIP}:{TargetPort}";
    }
}
