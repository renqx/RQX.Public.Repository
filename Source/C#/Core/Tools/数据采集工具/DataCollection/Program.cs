﻿using RQX.Common.Core.Config;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace DataCollection
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigUtils.Build();
            var name = AppDomain.CurrentDomain.FriendlyName;
            //name = name.Substring(0, name.LastIndexOf('.'));
            var pList = Process.GetProcessesByName(name);
            if (pList.Count() > 1)
            {
                return;
            }
            Application.Run(new MainForm());
        }
    }
}
