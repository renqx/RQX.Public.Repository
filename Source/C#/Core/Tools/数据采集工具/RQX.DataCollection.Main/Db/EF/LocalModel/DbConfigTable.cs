﻿using RQX.Common.Core.Extension;
using RQX.DataCollection.Main.Db.Local;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.Db.EF.LocalModel
{
    /// <summary>
    /// 数据库配置
    /// </summary>
    public class DbConfigTable
    {
        public string DbType { get; set; }
        public string ConnectStr { get; set; }
        public string IP { get; set; }
        public string Port { get; set; }
        public string Instance { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string DbName { get; set; }

        public static void CheckTableExists()
        {
            SqliteDbHelper helper = new SqliteDbHelper();
            var flag = helper.ExcuteSingleResult($"select 1 from sqlite_master where type = 'table' and name = 'DbConfig'");
            if (flag.IsNull())
            {
                helper.ExcuteSqlNoQuery($@"create table DbConfig (
                    DbType TEXT, 
                    IP TEXT, 
                    Port TEXT, 
                    Instance TEXT, 
                    Account TEXT, 
                    Password TEXT, 
                    DbName TEXT, 
                    ConnectStr TEXT)");
            }
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        public static List<DbConfigTable> GetList()
        {
            SqliteDbHelper helper = new SqliteDbHelper();
            var list = helper.ExcuteList($"select * from DbConfig", (dr) =>
            {
                return new DbConfigTable()
                {
                    DbType = dr["DbType"].ToString(),
                    IP = dr["IP"].ToString(),
                    Port = dr["Port"].ToString(),
                    Instance = dr["Instance"].ToString(),
                    Account = dr["Account"].ToString(),
                    Password = dr["Password"].ToString(),
                    DbName = dr["DbName"].ToString(),
                    ConnectStr = dr["ConnectStr"].ToString(),
                };
            });
            return list.ToList();
        }
        /// <summary>
        /// 获取单个信息
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static DbConfigTable GetItem(string typeName)
        {
            SqliteDbHelper helper = new SqliteDbHelper();
            var item = helper.ExcuteSingleObj($"select * from DbConfig where DbType='{typeName}'", (dr) =>
            {
                return new DbConfigTable()
                {
                    DbType = dr["DbType"].ToString(),
                    IP = dr["IP"].ToString(),
                    Port = dr["Port"].ToString(),
                    Instance = dr["Instance"].ToString(),
                    Account = dr["Account"].ToString(),
                    Password = dr["Password"].ToString(),
                    DbName = dr["DbName"].ToString(),
                    ConnectStr = dr["ConnectStr"].ToString(),
                };
            });
            return item;
        }
        /// <summary>
        /// 保存配置
        /// </summary>
        public int Save()
        {
            SqliteDbHelper helper = new SqliteDbHelper();
            var flag = helper.ExcuteSingleResult($"SELECT 1 FROM DbConfig WHERE DbType='{DbType}'");
            if (flag.IsNull())
            {
                return helper.ExcuteSqlNoQuery($@"INSERT INTO DbConfig (DbType,IP,Port,Instance,Account,Password,DbName,ConnectStr) 
                    values ('{DbType}','{IP}','{Port}','{Instance}','{Account}','{Password}','{DbName}','{ConnectStr}') ");
            }
            else
            {
                return helper.ExcuteSqlNoQuery($@"UPDATE DbConfig SET 
                    IP='{IP}',
                    Port='{Port}',
                    Instance='{Instance}',
                    Account='{Account}',
                    Password='{Password}',
                    DbName='{DbName}',
                    ConnectStr='{ConnectStr}'
                    WHERE 'DbType'='{DbType}' ");
            }
        }

    }
}
