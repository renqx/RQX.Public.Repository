﻿
using RQX.Common.Web.Core.Dto;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 基础树形Dto
    /// </summary>
    public class BaseTreeDto : BaseDto
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
