﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 排序接口
    /// </summary>
    public interface IBaseSort
    {
        /// <summary>
        /// 排序字段
        /// </summary>
        string Sort { get; set; }
        /// <summary>
        /// 排序方式，0升序，1降序
        /// </summary>
        SortType SortType { get; set; }
    }
}
