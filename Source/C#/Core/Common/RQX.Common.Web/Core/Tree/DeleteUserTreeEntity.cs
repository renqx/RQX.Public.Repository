﻿
using RQX.Common.Web.Core.Entity;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带删除者的树形实体
    /// </summary>
    public class DeleteUserTreeEntity : DeleteUserEntity
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
