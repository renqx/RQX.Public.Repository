﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    public interface IIocCollection
    {
        /// <summary>
        /// 开始
        /// </summary>
        void Start(bool isUseReStart = true);
        /// <summary>
        /// 解析后的数据处理事件
        /// </summary>
        Action<Dictionary<string, string>> DealDataHandler { get; set; }
        /// <summary>
        /// 显示信息事件
        /// </summary>
        Action<string> ShowMsgHandler { get; set; }
        /// <summary>
        /// 释放
        /// </summary>
        void Dispose();
        /// <summary>
        /// 重启
        /// </summary>
        void ReStart();
    }
}
