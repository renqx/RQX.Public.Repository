﻿using Microsoft.EntityFrameworkCore;
using RQX.Common.Core.Db.EF.Context;
using RQX.DataCollection.Main.EF.Db.Model;

namespace RQX.DataCollection.Main.Db.EF
{
    public class CollectionDbContext : BaseDbContext
    {
        public DbSet<CollectionData> CollectionDatas { get; set; }
    }
}
