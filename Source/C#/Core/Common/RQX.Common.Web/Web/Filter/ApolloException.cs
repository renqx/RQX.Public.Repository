﻿using System;

namespace RQX.Common.Web.Web.Filter
{
    /// <summary>
    /// 系统异常
    /// </summary>
    public class ApolloException : Exception
    {
        public readonly ApolloExceptionType ExceptionType;
        public ApolloException(string message) : this(ApolloExceptionType.异常提示, message) { }
        public ApolloException(ApolloExceptionType type) : this(type, type.ToString()) { }
        public ApolloException(ApolloExceptionType type, string message = null) : this(type, message, null) { }
        public ApolloException(ApolloExceptionType type, string msg = null, Exception ex = null) : base(msg, ex)
        {
            ExceptionType = type;
        }
    }
}
