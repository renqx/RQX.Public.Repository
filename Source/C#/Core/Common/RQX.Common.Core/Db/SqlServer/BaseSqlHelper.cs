﻿using RQX.Common.Core.Db.Base;
using RQX.Common.Core.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.SqlServer
{
    public class BaseSqlHelper : BaseDbHelper<SqlConnection, SqlCommand>
    {
        #region Init
        public BaseSqlHelper() : base()
        {
        }

        public BaseSqlHelper(string connStr) : base(connStr)
        {
        }
        protected override SqlConnection CreateDbConnection(string connectionStr)
        {
            return new SqlConnection(connectionStr);
        }
        #endregion
    }
}
