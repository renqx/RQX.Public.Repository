﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace RQX.Common.Web.Web.Swagger
{
    /// <summary>
    /// 文档标签排序
    /// </summary>
    public class SwaggerDocTagOrder : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = swaggerDoc.Tags.OrderBy(tag => tag.Name).ToList();
        }
    }
}
