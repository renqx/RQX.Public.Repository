using RQX.Common.Core.Config;
using RQX.Common.Core.Extension;
using RQX.Common.Core.File;
using RQX.DataCollection.Main.Db.EF.LocalModel;
using RQX.DataCollection.Main.Db.Local;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQX.DataCollection.Main
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FileUtils.CheckOrCreateDirPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"));
            //DbConfigTable.CheckTableExists();
            ConfigUtils.Build();
            Application.Run(new Main());
        }
    }
}
