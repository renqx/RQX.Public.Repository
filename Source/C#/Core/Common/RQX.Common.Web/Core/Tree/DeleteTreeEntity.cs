﻿using RQX.Common.Web.Core.Entity;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带软删除的树形实体
    /// </summary>
    public class DeleteTreeEntity : DeleteEntity
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
