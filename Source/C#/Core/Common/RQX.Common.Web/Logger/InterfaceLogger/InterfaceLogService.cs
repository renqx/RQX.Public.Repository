﻿using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.Container;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Service;

namespace RQX.Common.Web.Logger.InterfaceLogger
{
    /// <summary>
    /// 接口日志
    /// </summary>
    [IocInjection]
    [IgnoreApiService]
    public class InterfaceLogService : BaseLoggerService<InterfaceLog, InterfaceLogDto, BasePagger>
    {
        public InterfaceLogService(CommonDbContext dbContext) : base(dbContext)
        {
        }
    }
}
