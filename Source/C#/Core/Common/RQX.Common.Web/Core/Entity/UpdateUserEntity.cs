﻿using RQX.Common.Web.Core.Interface;
using RQX.Common.Web.Core.Valid;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RQX.Common.Web.Core.Entity
{
    /// <summary>
    /// 带修改者的实体
    /// </summary>
    public class UpdateUserEntity : CreateUserEntity, IUpdateUser
    {
        /// <summary>
        /// 更新者ID
        /// </summary>
        public int? update_user_id { get; set; }
        /// <summary>
        /// 更新者名字
        /// </summary>
        [ApolloMaxLength(50)]
        public string? update_user_name { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime? update_time { get; set; }
    }
}
