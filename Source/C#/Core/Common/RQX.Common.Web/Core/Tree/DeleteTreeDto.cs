﻿using RQX.Common.Web.Core.Dto;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带软删除的树形传输实体
    /// </summary>
    public class DeleteTreeDto : DeleteDto
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
