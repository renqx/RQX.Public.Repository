﻿using RQX.Common.Web.Logger.ExceptionLogger;
using RQX.Common.Web.Logger.OperationLogger;
using RQX.Common.Web.Web.Container;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Filter;
using RQX.Common.Web.Web.Service;

namespace RQX.Common.Web.Logger
{
    [IocInjection]
    [IgnoreApiService]
    public class LoggerService : ApiService
    {
        [AutoInjection]
        public OperationLogService _operationLogService { get; set; }
        [AutoInjection]
        private ExceptionLogService _exceptionLogService { get; set; }

        public LoggerService(AutoInjectionService autoInjection)
        {
            autoInjection.AutoInjection(this);
        }
        //public LoggerService(OperationLogService operationLogService, ExceptionLogService exceptionLogService)
        //{
        //    _operationLogService = operationLogService;
        //    _exceptionLogService = exceptionLogService;
        //}

        /// <summary>
        /// 写操作日志   
        /// </summary>
        /// <param name="msg"></param>
        public void WriteLog(string msg)
        {
            _operationLogService.Add(new OperationLog(msg));
        }

        /// <summary>
        /// 写异常日志
        /// </summary>
        /// <param name="exception"></param>
        public void WriteExceptionLog(ApolloException exception)
        {
            _exceptionLogService.Add(new ExceptionLog(exception));
        }
    }
}
