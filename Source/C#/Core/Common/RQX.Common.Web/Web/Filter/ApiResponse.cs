﻿namespace RQX.Common.Web.Web.Filter
{
    public class ApiResponse
    {
        public ApiResponse() { }
        public ApiResponse(int code) : this(code, new { }) { }
        public ApiResponse(int code, object content) : this(code, "", content) { }
        public ApiResponse(int code, string msg, object content)
        {
            Code = code;
            Content = content;
            Message = msg;
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public object Content { get; set; }
    }
}
