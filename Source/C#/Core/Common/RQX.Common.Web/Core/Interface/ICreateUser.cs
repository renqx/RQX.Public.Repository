﻿using System;

namespace RQX.Common.Web.Core.Interface
{
    /// <summary>
    /// 包含添加的信息
    /// </summary>
    public interface ICreateUser
    {
        /// <summary>
        /// 创建人ID
        /// </summary>
        int create_user_id { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        string create_user_name { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime create_time { get; set; }
    }
}
