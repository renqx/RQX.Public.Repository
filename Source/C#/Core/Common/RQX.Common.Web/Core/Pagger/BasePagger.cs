﻿namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 基础分页类
    /// </summary>
    public class BasePagger : IPagger
    {
        /// <summary>
        /// 页数，第一页从1开始，不传默认1
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 每页数据量，不传默认50
        /// </summary>
        public int PageSize { get; set; } = 25;
        /// <summary>
        /// 过滤条件
        /// </summary>
        public string Filter { get; set; }
        /// <summary>
        /// 排序，不传默认为id并且降序排序
        /// </summary>
        public string Sort { get; set; } = "id";
        /// <summary>
        /// 排序方式，0降序，1升序，不传默认降序
        /// </summary>
        public SortType SortType { get; set; } = SortType.DESC;
        /// <summary>
        /// 获取分页跳过的数据数量
        /// </summary>
        /// <returns></returns>
        public int GetSkipNum()
        {
            return (PageIndex - 1) * PageSize;
        }
    }
}
