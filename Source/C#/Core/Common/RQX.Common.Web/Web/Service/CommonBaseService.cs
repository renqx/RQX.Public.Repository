﻿using Microsoft.EntityFrameworkCore.Storage;
using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Repository;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace RQX.Common.Web.Web.Service
{
    [IgnoreApiService]
    public class CommonBaseService<TEntity> : ApiService, IBaseService<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IRepository<TEntity> _Repository;

        #region Init
        public CommonBaseService(BaseDbContext context) : base()
        {
            _Repository = new BaseRepository<TEntity>(context);
        }
        #endregion

        #region BaseMethod
        /// <summary>
        /// 获取仓储
        /// </summary>
        /// <returns></returns>
        protected internal IRepository<TEntity> GetRepository() => _Repository;
        /// <summary>
        /// 仓储包含
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="thenInclude"></param>
        public void Include(Expression<Func<TEntity, object>> expression, params Expression<Func<object, object>>[] thenInclude)
        {
            GetRepository().Include(expression, thenInclude);
        }
        /// <summary>
        /// 提交
        /// </summary>
        public void Commit()
        {
            GetRepository().Commit();
        }
        /// <summary>
        /// where过滤
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression)
        {
            return GetRepository().GetAll().Where(expression);
        }
        /// <summary>
        /// where条件过滤
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IQueryable<TEntity> WhereIf(bool isRun, Expression<Func<TEntity, bool>> expression)
        {
            if (isRun)
            {
                return Where(expression);
            }
            else
            {
                return GetRepository().GetAll();
            }
        }
        /// <summary>
        /// 数据库条件过滤
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> DoFilter(IEnumerable<TEntity> entities, Func<IQueryable<TEntity>, IQueryable<TEntity>> func)
        {
            var beforeListIds = entities.Select(k => k.id);
            var beforeList = GetRepository().Where(m => beforeListIds.Contains(m.id));
            var afterFilterList = func(beforeList);
            var afterListIds = afterFilterList.Select(k => k.id);
            var afterList = entities.Where(o => afterListIds.Contains(o.id));
            return afterList;
        }
        /// <summary>
        /// 事务
        /// </summary>
        /// <returns></returns>
        public IDbContextTransaction GetBeginTransaction()
        {
            return GetRepository().GetBeginTransaction();
        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="primarykeyList"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> Find(params int[] primarykeyList)
        {
            return GetRepository().Find(primarykeyList);
        }
        #endregion
    }
}
