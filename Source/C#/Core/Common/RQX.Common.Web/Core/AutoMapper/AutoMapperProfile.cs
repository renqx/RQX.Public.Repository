﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RQX.Common.Web.Core.AutoMapper
{
    /// <summary>
    /// 自动映射
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        private static readonly Dictionary<string, Type> dic = new Dictionary<string, Type>();
        public AutoMapperProfile()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var types = assembly.GetTypes().Where(k => k.IsDefined(typeof(AutoMapperAttribute), true));
                AddType(types);
            }
        }

        private void AddType(IEnumerable<Type> types)
        {
            foreach (var type in types)
            {
                if (AutoMapperCurrentType.ContainsKey(type.FullName))
                {
                    continue;
                }
                else
                {
                    var attributes = type.GetCustomAttributes(typeof(AutoMapperAttribute), true);
                    var targetTypeFullName = ((AutoMapperAttribute)attributes.FirstOrDefault()).BaseType.FullName;
                    var sourceTypeFullName = type.FullName;
                    var targetType = AutoMapperCurrentType.GetType(targetTypeFullName);
                    var sourceType = AutoMapperCurrentType.GetType(sourceTypeFullName);

                    CreateMap(sourceType, targetType);
                    CreateMap(targetType, sourceType);
                }
            }
        }

        //private void AddMap(Type sourceType, Type targetType)
        //{
        //    var mapInstance = CreateMap(sourceType, targetType);
        //    mapInstance.BeforeMap(BeforeMapAction);
        //}

        //private void BeforeMapAction<TSource,TDestination>(TSource source,TDestination destination)
        //{
        //        if (source is IEnumerable<object> enumerableSource)
        //        {
        //            var t = typeof(TDestination);

        //            var sourceCurrentType = enumerableSource.AsQueryable().ElementType.FullName.GetCurrentType();
        //            var resultObj = typeof(TDestination).Assembly.CreateInstance(typeof(TDestination).FullName);
        //            var destinationCurrentType = (resultObj as IEnumerable<object>).AsQueryable().ElementType.FullName.GetCurrentType();

        //            List<object> resultList = new List<object>();
        //            enumerableSource.ForEach(item =>
        //            {
        //                var sourceObj = sourceCurrentType.Assembly.CreateInstance(sourceCurrentType.FullName);
        //                item.CopyTo(sourceObj);
        //                //var destinationObj = mapper.Map(sourceObj, sourceCurrentType, destinationCurrentType);
        //                //var resultObj = typeof(TDestination).Assembly.CreateInstance(destinationCurrentType.FullName);
        //                //destinationObj.CopyTo(resultObj);
        //                //resultList.Add(resultObj);
        //            });
        //        }
        //        else
        //        {
        //            var sourceCurrentType = source.GetType().FullName.GetCurrentType();
        //            var destinationCurrentType = typeof(TDestination).FullName.GetCurrentType();

        //            var sourceObj = sourceCurrentType.Assembly.CreateInstance(sourceCurrentType.FullName);
        //            source.CopyTo(sourceObj);
        //            //var destinationObj = mapper.Map(sourceObj, sourceCurrentType, destinationCurrentType);
        //            //var resultObj = typeof(TDestination).Assembly.CreateInstance(destinationCurrentType.FullName);
        //            //destinationObj.CopyTo(resultObj);
        //        }
        //}

    }
}
