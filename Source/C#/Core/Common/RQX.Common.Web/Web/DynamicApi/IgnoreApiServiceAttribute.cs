﻿using System;

namespace RQX.Common.Web.Web.DynamicApi
{
    /// <summary>
    /// 忽视动态API特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false)]
    public class IgnoreApiServiceAttribute : Attribute
    {
    }
}
