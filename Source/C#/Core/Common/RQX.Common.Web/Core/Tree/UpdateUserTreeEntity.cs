﻿using RQX.Common.Web.Core.Entity;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带修改者的树形实体
    /// </summary>
    public class UpdateUserTreeEntity : UpdateUserEntity
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
