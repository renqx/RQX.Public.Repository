﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图片转Base64字符串工具
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txt_file_TextChanged(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            dialog.Filter = "图片文件|(*.png)|所有程序(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txt_file.Text = dialog.FileName;
            }
        }

        private void btn_change_Click(object sender, EventArgs e)
        {
            var filePath = txt_file.Text;
            if (File.Exists(filePath))
            {
                Bitmap bmp = new Bitmap(filePath);

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] arr = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr, 0, (int)ms.Length);
                ms.Close();
                var str = Convert.ToBase64String(arr);
                rtxt_content.Text = str;
            }
        }
    }
}
