﻿using System;

namespace RQX.Common.Web.Web.Container
{
    /// <summary>
    /// 需要注入的类型特性
    /// </summary>
    public class AutoInjectionAttribute : Attribute
    {
    }
}
