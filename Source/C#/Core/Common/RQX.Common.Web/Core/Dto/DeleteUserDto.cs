﻿using RQX.Common.Web.Core.Interface;
using RQX.Common.Web.Core.Valid;
using System;

namespace RQX.Common.Web.Core.Dto
{
    /// <summary>
    /// 带删除者的传输实体
    /// </summary>
    public class DeleteUserDto : UpdateUserDto, IDeleteUser, IDelete
    {
        /// <summary>
        /// 删除者ID
        /// </summary>
        public int? delete_user_id { get; set; }
        /// <summary>
        /// 删除者名字
        /// </summary>
        [ApolloMaxLength(50)]
        public string? delete_user_name { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        public DateTime? delete_time { get; set; }
        /// <summary>
        /// 能否被删除
        /// </summary>
        public bool can_dele { get; set; } = true;
        /// <summary>
        /// 是否已删除
        /// </summary>
        public bool is_remove { get; set; } = false;
    }
}
