﻿using RQX.Common.Framework.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    public abstract class AbstractTcpClientDelegateCollection : AbstractTcpClientCollection
    {
        public AbstractTcpClientDelegateCollection(StructureModel<TcpSocketClient> structureModel) : base(structureModel)
        {
            _conn = null;
        }

        protected override void Send(byte[] buffer) { }
    }
}
