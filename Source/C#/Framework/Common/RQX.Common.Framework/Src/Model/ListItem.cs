﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Model
{
    public class ListItem
    {
        /// <summary>
        /// 显示值
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 实际值
        /// </summary>
        public int Value { get; set; }

        public ListItem() : this(0, "") { }
        public ListItem(int value, string text)
        {
            this.Value = value;
            this.Text = text;
        }
        public ListItem(int value) : this(value, value.ToString()) { }
    }
}
