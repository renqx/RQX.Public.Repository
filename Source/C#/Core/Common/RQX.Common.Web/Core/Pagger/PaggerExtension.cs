﻿using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Tools;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 分页方法扩展
    /// </summary>
    public static class PaggerExtension
    {
        /// <summary>
        /// 排序
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entities"></param>
        /// <param name="baseSort"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> entities, IBaseSort baseSort)
            where TEntity : BaseEntity
        {
            if (baseSort.Sort.IsNullOrEmpty())
            {
                return entities.OrderByDescending(x => x.id);
            }
            else
            {
                var param = Expression.Parameter(typeof(TEntity));
                var prop = Expression.Property(param, baseSort.Sort);
                var exp = Expression.Lambda(prop, param);
                string method;
                switch (baseSort.SortType)
                {
                    case SortType.ASC: method = "OrderBy"; break;
                    case SortType.DESC:
                    default: method = "OrderByDescending"; break;
                }
                Type[] types = new Type[] { entities.ElementType, exp.Body.Type };
                var mce = Expression.Call(typeof(Queryable), method, types, entities.Expression, exp);
                return entities.Provider.CreateQuery<TEntity>(mce);
            }
        }
    }
}
