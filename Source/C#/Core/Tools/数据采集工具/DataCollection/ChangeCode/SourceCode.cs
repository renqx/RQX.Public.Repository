﻿namespace DataCollection.ChangeCode
{
    public class SourceCode
    {
        /// <summary>
        /// 插件名称
        /// </summary>
        public string PlugName { get; set; }
        /// <summary>
        /// 采集值名称
        /// </summary>
        public string CollectionName { get; set; }
    }
}
