﻿using DataCollection.Manage.ProtocalType;
using RQX.Common.Core.Config;
using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace DataCollection.Manage
{
    /// <summary>
    /// 设备信息
    /// </summary>
    public class DeviceInfo
    {
        #region 属性
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string GUID { get; set; }
        /// <summary>
        /// 终端设备名称
        /// </summary>
        public string DeviceName { get; set; }
        /// <summary>
        /// 采集插件名称
        /// </summary>
        public string PlugName { get; set; }
        /// <summary>
        /// 协议类型
        /// </summary>
        public ProType ProType { get; set; }

        public COM COM { get; set; }
        public TCPClient TCPClient { get; set; }
        public TCPServer TCPServer { get; set; }
        public UDP UDP { get; set; }
        #endregion
        #region Init
        public static List<DeviceInfo> list;
        static DeviceInfo()
        {
            list = ConfigUtils.GetConfigurationRoot().GetSection("Device").BuildClass<List<DeviceInfo>>();
        }
        public DeviceInfo() { }
        public DeviceInfo(string guid)
        {
            GUID = guid;
        }
        public Type GetDeviceClassType()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins", PlugName);
            //加载插件列表
            var assembly = Assembly.LoadFrom(path);
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (!type.IsAbstract && type.IsPublic && typeof(IIocCollection).IsAssignableFrom(type))
                {
                    return type;
                }
            }
            return null;
        }
        /// <summary>
        /// 通过guid获取一个DeviceInfo，不存在则新增一个
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static DeviceInfo GetDevice(string guid)
        {
            var info = list.Find(k => k.GUID.Equals(guid));
            if (info == null)
            {
                info = new DeviceInfo(guid);
                list.Add(info);
            }
            return info;
        }
        /// <summary>
        /// 保存当前配置到配置文件中
        /// </summary>
        public static void Save()
        {
            var root = ConfigUtils.GetConfigurationRoot();
            var deviceRoot = root.GetSection("Device");
            var allList = deviceRoot.GetAllConfigDic();
            //移除原有数据
            allList.ForEach(item =>
            {
                root.SetValue(item.Key, null);
            });
            //添加当前数据
            for (var i = 0; i < list.Count; i++)
            {
                var device = deviceRoot.GetSection(i.ToString());

                device.SetValue($"GUID", list[i].GUID);
                device.SetValue($"DeviceName", list[i].DeviceName);
                device.SetValue($"PlugName", list[i].PlugName);
                device.SetValue($"ProType", list[i].ProType);
                switch (list[i].ProType)
                {
                    case ProType.UDP:
                        list[i].UDP.Save(device.GetSection("UDP"));
                        break;
                    case ProType.TCPClient:
                        list[i].TCPClient.Save(device.GetSection("TCPClient"));
                        break;
                    case ProType.TCPServer:
                        list[i].TCPServer.Save(device.GetSection("TCPServer"));
                        break;
                    case ProType.COM:
                        list[i].COM.Save(device.GetSection("COM"));
                        break;
                }
            }
            ConfigUtils.SaveToFile();
        }
        #endregion


    }
}
