﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maquet_Flow_i
{
    class Program
    {
        private static string[] BASE_CODE = new string[] { "100", "102", "104", "105", "106", "107", "108", "118", "124" };
        //private static string[] BASE_CODE = new string[] { "110"};//  };
        private static string[] BASE_NAME = new string[] { "RR", "VTe", "MVe", "Ppeak", "Pmean", "Pplat", "PEEP", "FiO2", "EtCO2" };
        //private static string[] BASE_NAME = new string[] { "I:E" };//,  };

        static void Main(string[] args)
        {
            //var cleanStr = "52435459314304";
            //var setIE = "5344414442313130363004";
            //var getIE = "5241444142353404";
            StructureModel<MyCom> model = new StructureModel<MyCom>
            {
                Conn = new MyCom(38400, "COM3", 8, StopBits.One, Parity.Even)
            };
            Flow_i flow_i = new Flow_i(model);
            flow_i.DealDataHandler = (dic) =>
            {
                dic.ForEach(item =>
                {
                    Console.WriteLine($"{item.Key}\t{item.Value}");
                });
            };
            flow_i.Start();
            Console.ReadKey();
        }
    }
}
