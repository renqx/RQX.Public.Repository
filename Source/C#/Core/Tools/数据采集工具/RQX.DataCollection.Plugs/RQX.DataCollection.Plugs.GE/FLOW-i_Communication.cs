﻿using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Plugs
{
    /// <summary>
    /// 迈柯唯
    /// </summary>
    public class FLOW_i_Communication : AbstractComCollection
    {
        private const byte EOT = 0x04;
        private const byte ESC = 0x1B;

        public FLOW_i_Communication(StructureModel<MyCom> conn) : base(conn) { }

        protected override Dictionary<string, double> AnalysisData(byte[] buffer)
        {
            throw new NotImplementedException();
        }

        protected override StepType CheckRecvMsgType(string lineStr)
        {
            throw new NotImplementedException();
        }

        protected override void CircleSend()
        {
            throw new NotImplementedException();
        }

        protected override void Connect(StepType stepNum)
        {
            throw new NotImplementedException();
        }

        protected override string CutToOneMsg(StringBuilder sb)
        {
            throw new NotImplementedException();
        }
    }
}
