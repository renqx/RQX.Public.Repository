﻿
using RQX.Common.Web.Core.Entity;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 基础树形实体
    /// </summary>
    public class BaseTreeEntity : BaseEntity
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
