﻿using System;

namespace RQX.Common.Web.Authority
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false)]
    public class NoLoginAttribute : Attribute
    {

    }
}
