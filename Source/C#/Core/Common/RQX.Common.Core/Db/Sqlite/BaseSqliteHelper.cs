﻿using Microsoft.Data.Sqlite;
using RQX.Common.Core.Db.Base;
using RQX.Common.Core.Extension;
using RQX.Common.Core.File;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Db.Sqlite
{
    public class BaseSqliteHelper : BaseDbHelper<SqliteConnection, SqliteCommand>
    {
        #region Init
        public BaseSqliteHelper() : base()
        {
        }

        public BaseSqliteHelper(string connStr) : base(connStr)
        {
        }
        /// <summary>
        /// 创建数据库连接的实现
        /// </summary>
        /// <param name="connectionStr"></param>
        /// <returns></returns>
        protected override SqliteConnection CreateDbConnection(string connectionStr)
        {
            return new SqliteConnection(connectionStr);
        }
        #endregion
    }
}
