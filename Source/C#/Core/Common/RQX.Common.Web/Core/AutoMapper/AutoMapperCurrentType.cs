﻿using RQX.Common.Web.Tools;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace RQX.Common.Web.Core.AutoMapper
{
    /// <summary>
    /// 自动映射，保存当前
    /// </summary>
    public static class AutoMapperCurrentType
    {
        private static readonly ConcurrentDictionary<string, KeyValuePair<Type, List<string>>> TypeDic = new ConcurrentDictionary<string, KeyValuePair<Type, List<string>>>();

        /// <summary>
        /// 获取type
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        public static Type GetType(string typeFullName)
        {
            if (ContainsKey(typeFullName))
            {
                return TypeDic[typeFullName].Key;
            }
            else
            {
                return AddType(typeFullName).Key;
            }
        }
        /// <summary>
        /// 获取反射字段列表
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        public static List<string> GetPropertyList(string typeFullName)
        {
            if (ContainsKey(typeFullName))
            {
                return TypeDic[typeFullName].Value;
            }
            else
            {
                return AddType(typeFullName).Value;
            }
        }

        /// <summary>
        /// 插件赋值
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void PlugCopyTo<TDestination>(this object source, TDestination target)
        {
            var sourceType = source.GetType();
            var targetType = target.GetType();
            var propertyList = GetPropertyList(sourceType.FullName);
            foreach (var fieldName in propertyList)
            {
                var sourceItem = sourceType.GetProperty(fieldName);
                var targetItem = targetType.GetProperty(fieldName);
                if (sourceItem == null ||
                    targetItem == null ||
                    !sourceItem.CanRead ||
                    !targetItem.CanWrite ||
                    (sourceItem.PropertyType.Name.Equals("string") && sourceItem.PropertyType.GetInterface(typeof(IEnumerable<>).Name) != null)
                    ) continue;
                var value = sourceItem.GetValue(source);
                if (value == null) continue;
                targetItem.SetValue(target, value);
            }
        }
        /// <summary>
        /// 插件赋值列表
        /// </summary>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void PlugListCopyTo<TDestination>(this IEnumerable<object> source, List<TDestination> target) where TDestination : new()
        {
            var tmpList = target.ToList();
            var targetType = target.AsQueryable().ElementType;
            source.ForEach(item =>
            {
                var targetItem = new TDestination();
                item.PlugCopyTo(targetItem);
                target.Add(targetItem);
            });
        }


        /// <summary>
        /// 判断是否包含某个类
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        public static bool ContainsKey(string typeFullName)
        {
            return TypeDic.ContainsKey(typeFullName);
        }

        /// <summary>
        /// 添加currentType
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        private static KeyValuePair<Type, List<string>> AddType(string typeFullName)
        {
            var currentType = typeFullName.GetCurrentType();
            var propertyList = new List<string>();
            foreach (var property in currentType.GetProperties())
            {
                propertyList.Add(property.Name);
            }
            var kvp = new KeyValuePair<Type, List<string>>(currentType, propertyList);
            TypeDic.TryAdd(typeFullName, kvp);
            return kvp;
        }


    }
}
