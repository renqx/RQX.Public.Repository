﻿namespace RQX.Common.Web.Core.Dto
{
    public class BaseDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public int id { get; set; }
    }
}
