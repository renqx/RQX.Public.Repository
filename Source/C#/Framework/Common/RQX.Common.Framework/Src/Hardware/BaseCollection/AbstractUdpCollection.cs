﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    /// <summary>
    /// UDP采集抽象类
    /// </summary>
    public abstract class AbstractUdpCollection : AbstractBaseCollection<UdpSocket>
    {
        public AbstractUdpCollection(StructureModel<UdpSocket> structureModel) : base(structureModel)
        {
            _conn.BindRecvHandler(RecvData);
        }

        public override void Dispose()
        {
            _conn.Close();
            base.Dispose();
        }

        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn.Send(buffer);
        }
    }
}
