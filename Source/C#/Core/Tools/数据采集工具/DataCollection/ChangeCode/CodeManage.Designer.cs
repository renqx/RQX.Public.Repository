﻿using System.Drawing;

namespace DataCollection.ChangeCode
{
    partial class CodeManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeManage));
            this.ddl_device_type = new System.Windows.Forms.ComboBox();
            this.ddl_plug_name = new System.Windows.Forms.ComboBox();
            this.lable1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_change_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // ddl_device_type
            // 
            this.ddl_device_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddl_device_type.FormattingEnabled = true;
            this.ddl_device_type.Location = new System.Drawing.Point(74, 6);
            this.ddl_device_type.Name = "ddl_device_type";
            this.ddl_device_type.Size = new System.Drawing.Size(121, 25);
            this.ddl_device_type.TabIndex = 0;
            this.ddl_device_type.SelectedIndexChanged += new System.EventHandler(this.ddl_device_type_SelectedIndexChanged);
            // 
            // ddl_plug_name
            // 
            this.ddl_plug_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddl_plug_name.FormattingEnabled = true;
            this.ddl_plug_name.Location = new System.Drawing.Point(256, 6);
            this.ddl_plug_name.Name = "ddl_plug_name";
            this.ddl_plug_name.Size = new System.Drawing.Size(161, 25);
            this.ddl_plug_name.TabIndex = 1;
            this.ddl_plug_name.SelectedIndexChanged += new System.EventHandler(this.ddl_plug_name_SelectedIndexChanged);
            // 
            // lable1
            // 
            this.lable1.AutoSize = true;
            this.lable1.Location = new System.Drawing.Point(12, 9);
            this.lable1.Name = "lable1";
            this.lable1.Size = new System.Drawing.Size(56, 17);
            this.lable1.TabIndex = 4;
            this.lable1.Text = "设备类型";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "插件";
            // 
            // dgv_code
            // 
            this.dgv_code.HeaderText = "编号";
            this.dgv_code.Name = "dgv_code";
            this.dgv_code.ReadOnly = true;
            this.dgv_code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_name
            // 
            this.dgv_name.HeaderText = "名称";
            this.dgv_name.Name = "dgv_name";
            this.dgv_name.ReadOnly = true;
            this.dgv_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_desc
            // 
            this.dgv_desc.HeaderText = "描述";
            this.dgv_desc.Name = "dgv_desc";
            this.dgv_desc.ReadOnly = true;
            this.dgv_desc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgv_desc.Width = 120;
            // 
            // dgv_low
            // 
            this.dgv_low.HeaderText = "下限";
            this.dgv_low.Name = "dgv_low";
            this.dgv_low.ReadOnly = true;
            this.dgv_low.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_high
            // 
            this.dgv_high.HeaderText = "上限";
            this.dgv_high.Name = "dgv_high";
            this.dgv_high.ReadOnly = true;
            this.dgv_high.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgv_change_code
            // 
            this.dgv_change_code.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_change_code.HeaderText = "对码值";
            this.dgv_change_code.Name = "dgv_change_code";
            this.dgv_change_code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_code,
            this.dgv_name,
            this.dgv_desc,
            this.dgv_low,
            this.dgv_high,
            this.dgv_change_code});
            this.grid.Location = new System.Drawing.Point(12, 37);
            this.grid.Name = "grid";
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(831, 480);
            this.grid.TabIndex = 2;
            this.grid.Text = "dataGridView1";
            this.grid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grid_CellBeginEdit);
            this.grid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellEndEdit);
            // 
            // CodeManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 525);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lable1);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.ddl_plug_name);
            this.Controls.Add(this.ddl_device_type);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CodeManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "对码配置";
            this.Load += new System.EventHandler(this.CodeManage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddl_device_type;
        private System.Windows.Forms.ComboBox ddl_plug_name;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Label lable1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_high;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_change_code;
    }
}