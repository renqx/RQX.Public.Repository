﻿namespace RQX.Common.Web.Config
{
    public static class GlobalConfig
    {
        public const string DB_SectionName = "DbConnection";
        public const string DB_ApolloConnectionName = "ApolloConnection";
        public const string LoggerPath = "Logger:LogPath";
        public const string PDFServerIp = "PDFServer:Ip";
        public const string PDFServerPort = "PDFServer:Port";
        public const string PlugDic = "Plug:PlugList";
        public const string PlugPath = "Plug:PlugDirPath";
        public const string LoggerLevel = "Logger:LogLevel";
    }
}
