﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.BaseCollection;
using RQX.Common.Core.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips_MX550
{
    class Program
    {
        private static int Port = 24105;
        private static string IP = "192.168.188.5";


        static void Main(string[] args)
        {
            MX550 mx550 = null;
            mx550 = new MX550(new StructureModel<UdpSocket>()
            {
                Conn = new UdpSocket(IP, Port),
                MaxRecvTime = 30 * 1000,
                Ext1 = IP,
                Ext2 = Port
            });
            mx550?.Start();
            Console.ReadKey();
        }
    }
}
