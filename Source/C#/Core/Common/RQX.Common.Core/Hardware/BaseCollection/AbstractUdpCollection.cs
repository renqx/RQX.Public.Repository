﻿using RQX.Common.Core.Extension;
using RQX.Common.Core.Hardware.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    /// <summary>
    /// UDP采集抽象类
    /// </summary>
    public abstract class AbstractUdpCollection : AbstractBaseCollection<UdpSocket>
    {
        #region 协议默认参数
        public override ProType ProType => ProType.UDP;
        public abstract string IP { get; }
        public abstract int Port { get; }
        public abstract int Port_Local { get; }
        #endregion
        protected AbstractUdpCollection() { }
        protected AbstractUdpCollection(StructureModel<UdpSocket> structureModel) : base(structureModel)
        {
            _conn.BindRecvHandler(RecvData);
        }

        public override void Dispose()
        {
            _conn.Close();
            base.Dispose();
        }

        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn.Send(buffer);
        }
    }
}
