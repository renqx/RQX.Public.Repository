﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.BaseCollection;
using RQX.Common.Framework.Hardware.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_COM
{
    class Program
    {
        static void Main(string[] args)
        {
            ComDemo demo = new ComDemo(new StructureModel<MyCom>()
            {
                Conn = new MyCom("COM1"),
            });
            demo.DealDataHandler = (dic) => dic.ForEach(item => Console.WriteLine($"{item.Key}\t{item.Value}"));
            demo.Start();
            Console.ReadKey();
        }
    }
}
