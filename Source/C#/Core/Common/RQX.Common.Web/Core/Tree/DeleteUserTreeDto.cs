﻿
using RQX.Common.Web.Core.Dto;

namespace RQX.Common.Web.Core.Tree
{
    /// <summary>
    /// 带删除者的树形传输实体
    /// </summary>
    public class DeleteUserTreeDto : DeleteUserDto
    {
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int pid { get; set; }
    }
}
