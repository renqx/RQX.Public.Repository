# 平台开发文档

## 目录<span id="Directory"/>

1. [**QuickStart**](#QuickStart)
2. [**Environment**](#Environment)
3. [**EntityClass**](#EntityClass)
	1. [**Entity**](#Entity)
	1. [**Dto**](#Dto)
	1. [**Attribute**](#Attribute)
	1. [**Pagger**](#Pagger)
4. [**Interface**](#Interface)
5. [**Repository**](#Repository)
6. [**PlugDevelopement**](#PlugDevelopement)
7. [**Attention**](#Attention)

## [QuickStart](#Directory)<span id="QuickStart"/>

1. **Create Project**
	
	创建ASP.NET Core Web 应用程序<br>
	选择ASP.NET Core 3.1<br>
	选择API project template

2. **Clean Project**
	
	移除Controller文件夹、WeatherForecast.cs文件、移除Properties->launchSettings.json中Project配置节与launchUrl<br>

		{
		  "$schema": "http://json.schemastore.org/launchsettings.json",
		  "iisSettings": {
		    "windowsAuthentication": false,
		    "anonymousAuthentication": true,
		    "iisExpress": {
		      "applicationUrl": "http://localhost:63917",
		      "sslPort": 44386
		    }
		  },
		  "profiles": {
		    "IIS Express": {
		      "commandName": "IISExpress",
		      "launchBrowser": true,
		      "environmentVariables": {
		        "ASPNETCORE_ENVIRONMENT": "Development"
		      }
		    }
		  }
		}

3. **Update Config、Startup**

	修改appsettings.json文件，增加DbConnection配置节与ApolloConnection子配置项

		"DbConnection": {
			"ApolloConnection": "Server=192.168.10.120;User Id=sa;Password=123.com;Database=AIMS_Developement;"
		},
	
	修改Startup，继承BaseStartup，清空所有方法，实现带参构造函数

		public class Startup : BaseStartup
	    {
	        public Startup(IConfiguration configuration) : base(configuration)
	        {
	        }
	    }	

4. **Add Entity、Dto、PlugDbContext、Service**

	增加Entity，按需继承Entity基类，标注Table标签，标签内容为生成的数据库表名<br>
	
		[Table("Demo_Table")]
		public class DemoTable : BaseEntity
	    {
	        public string name { get; set; }
	    }

	增加Dto，按需继承Dto基类，标注AutoMapper标签，标签内容为可以与该Dto互转的Entity类型<br>

		[AutoMapper(typeof(DemoTable))]
		public class DemoTableDto : BaseDto
	    {
	        public string name { get; set; }
	    }

	增加PlugDbContext，可自定义名称，继承自BaseDbContext，将创建的Entity加入

		public class PlugDbContext: BaseDbContext
	    {
	        public DbSet<DemoTable> DemoTables { get; set; }
	    }

	增加DemoTableService，可自定义名称，按需继承Interface基类，需要注意构造函数需要传入PlugDbContext而不是默认的BaseDbContext

		[NoLogin]
		public class DemoTableService : CurdService<DemoTable, DemoTableDto, BasePagger>
		{
		    public DemoTableService(PlugDbContext context) : base(context)
		    {
		    }
		}

5. **create or update database**

	在程序包管理控制台中输入migration命令，生成相关表信息
	
		add-migration [tagName] -context [PlugDbContext]
		update-database -context [PlugDbContext]

6. **开始使用**


## [Environment](#Directory)<span id="Environment"/>

.NETCore3.1

Visual Studio 2019 version 16.4.2+

Microsoft.EntityFrameworkCore.Design version 3.1.0

## [EntityClass](#Directory)<span id="EntityClass"/>

+ [**Entity**](#EntityClass)<span id="Entity"/>

EntityName|BaseClass|ExtensionProperty|AllProperty|Descripton
---|:---:|---|---
BaseEntity|-|id|id|基础实体基类，所有实体都派生自该类
CreateUserEntity|BaseEntity|create_user_id<br> create_user_name<br> create_time|id<br> create_user_id<br> create_user_name<br> create_time|带有创建者日志信息
UpdateUserEntity|CreateUserEntity|update_user_id<br> update_user_name<br> update_time|id<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time|带有修改者日志信息
DeleteEntity|BaseEntity|can_dele，能否被删除<br> is_remove，是否已删除|id<br> can_dele<br> is_remove|带有软删除属性
DeleteUserEntity|UpdateUserEntity|delete_user_id<br> delete_user_name<br> delete_time<br> can_dele，能否被删除<br> is_remove，是否已删除|id<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time<br> delete_user_id<br> delete_user_name<br> delete_time<br> can_dele<br> is_remove<br>|带有删除者日志信息与软删除属性
BaseTreeEntity|BaseEntity|pid|id<br> pid|基础树形类
CreateUserTreeEntity|CreateUserEntity|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time|带有创建者日志信息的树形类
UpdateUserTreeEntity|UpdateUserEntity|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time|带有修改者日志信息的树形类
DeleteUserTreeEntity|DeleteEntity|pid|id<br> pid<br> can_dele<br> is_remove|带有软删除属性的树形类
DeleteUserTreeEntity|DeleteUserEntity|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time<br> delete_user_id<br> delete_user_name<br> delete_time<br> can_dele<br> is_remove<br>|带有删除者日志信息与软删除属性的树形类

+ [**Dto**](#EntityClass)<span id="Dto"/>

DtoName|BaseClass|ExtensionProperty|AllProperty|Descripton
---|:---:|---|---
BaseDto|-|id|id|基础传输实体基类，所有传输实体都派生自该类
CreateUserDto|BaseDto|create_user_id<br> create_user_name<br> create_time|id<br> create_user_id<br> create_user_name<br> create_time|带有创建者日志信息
UpdateUserDto|CreateUserDto|update_user_id<br> update_user_name<br> update_time|id<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time|带有修改者日志信息
DeleteDto|BaseDto|can_dele，能否被删除<br> is_remove，是否已删除|id<br> can_dele<br> is_remove|带有软删除属性
DeleteUserDto|UpdateUserDto|delete_user_id<br> delete_user_name<br> delete_time<br> can_dele，能否被删除<br> is_remove，是否已删除|id<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time<br> delete_user_id<br> delete_user_name<br> delete_time<br> can_dele<br> is_remove<br>|带有删除者日志信息与软删除属性
BaseTreeDto|BaseDto|pid|id<br> pid|基础树形类
CreateUserTreeDto|CreateUserDto|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time|带有创建者日志信息的树形类
UpdateUserTreeDto|UpdateUserDto|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time|带有修改者日志信息的树形类
DeleteUserTreeDto|DeleteDto|pid|id<br> pid<br> can_dele<br> is_remove|带有软删除属性的树形类
DeleteUserTreeDto|DeleteUserDto|pid|id<br> pid<br> create_user_id<br> create_user_name<br> create_time<br> update_user_id<br> update_user_name<br> update_time<br> delete_user_id<br> delete_user_name<br> delete_time<br> can_dele<br> is_remove<br>|带有删除者日志信息与软删除属性的树形类

+ [**Attribute**](#EntityClass)<span id="Attribute"/>

	+ **EnumDescriptionAttribute**

    >枚举描述信息备注特性<br>
    >枚举扩展ToDescription，将提取描述信息，若没有相关特性，则返回枚举值字符串

		public enum SortType
		{
		    [EnumDescription("降序")]
		    DESC = 0,
		    [EnumDescription("升序")]
		    ASC = 1,
		}

	+ **ApolloMaxLengthAttribute**

	>封装MaxLengthAttribute扩展中文提示

	+ **ApolloRangeAttribute**

	>封装RangeAttribute扩展中文提示

	+ **ApolloRequiredAttribute**

	>封装RequiredAttribute扩展中文提示
	
	+ **ApiServiceAttribute**

	>标注类是接口，默认继承接口基类可以不需要写

	+ **IgnoreApiServiceAttribute**

	>标注类或者方法为非接口，不对外公布

	+ **NoLoginAttribute**

	>标注类或者接口可以不用登陆直接访问

+ [**Pagger**](#EntityClass)<span id="Pagger"/>

	+ **BasePagger**

    >分页基类，包含属性如下：<br>
    >PageIndex，页数，第一页从1开始，不传默认1<br>
    >PageSize，每页数据量，不传默认50<br>
    >Filter，过滤条件<br>
    >Sort，排序字段，不传默认为id降序<br>
    >SortType，排序方式，0降序，1升序，不传默认降序

	+ **BasePaggerBack**

	>分页返回类<br>
	>Count，数据总数<br>
	>Data，当前分页数据
	
## [Interface](#Directory)<span id="Interface"/>

InterfaceName|BaseClass|ExtensionMethod|Descripton
---|:---:|---|---
ApiService|-||接口基类，所有接口均直接或间接继承自该类
CommonBaseService|ApiService|Include<br>用于关联查询前包含需要查询的子项信息 Where<br>查询条件过滤 WhereIf<br>带判断的查询条件过滤 DoFilter<br>数据集合条件过滤 Find<br>数据查找 GetBeginTransaction<br>开启事务|系统自动带接口类公用函数，一般插件不继承该类
BaseEntityService|CommonBaseService|AddEntity<br> UpdateEntity<br> RemoveEntity<br> GetEntityInfo<br> GetAllEntities<br> GetEntityList|实体查询接口基类，只处理实体相关操作，常用接口基类
BaseDtoService|BaseEntityService|Add<br> AddList<br> Update<br> UpdateList<br> Remove<br> RemoveList<br> GetInfo<br> GetInfoList<br> GetAll<br> GetList|传输实体查询接口基类，处理传输实体相关操作
BaseService|BaseDtoService|-|用于兼容老版本系统，最常用基类接口之一
CurdService|BaseService|Add<br> Update<br> Remove<br> GetInfo<br> GetAll<br> GetList|开放接口，包含增删查改基本方法，继承该接口，自动对外公布平台自带的6个接口，最常用基类接口之一

## [Repository](#Directory)<span id="Repository"/>

+ **DbContext**

	>需要生成数据表的，需要标注Table标签<br> 
	>需要创建PlugDbContext继承自BaseDbContext，并添加类到DbSet

+ **AppSetting**

	>数据库连接字符串放在AppSetting下DbConnection配置节的ApolloConnection字段上 LogPath为本地日志存放目录，可以不写，默认站点目录

		"DbConnection": {
		    "ApolloConnection": "Server=192.168.10.120;User Id=sa;Password=xxxx;Database=AIMS_Developement;"
		},
		"LogPath": "D:\\Log",

+ **Migration命令**

		add-migration [tagName] -context [PlugDbContext]

	创建migration，其中tagName为标签名称，PlugDbContext为继承自BaseDbContext的类名

		update-migration -context [PlugDbContext]

	更新migration到数据库

		update-migration [tagName] -context [PlugDbContext]

	更新到指定版本

		remove-migration

	删除最新的migration文件

## [PlugDevelopement](#Directory)<span id="PlugDevelopement"/>

+ **config**

ConfigItemName|ParentItem|Description
---|---|---
PlugDirPath|root|插件存放目录，平台指向的Plugins目录，开发环境可指向公共libs目录
PlugList|root|插件列表，用于保存所有插件的名称和db名称
SelfDefinePlugName|PlugList|自定义插件名，插件别名定义
DbContext|SelfDefinePlugName|插件中DbContext的名称
DllName|SelfDefinePlugName|插件文件名称，不加dll后缀

	"PlugDirPath": "D:\\Source\\RQX.Common.Web.Platform\\bin\\Debug\\netcoreapp3.1\\Plugins",
	  "PlugList": {
	    "SelfDefinePlugName1": {
	      "DbContext": "Plug1DbContext",
	      "DllName": "Plug1"
	    },
	    "SelfDefinePlugName2": {
	      "DbContext": "Plug2DbContext",
	      "DllName": "Plug2"
	    }
	  }

+ **使用其他插件的函数**

		ApolloPlugReflection.GetPlug("SelfDefinePlugName").GetClass("OtherPlugServiceName").GetMethod("MethodName").Invoke(parameters);

	其中SelfDefinePlugName为config中自定义插件名，GetPlug会获取到该插件相关信息<br>
	OtherPlugServiceName为其他插件中要调用的服务类名，GetClass会获取到该类相关信息<br>
	MethodName为服务类中的函数名,GetMethod会获取到该方法<br>
	parameters为方法调用需要的参数列表，Invoke会调用该方法，并传入参数

+ **插件使用注意事项**

	1. 保证配置项中各名称的准确性
	2. 只能调用带一个DbContext参数的服务
	3. 调用需要使用Dto，不能使用Entity

## [Attention](#Directory)<span id="Attention"/>

+ **AutoMapper**

	>自动实体与Dto映射<br>
	>需要传输实体上标注AutoMapper到对应实体<br>
	>接口基类自动处理了的，若需手动调用，可使用MapTo方法<br>
	>实体与传输实体需要名称、类型相同的才能自动映射，不同的需要手动处理

		[AutoMapper(typeof(XXXXX))]

+ **DateTime**

	>数据库时间字段，需要标注列属性

		[Column(TypeName = "datetime")]

+ **Exception**

	>系统捕获处理的异常需要抛出ApolloException异常，普通Exception会以未捕获异常处理




















