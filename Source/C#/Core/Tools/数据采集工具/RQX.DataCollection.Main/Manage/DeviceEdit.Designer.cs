﻿namespace RQX.DataCollection.Main.Manage
{
    partial class DeviceEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_tcp_server_port = new System.Windows.Forms.TextBox();
            this.txt_tcp_server_ip = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_tcp_client_port = new System.Windows.Forms.TextBox();
            this.txt_tcp_client_ip = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_udp_target_port = new System.Windows.Forms.TextBox();
            this.txt_udp_target_ip = new System.Windows.Forms.TextBox();
            this.txt_udp_local_port = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ddl_com_portname = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ddl_com_databits = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ddl_com_stopbits = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ckb_com_rts = new System.Windows.Forms.CheckBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rbtn_udp = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.rbtn_com = new System.Windows.Forms.RadioButton();
            this.ddl_plug = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_device_name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.group_tcp_server = new System.Windows.Forms.GroupBox();
            this.group_com = new System.Windows.Forms.GroupBox();
            this.ddl_com_parity = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ckb_com_dtr = new System.Windows.Forms.CheckBox();
            this.ddl_com_baudrate = new System.Windows.Forms.ComboBox();
            this.group_udp = new System.Windows.Forms.GroupBox();
            this.group_tcp_client = new System.Windows.Forms.GroupBox();
            this.group_main = new System.Windows.Forms.GroupBox();
            this.rbtn_tcp_server = new System.Windows.Forms.RadioButton();
            this.rbtn_tcp_client = new System.Windows.Forms.RadioButton();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_cancle = new System.Windows.Forms.Button();
            this.group_tcp_server.SuspendLayout();
            this.group_com.SuspendLayout();
            this.group_udp.SuspendLayout();
            this.group_tcp_client.SuspendLayout();
            this.group_main.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_tcp_server_port
            // 
            this.txt_tcp_server_port.Location = new System.Drawing.Point(92, 55);
            this.txt_tcp_server_port.Name = "txt_tcp_server_port";
            this.txt_tcp_server_port.Size = new System.Drawing.Size(140, 23);
            this.txt_tcp_server_port.TabIndex = 3;
            // 
            // txt_tcp_server_ip
            // 
            this.txt_tcp_server_ip.Location = new System.Drawing.Point(92, 25);
            this.txt_tcp_server_ip.Name = "txt_tcp_server_ip";
            this.txt_tcp_server_ip.Size = new System.Drawing.Size(140, 23);
            this.txt_tcp_server_ip.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "监听端口";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "本机IP";
            // 
            // txt_tcp_client_port
            // 
            this.txt_tcp_client_port.Location = new System.Drawing.Point(91, 50);
            this.txt_tcp_client_port.Name = "txt_tcp_client_port";
            this.txt_tcp_client_port.Size = new System.Drawing.Size(141, 23);
            this.txt_tcp_client_port.TabIndex = 3;
            // 
            // txt_tcp_client_ip
            // 
            this.txt_tcp_client_ip.Location = new System.Drawing.Point(91, 20);
            this.txt_tcp_client_ip.Name = "txt_tcp_client_ip";
            this.txt_tcp_client_ip.Size = new System.Drawing.Size(141, 23);
            this.txt_tcp_client_ip.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "目标端口";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "目标地址";
            // 
            // txt_udp_target_port
            // 
            this.txt_udp_target_port.Location = new System.Drawing.Point(92, 80);
            this.txt_udp_target_port.Name = "txt_udp_target_port";
            this.txt_udp_target_port.Size = new System.Drawing.Size(140, 23);
            this.txt_udp_target_port.TabIndex = 3;
            // 
            // txt_udp_target_ip
            // 
            this.txt_udp_target_ip.Location = new System.Drawing.Point(92, 51);
            this.txt_udp_target_ip.Name = "txt_udp_target_ip";
            this.txt_udp_target_ip.Size = new System.Drawing.Size(140, 23);
            this.txt_udp_target_ip.TabIndex = 3;
            // 
            // txt_udp_local_port
            // 
            this.txt_udp_local_port.Location = new System.Drawing.Point(92, 22);
            this.txt_udp_local_port.Name = "txt_udp_local_port";
            this.txt_udp_local_port.Size = new System.Drawing.Size(140, 23);
            this.txt_udp_local_port.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "目标端口";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "目标地址";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "本机端口";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "串口号";
            // 
            // ddl_com_portname
            // 
            this.ddl_com_portname.FormattingEnabled = true;
            this.ddl_com_portname.Location = new System.Drawing.Point(91, 22);
            this.ddl_com_portname.Name = "ddl_com_portname";
            this.ddl_com_portname.Size = new System.Drawing.Size(140, 25);
            this.ddl_com_portname.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "端口号";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(57, 47);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(78, 25);
            this.comboBox2.TabIndex = 1;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(57, 78);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(78, 25);
            this.comboBox3.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "校验位";
            // 
            // ddl_com_databits
            // 
            this.ddl_com_databits.FormattingEnabled = true;
            this.ddl_com_databits.Location = new System.Drawing.Point(91, 115);
            this.ddl_com_databits.Name = "ddl_com_databits";
            this.ddl_com_databits.Size = new System.Drawing.Size(140, 25);
            this.ddl_com_databits.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(41, 117);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "数据位";
            // 
            // ddl_com_stopbits
            // 
            this.ddl_com_stopbits.FormattingEnabled = true;
            this.ddl_com_stopbits.Location = new System.Drawing.Point(91, 146);
            this.ddl_com_stopbits.Name = "ddl_com_stopbits";
            this.ddl_com_stopbits.Size = new System.Drawing.Size(140, 25);
            this.ddl_com_stopbits.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "停止位";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(7, 171);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(51, 21);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "DTR";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // ckb_com_rts
            // 
            this.ckb_com_rts.AutoSize = true;
            this.ckb_com_rts.Location = new System.Drawing.Point(183, 177);
            this.ckb_com_rts.Name = "ckb_com_rts";
            this.ckb_com_rts.Size = new System.Drawing.Size(49, 21);
            this.ckb_com_rts.TabIndex = 4;
            this.ckb_com_rts.Text = "RTS";
            this.ckb_com_rts.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(275, 46);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(84, 21);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "TCP服务端";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(181, 46);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(84, 21);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "TCP客户端";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // rbtn_udp
            // 
            this.rbtn_udp.AutoSize = true;
            this.rbtn_udp.Location = new System.Drawing.Point(91, 106);
            this.rbtn_udp.Name = "rbtn_udp";
            this.rbtn_udp.Size = new System.Drawing.Size(51, 21);
            this.rbtn_udp.TabIndex = 4;
            this.rbtn_udp.TabStop = true;
            this.rbtn_udp.Text = "UDP";
            this.rbtn_udp.UseVisualStyleBackColor = true;
            this.rbtn_udp.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 17);
            this.label13.TabIndex = 5;
            this.label13.Text = "通讯协议";
            // 
            // rbtn_com
            // 
            this.rbtn_com.AutoSize = true;
            this.rbtn_com.Location = new System.Drawing.Point(92, 79);
            this.rbtn_com.Name = "rbtn_com";
            this.rbtn_com.Size = new System.Drawing.Size(56, 21);
            this.rbtn_com.TabIndex = 4;
            this.rbtn_com.TabStop = true;
            this.rbtn_com.Tag = "";
            this.rbtn_com.Text = "COM";
            this.rbtn_com.UseVisualStyleBackColor = true;
            this.rbtn_com.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // ddl_plug
            // 
            this.ddl_plug.FormattingEnabled = true;
            this.ddl_plug.Location = new System.Drawing.Point(92, 48);
            this.ddl_plug.Name = "ddl_plug";
            this.ddl_plug.Size = new System.Drawing.Size(140, 25);
            this.ddl_plug.TabIndex = 3;
            this.ddl_plug.SelectedIndexChanged += new System.EventHandler(this.ddl_plug_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "使用采集插件";
            // 
            // txt_device_name
            // 
            this.txt_device_name.Location = new System.Drawing.Point(92, 19);
            this.txt_device_name.Name = "txt_device_name";
            this.txt_device_name.Size = new System.Drawing.Size(140, 23);
            this.txt_device_name.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "终端名称";
            // 
            // group_tcp_server
            // 
            this.group_tcp_server.Controls.Add(this.txt_tcp_server_port);
            this.group_tcp_server.Controls.Add(this.txt_tcp_server_ip);
            this.group_tcp_server.Controls.Add(this.label1);
            this.group_tcp_server.Controls.Add(this.label2);
            this.group_tcp_server.Location = new System.Drawing.Point(12, 176);
            this.group_tcp_server.Name = "group_tcp_server";
            this.group_tcp_server.Size = new System.Drawing.Size(245, 207);
            this.group_tcp_server.TabIndex = 0;
            this.group_tcp_server.TabStop = false;
            this.group_tcp_server.Text = "TCP服务端配置";
            // 
            // group_com
            // 
            this.group_com.Controls.Add(this.ddl_com_parity);
            this.group_com.Controls.Add(this.label16);
            this.group_com.Controls.Add(this.ckb_com_dtr);
            this.group_com.Controls.Add(this.ddl_com_baudrate);
            this.group_com.Controls.Add(this.ddl_com_portname);
            this.group_com.Controls.Add(this.label9);
            this.group_com.Controls.Add(this.label10);
            this.group_com.Controls.Add(this.ddl_com_databits);
            this.group_com.Controls.Add(this.label11);
            this.group_com.Controls.Add(this.ddl_com_stopbits);
            this.group_com.Controls.Add(this.label12);
            this.group_com.Controls.Add(this.ckb_com_rts);
            this.group_com.Location = new System.Drawing.Point(12, 173);
            this.group_com.Name = "group_com";
            this.group_com.Size = new System.Drawing.Size(245, 210);
            this.group_com.TabIndex = 1;
            this.group_com.TabStop = false;
            this.group_com.Text = "串口配置";
            this.group_com.Visible = false;
            // 
            // ddl_com_parity
            // 
            this.ddl_com_parity.FormattingEnabled = true;
            this.ddl_com_parity.Location = new System.Drawing.Point(91, 84);
            this.ddl_com_parity.Name = "ddl_com_parity";
            this.ddl_com_parity.Size = new System.Drawing.Size(140, 25);
            this.ddl_com_parity.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(41, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 17);
            this.label16.TabIndex = 0;
            this.label16.Text = "波特率";
            // 
            // ckb_com_dtr
            // 
            this.ckb_com_dtr.AutoSize = true;
            this.ckb_com_dtr.Location = new System.Drawing.Point(91, 177);
            this.ckb_com_dtr.Name = "ckb_com_dtr";
            this.ckb_com_dtr.Size = new System.Drawing.Size(51, 21);
            this.ckb_com_dtr.TabIndex = 4;
            this.ckb_com_dtr.Text = "DTR";
            this.ckb_com_dtr.UseVisualStyleBackColor = true;
            // 
            // ddl_com_baudrate
            // 
            this.ddl_com_baudrate.FormattingEnabled = true;
            this.ddl_com_baudrate.Location = new System.Drawing.Point(91, 53);
            this.ddl_com_baudrate.Name = "ddl_com_baudrate";
            this.ddl_com_baudrate.Size = new System.Drawing.Size(140, 25);
            this.ddl_com_baudrate.TabIndex = 1;
            // 
            // group_udp
            // 
            this.group_udp.Controls.Add(this.txt_udp_target_port);
            this.group_udp.Controls.Add(this.txt_udp_target_ip);
            this.group_udp.Controls.Add(this.txt_udp_local_port);
            this.group_udp.Controls.Add(this.label5);
            this.group_udp.Controls.Add(this.label6);
            this.group_udp.Controls.Add(this.label7);
            this.group_udp.Location = new System.Drawing.Point(12, 176);
            this.group_udp.Name = "group_udp";
            this.group_udp.Size = new System.Drawing.Size(245, 207);
            this.group_udp.TabIndex = 0;
            this.group_udp.TabStop = false;
            this.group_udp.Text = "UDP配置";
            // 
            // group_tcp_client
            // 
            this.group_tcp_client.Controls.Add(this.txt_tcp_client_port);
            this.group_tcp_client.Controls.Add(this.txt_tcp_client_ip);
            this.group_tcp_client.Controls.Add(this.label3);
            this.group_tcp_client.Controls.Add(this.label4);
            this.group_tcp_client.Location = new System.Drawing.Point(12, 173);
            this.group_tcp_client.Name = "group_tcp_client";
            this.group_tcp_client.Size = new System.Drawing.Size(245, 207);
            this.group_tcp_client.TabIndex = 0;
            this.group_tcp_client.TabStop = false;
            this.group_tcp_client.Text = "TCP客户端配置";
            // 
            // group_main
            // 
            this.group_main.Controls.Add(this.rbtn_tcp_server);
            this.group_main.Controls.Add(this.rbtn_tcp_client);
            this.group_main.Controls.Add(this.rbtn_udp);
            this.group_main.Controls.Add(this.label13);
            this.group_main.Controls.Add(this.rbtn_com);
            this.group_main.Controls.Add(this.ddl_plug);
            this.group_main.Controls.Add(this.label14);
            this.group_main.Controls.Add(this.txt_device_name);
            this.group_main.Controls.Add(this.label15);
            this.group_main.Location = new System.Drawing.Point(12, 12);
            this.group_main.Name = "group_main";
            this.group_main.Size = new System.Drawing.Size(245, 152);
            this.group_main.TabIndex = 0;
            this.group_main.TabStop = false;
            this.group_main.Text = "基础信息配置";
            // 
            // rbtn_tcp_server
            // 
            this.rbtn_tcp_server.AutoSize = true;
            this.rbtn_tcp_server.Location = new System.Drawing.Point(148, 106);
            this.rbtn_tcp_server.Name = "rbtn_tcp_server";
            this.rbtn_tcp_server.Size = new System.Drawing.Size(85, 21);
            this.rbtn_tcp_server.TabIndex = 4;
            this.rbtn_tcp_server.TabStop = true;
            this.rbtn_tcp_server.Text = "TCPServer";
            this.rbtn_tcp_server.UseVisualStyleBackColor = true;
            this.rbtn_tcp_server.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // rbtn_tcp_client
            // 
            this.rbtn_tcp_client.AutoSize = true;
            this.rbtn_tcp_client.Location = new System.Drawing.Point(148, 79);
            this.rbtn_tcp_client.Name = "rbtn_tcp_client";
            this.rbtn_tcp_client.Size = new System.Drawing.Size(80, 21);
            this.rbtn_tcp_client.TabIndex = 4;
            this.rbtn_tcp_client.TabStop = true;
            this.rbtn_tcp_client.Text = "TCPClient";
            this.rbtn_tcp_client.UseVisualStyleBackColor = true;
            this.rbtn_tcp_client.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(12, 386);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 37);
            this.btn_save.TabIndex = 2;
            this.btn_save.Text = "保存";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_cancle
            // 
            this.btn_cancle.Location = new System.Drawing.Point(182, 386);
            this.btn_cancle.Name = "btn_cancle";
            this.btn_cancle.Size = new System.Drawing.Size(75, 37);
            this.btn_cancle.TabIndex = 2;
            this.btn_cancle.Text = "取消";
            this.btn_cancle.UseVisualStyleBackColor = true;
            this.btn_cancle.Click += new System.EventHandler(this.btn_cancle_Click);
            // 
            // DeviceEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 431);
            this.Controls.Add(this.btn_cancle);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.group_main);
            this.Controls.Add(this.group_com);
            this.Controls.Add(this.group_tcp_server);
            this.Controls.Add(this.group_udp);
            this.Controls.Add(this.group_tcp_client);
            this.Name = "DeviceEdit";
            this.Text = "编辑设备终端信息";
            this.Load += new System.EventHandler(this.DeviceEdit_Load);
            this.group_tcp_server.ResumeLayout(false);
            this.group_tcp_server.PerformLayout();
            this.group_com.ResumeLayout(false);
            this.group_com.PerformLayout();
            this.group_udp.ResumeLayout(false);
            this.group_udp.PerformLayout();
            this.group_tcp_client.ResumeLayout(false);
            this.group_tcp_client.PerformLayout();
            this.group_main.ResumeLayout(false);
            this.group_main.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txt_tcp_server_port;
        private System.Windows.Forms.TextBox txt_tcp_server_ip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_tcp_client_port;
        private System.Windows.Forms.TextBox txt_tcp_client_ip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_udp_target_port;
        private System.Windows.Forms.TextBox txt_udp_target_ip;
        private System.Windows.Forms.TextBox txt_udp_local_port;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ddl_com_portname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ddl_com_databits;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddl_com_stopbits;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox ckb_com_rts;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rbtn_udp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rbtn_com;
        private System.Windows.Forms.ComboBox ddl_plug;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_device_name;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox group_tcp_server;
        private System.Windows.Forms.GroupBox group_tcp_client;
        private System.Windows.Forms.GroupBox group_udp;
        private System.Windows.Forms.GroupBox group_com;
        private System.Windows.Forms.GroupBox group_main;
        private System.Windows.Forms.CheckBox ckb_com_dtr;
        private System.Windows.Forms.ComboBox ddl_com_baudrate;
        private System.Windows.Forms.RadioButton rbtn_tcp_client;
        private System.Windows.Forms.RadioButton rbtn_tcp_server;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_cancle;
        private System.Windows.Forms.ComboBox ddl_com_parity;
        private System.Windows.Forms.Label label16;
    }
}