﻿using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.Net;
using RQX.Common.Framework.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Hardware.BaseCollection
{
    /// <summary>
    /// TCP客户端采集抽象类
    /// </summary>
    public abstract class AbstractTcpClientCollection : AbstractBaseCollection<TcpSocketClient>
    {
        protected AbstractTcpClientCollection(StructureModel<TcpSocketClient> structureModel) : base(structureModel)
        {
            if (_conn.IsNotNull())
            {
                _conn.HandleMsgReceived = (obj, buffer) =>
                {
                    RecvData(buffer);
                };
                //_conn.HandleException = (c, e) => LogUtils.WriteLogToConsole(e.ToString());
            }
        }
        public override void Start(bool isUseReStart = true)
        {
            _conn?.Start();
            Thread.Sleep(1000);
            base.Start(isUseReStart);
        }

        public override void Dispose()
        {
            _conn?.Close();
            _conn = null;
            base.Dispose();
        }
        
        protected override void Send(byte[] buffer)
        {
            WriteSendLogMethod(BufferTransferToString(buffer));
            _conn?.Send(buffer);
        }
    }
}
