﻿using RQX.Common.Core.Config;
using System;
using System.Data;
using System.Windows.Forms;

namespace DataCollection.Config
{
    public partial class DbSetting : Form
    {
        public DbSetting()
        {
            InitializeComponent();
        }
        /// <summary>
        /// init加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DbSetting_Load(object sender, EventArgs e)
        {
            var root = ConfigUtils.GetConfigurationRoot().GetSection("DataBase");
            var currentDbName = root.GetValue("CurrentUsedDB");
            var connStr = root.GetValue(currentDbName);
            txt_conn_str.Text = connStr;
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                var connStr = txt_conn_str.Text;
                var root = ConfigUtils.GetConfigurationRoot().GetSection("DataBase");
                var currentDbName = root.GetValue("CurrentUsedDB");
                root.SetValue(currentDbName, connStr);
                ConfigUtils.SaveToFile();
                FormUtils.ShowMsg("保存成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                FormUtils.ShowMsg($"保存异常！{ex.Message}");
            }
        }
    }
}
