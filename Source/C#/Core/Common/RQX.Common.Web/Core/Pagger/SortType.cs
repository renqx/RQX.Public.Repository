﻿using RQX.Common.Web.Core.Enum;

namespace RQX.Common.Web.Core.Pagger
{
    /// <summary>
    /// 排序方式枚举
    /// </summary>
    public enum SortType
    {
        [EnumDescription("降序")]
        DESC = 0,
        [EnumDescription("升序")]
        ASC = 1,
    }
}
