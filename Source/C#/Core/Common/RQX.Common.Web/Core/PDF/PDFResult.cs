﻿namespace RQX.Common.Web.Core.PDF
{
    public class PDFResult
    {
        public byte[] buffer { get; set; }
        public PDFResult() { }
        public PDFResult(byte[] buffer)
        {
            this.buffer = buffer;
        }
    }
}
