﻿using System;
using System.IO;

namespace RQX.Common.Framework.File
{
    /// <summary>
    /// 文件操作工具类
    /// </summary>
    public class FileUtils
    {
        public static void SaveAllToFile(string fileFullPath, string msg)
        {
            CheckOrCreatePath(fileFullPath);
            using (StreamWriter sw = new StreamWriter(fileFullPath, false))
            {
                sw.Write(msg);
            }
        }
        public static void SaveAppendToFile(string fileFullPath, string msg)
        {
            CheckOrCreatePath(fileFullPath);
            using (StreamWriter sw = new StreamWriter(fileFullPath, true))
            {
                sw.Write(msg);
            }
        }

        public static void CheckOrCreateDirPath(string dirPath)
        {
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
        }
        public static void CheckOrCreatePath(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            var dirPath = fileInfo.DirectoryName;
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
            if (!fileInfo.Exists)
                using (fileInfo.Create()) { }
        }

        public static void CreateNewFile(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            var dirPath = fileInfo.DirectoryName;
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
            using (fileInfo.Create()) { }
        }

        public static void DeletePath(string path,bool isRelative=false)
        {
            if (isRelative)
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            }
            Directory.Delete(path,true);
        }

    }
}
