﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Core.Hardware.BaseCollection
{
    public class StructureModel<T>
    {
        /// <summary>
        /// 连接协议
        /// </summary>
        public T Conn { get; set; }
        /// <summary>
        /// 本机IP
        /// </summary>
        public string LocalIP { get; set; }
        /// <summary>
        /// 最大接收间隔，超过时间则重启
        /// </summary>
        public int MaxRecvTime { get; set; }

        public object Ext1 { get; set; }
        public object Ext2 { get; set; }
        public object Ext3 { get; set; }
    }
}
