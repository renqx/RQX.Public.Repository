﻿namespace RQX.Common.Web.Web.Filter
{
    /// <summary>
    /// 异常类型
    /// </summary>
    public enum ApolloExceptionType
    {
        未登录 = 450,
        登录已过期 = 451,
        服务器内部错误 = 500,
        参数验证失败 = 501,
        数据已存在 = 502,
        数据未找到 = 503,
        日志记录 = 600,
        异常提示 = 700,
    }
}
