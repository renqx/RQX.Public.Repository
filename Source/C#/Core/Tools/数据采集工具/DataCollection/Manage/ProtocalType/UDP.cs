﻿using Microsoft.Extensions.Configuration;
using RQX.Common.Core.Config;

namespace DataCollection.Manage.ProtocalType
{
    /// <summary>
    /// UDP协议
    /// </summary>
    public class UDP
    {
        /// <summary>
        /// 本机端口
        /// </summary>
        public int LocalPort { get; set; }
        /// <summary>
        /// 目标IP地址
        /// </summary>
        public string TargetIP { get; set; }
        /// <summary>
        /// 目标端口
        /// </summary>
        public int TargetPort { get; set; }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="path"></param>
        public void Save(IConfigurationSection section)
        {
            section.SetValue($"LocalPort", LocalPort);
            section.SetValue($"TargetIP", TargetIP);
            section.SetValue($"TargetPort", TargetPort);
        }
        /// <summary>
        /// 获取参数字符串
        /// </summary>
        /// <returns></returns>
        public string GetParamStr() => $"Target:{TargetIP}:{TargetPort};Local:{LocalPort}";
    }
}
