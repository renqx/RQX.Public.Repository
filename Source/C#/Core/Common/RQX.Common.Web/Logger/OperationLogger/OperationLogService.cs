﻿using RQX.Common.Web.Core.Pagger;
using RQX.Common.Web.Core.Repository.DbContext;
using RQX.Common.Web.Web.Container;
using RQX.Common.Web.Web.DynamicApi;
using RQX.Common.Web.Web.Service;

namespace RQX.Common.Web.Logger.OperationLogger
{
    /// <summary>
    /// 操作日志
    /// </summary>
    [IocInjection]
    [IgnoreApiService]
    public class OperationLogService : BaseLoggerService<OperationLog, OperationLogDto, BasePagger>
    {
        public OperationLogService(CommonDbContext dbContext) : base(dbContext)
        {
        }
    }
}
