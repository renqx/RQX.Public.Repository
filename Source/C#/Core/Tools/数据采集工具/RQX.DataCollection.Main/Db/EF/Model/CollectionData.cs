﻿using RQX.Common.Core.Db.EF.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.DataCollection.Main.EF.Db.Model
{
    /// <summary>
    /// 采集到的数据存放类
    /// </summary>
    [Table("Data_Collection")]
    public class CollectionData : BaseEntity
    {
        /// <summary>
        /// 设备终端名称
        /// </summary>
        [Column("device_name")]
        public string DeviceName { get; set; }
        /// <summary>
        /// 中文名称
        /// </summary>
        [Column("name")]
        public string Name { get; set; }
        /// <summary>
        /// 编号名称
        /// </summary>
        [Column("code")]
        public string Code { get; set; }
        /// <summary>
        /// 采集值
        /// </summary>
        [Column("value")]
        public double Value { get; set; }
        /// <summary>
        /// 采集时间
        /// </summary>
        [Column("time")]
        public DateTime Time { get; set; }
    }
}
