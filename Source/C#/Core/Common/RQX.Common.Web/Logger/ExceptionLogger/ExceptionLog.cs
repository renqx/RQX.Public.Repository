﻿using RQX.Common.Web.Core.Entity;
using RQX.Common.Web.Core.Valid;
using RQX.Common.Web.Web.Filter;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RQX.Common.Web.Logger.ExceptionLogger
{
    /// <summary>
    /// 异常日志
    /// </summary>
    [Table("BS_LOG_EXCEPTION")]
    public class ExceptionLog : BaseEntity
    {
        /// <summary>
        /// 异常编号GUID
        /// </summary>
        [ApolloMaxLength(40)]
        public string code { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        [ApolloMaxLength(100)]
        public string type_name { get; set; }
        /// <summary>
        /// 异常发生时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime create_time { get; set; }
        /// <summary>
        /// 异常简要信息
        /// </summary>
        [Column(TypeName = "text")]
        public string message { get; set; }
        /// <summary>
        /// 异常堆栈信息
        /// </summary>
        public string exception { get; set; }

        public ExceptionLog() { }
        public ExceptionLog(ApolloException ex) : this(ex, ex.ExceptionType.ToString()) { }
        public ExceptionLog(Exception ex, string typeName)
        {
            create_time = DateTime.Now;
            message = ex.Message;
            exception = ex.ToString();
            type_name = typeName;
        }
    }
}
