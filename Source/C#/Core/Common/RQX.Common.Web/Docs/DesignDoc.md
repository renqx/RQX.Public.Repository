# 平台设计文档

##目录<span id="Directory"/>

1. [Core](#Core)
	1. [Entity](#Entity)
	2. [Dto](#Dto)
	3. [Enum](#Enum)
	4. [Interface](#Interface)
	5. [Pagger](#Pagger)
	6. [Tree](#Tree)
	7. [AutoMapper](#AutoMapper)
	8. [Valid](#Valid)
	9. [Repository](#Repository)
	10. [PDF](#PDF)
2. [Config](#Config)
3. [Authority](#Authority)
4. [Logger](#Logger)
	1. [InterfaceLogger](#InterfaceLogger)
	2. [ExceptionLogger](#ExceptionLogger)
	3. [OperationLogger](#OperationLogger)
	4. [LocalLogger](#LocalLogger)
5. [Web](#Web)
	1. [Container](#Container)
	2. [DynamicApi](#DynamicApi)
	3. [Filter](#Filter)
	4. [Plug](#Plug)
	5. [Service](#Service)
	6. [Startup](#Startup)
	7. [Swagger](#Swagger)
6. [Tools](#Tools)

## [Core](#Directory)<span id="Core"/>

用于存放核心基础类，包括实体、传输实体、相关扩展类、分页、枚举、验证实体、实体映射、仓储等

### [Entity](#Core)<span id="Entity"/>

+ **BaseEntity**

	基础实体，所有Entity的父类，定义了int型的主键id

+ **CreateUserEntity**

	继承自BaseEntity，扩展了create_user_id、create_user_name、create_time，处理创建者相关信息

+ **DeleteEntity**

	继承自BaseEntity，扩展了can_dele、is_remove字段，用于软删除

+ **DeleteUserEntity**

	继承自UdpateUserEntity，扩展了delete_user_id、delete_user_name、delete_time、can_dele、is_remove字段，处理删除者相关信息与软删除

+ **UdpateUserEntity**

	继承自CreateUserEntity，扩展了update_user_id、update_user_name、update_time，处理更新者相关信息

### [Dto](#Core)<span id="Dto"/>

+ **BaseDto**

	基础传输实体，所有Dto的父类，定义了id，与BaseEntity对应
	
+ **CreateUserDto**
 
	继承自BaseDto，扩展了create_user_id、create_user_name、create_time，处理创建者相关信息，与CreateUserEntity对应
	
+ **DeleteDto**

	继承自BaseDto，扩展了can_dele、is_remove字段，用于软删除，与DeleteEntity对应

+ **DeleteUserDto**

	继承自UpdateUserDto，扩展了delete_user_id、delete_user_name、delete_time、can_dele、is_remove字段，处理删除者相关信息与软删除，与DeleteUserEntity对应

+ **UpdateUserDto**

	继承自CreateUserDto，扩展了update_user_id、update_user_name、update_time，处理更新者相关信息，与UdpateUserEntity对应

### [Enum](#Core)<span id="Enum"/>

+ **EnumDescriptionAttribute**

	枚举标注，扩展备注信息，扩展了枚举值ToDescription方法，获取标注的备注信息

### [Interface](#Core)<span id="Interface"/>

+ **ICreateUser**

	包含添加者信息的接口

+ **IDelete**

	包含软删除的接口

+ **IUpdateUser**

	包含更新者信息的接口，继承自ICreateUser

+ **IDeleteUser**

	包含删除者与软删除的接口，继承自IUpdateUser、IDelete

### [Pagger](#Core)<span id="Pagger"/>

+ **IBaseFilter**

	包含过滤条件的接口

+ **IBasePagger**

	包含分页的接口

+ **IBaseSort**

	包含排序的接口

+ **IPagger**

	分页接口，继承自IBaseSort、IBasePagger、IBaseFilter

+ **SortType** 

	排序方式枚举值，包含升序与降序

+ **BasePagger**

	分页基础类

+ **BasePaggerBack**

	分页返回类

+ **PaggerExtension**

	分页扩展，扩展了Orderby排序方法

### [Tree](#Core)<span id="Tree"/>

+ **BaseTreeEntity**

	基础树形实体，扩展了pid，继承自BaseEntity

+ **CreateUserTreeEntity**

	带创建者的树形实体，扩展了pid，继承自CreateUserEntity

+ **DeleteTreeEntity**

	带软删除的树形实体，扩展了pid，继承自DeleteEntity

+ **UpdateUserTreeEntity**

	带修改者的树形实体，扩展了pid，继承自UpdateUserEntity

+ **DeleteUserTreeEntity**

	带删除者的树形实体，扩展了pid，继承自DeleteUserEntity

+ **BaseTreeDto**
	
	基础树形Dto，扩展了pid，继承自BaseDto

+ **CreateUserTreeDto**

	带创建者的树形传输实体，扩展了pid，继承自CreateUserDto

+ **DeleteTreeDto**

	带软删除的树形传输实体，扩展了pid，继承自DeleteDto

+ **UpdateUserTreeDto**

	带修改者的树形传输实体，扩展了pid，继承自UpdateUserDto

+ **DeleteUserTreeDto**

	带删除者的树形传输实体，扩展了pid，继承自DeleteUserDto

### [AutoMapper](#Core)<span id="AutoMapper"/>

+ **AutoMapperAttribute**

	自定义类型转换特性，标注了的会进行相关映射

+ **AutoMapperExtension**

	AutoMapper扩展，用于service注入与MapTo方法扩展

+ **AutoMapperProfile**

	关系映射实现类	

### [Valid](#Core)<span id="Valid"/>

+ **ApolloMaxLengthAttribute**

	继承自MaxLengthAttribute，扩展中文提示

+ **ApolloRangeAttribute**

	继承自RangeAttribute，扩展中文提示

+ **ApolloRequiredAttribute**

	继承自RequiredAttribute，扩展中文提示

### [Repository](#Core)<span id="Repository"/>

+ **BaseDbContext**

	基础仓储类

+ **CommonDbContext**

	系统仓储类，扩展了接口相关表与系统配置相关表

+ **DbContextExtension**

	仓储扩展类，扩展了db注入、WhereIf方法、OrderBy方法
	
+ **IRepository**

	仓储封装接口

+ **BaseRepository**

	仓储封装实现类

### [PDF](#Core)<span id="PDF"/>

+ **PDFResult**

	PDF二进制数据回传封装类

## [Config](#Directory)<span id="Config"/>

+ **AppConfigurtaion**

	配置项获取类

+ **GlobalConfig**

	配置项名称配置类

+ **SysConfTemplates**

	系统配置模板相关文件

+ **SysConfTemplateItems**

	系统模板配置子项相关文件

+ **SysConfModels**

	系统配置模块相关文件

+ **SysConfItems**

	系统配置项相关文件

+ **SysConfDatas**

	系统配置数据相关文件

## [Authority](#Directory)<span id="Authority"/>

+ **CypherHelper**

	加密类

+ **LoginUser**

	登录用户信息与用户验证

+ **NoLoginAttribute**

	不需要登录的标签特性

## [Logger](#Directory)<span id="Logger"/>

### [InterfaceLogger](#Logger)<span id="InterfaceLogger"/>

记录接口日志相关信息

### [ExceptionLogger](#Logger)<span id="ExceptionLogger"/>

记录异常日志信息

### [OperationLogger](#Logger)<span id="OperationLogger"/>

记录操作日志信息

### [LocalLogger](#Logger)<span id="LocalLogger"/>

记录本地日志信息

## [Web](#Directory)<span id="Web"/>

### [Container](#Web)<span id="Container"/>

包含容器注入与跨插件反射调用方法

### [DynamicApi](#Web)<span id="DynamicApi"/>

动态API相关控制，接口是否暴露等

### [Filter](#Web)<span id="Filter"/>

接口过滤器与异常捕获过滤器，并同时记录相关接口日志信息

### [Plug](#Web)<span id="Plug"/>

插件目录加载dll扩展

### [Service](#Web)<span id="Service"/>

存放各基础接口基类，常用的为BaseService与CurdService

### [Startup](#Web)<span id="Startup"/>

程序启动配置注入与相关组件注入

### [Swagger](#Web)<span id="Swagger"/>

Swagger接口文档相关配置

## [Tools](#Directory)<span id="Tools"/>

工具相关的，包括常见的一些扩展方法，反射方法，Socket连接，DataMapper等