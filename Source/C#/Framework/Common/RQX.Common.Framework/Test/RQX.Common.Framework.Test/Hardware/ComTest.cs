﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RQX.Common.Framework.Extension;
using RQX.Common.Framework.Hardware.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RQX.Common.Framework.Test.Hardware
{
    [TestClass]
    public class ComTest
    {
        [TestMethod]
        public void CreateMyCom()
        {
            var com = new MyCom("COM1");
            Assert.IsTrue(com.IsNotNull());
        }
    }
}
